-- MariaDB dump 10.18  Distrib 10.5.8-MariaDB, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: forumdb
-- ------------------------------------------------------
-- Server version	10.5.8-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banned_users`
--

DROP TABLE IF EXISTS `banned_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banned_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `reason` varchar(45) DEFAULT NULL,
  `banned_user_id` int(11) NOT NULL,
  `banned_from_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`banned_user_id`,`banned_from_id`),
  KEY `fk_banned_users_users1_idx` (`banned_user_id`),
  KEY `fk_banned_users_users2_idx` (`banned_from_id`),
  CONSTRAINT `fk_banned_users_users1` FOREIGN KEY (`banned_user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_banned_users_users2` FOREIGN KEY (`banned_from_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banned_users`
--

LOCK TABLES `banned_users` WRITE;
/*!40000 ALTER TABLE `banned_users` DISABLE KEYS */;
INSERT INTO `banned_users` VALUES (1,'2021-07-02 11:02:58','не ме кефи',3,1);
/*!40000 ALTER TABLE `banned_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `date_created` datetime NOT NULL DEFAULT current_timestamp(),
  `date_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `posts_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`posts_id`,`users_id`),
  KEY `fk_comments_posts1_idx` (`posts_id`),
  KEY `fk_comments_users1_idx` (`users_id`),
  CONSTRAINT `fk_comments_posts1` FOREIGN KEY (`posts_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,'Първоначално името на езика е било Pearl (произнася се по същия начин – „пърл“). Още преди да излезе първата версия на езика, Лари установява, че съществува език за програмиране на име PEARL. С цел да се избегнат конфликти, а и повлиян от характерната за философията на Unix тенденция към кратки имена, Лари променя името на езика на Perl, без да променя произношението му. Впоследствие се появяват интерпретации на името като съкращение, най-популярната от които е Practical Extraction and Report Language (в превод: практичен език за извличане и отчети). Съществуват и много други интерпретации, включително и такива от самия Лари Уол, като например шеговитото Pathologically Eclectic Rubbish Lister (в превод: патологично многостранен изброител на глупости). Всички те обаче са бекроними. Името на езика не е съкращение от нищо, поради което се пише само с една главна буква – Perl, а не PERL (за разлика от споменатия език PEARL, чието име наистина е съкращение). Съществува също така тънка разлика в значението в зависимост от това дали името е изписано с главна буква в началото или изцяло с малки букви. Perl е името на езика за програмиране, което, както (почти) всяко друго име, се пише с главна буква. Под perl се има предвид интерпретатора на езика Perl, т.е. програмата, която се стартира, за да се изпълни даден код на Perl. Името на интерпретатора спазва неписаното правило имената на команди под Юникс да са изцяло с малки букви.','2021-07-07 12:33:27','2021-07-07 12:33:27',1,2,3),(2,'Нов текст отново','2021-07-07 12:36:10','2021-07-08 22:37:35',0,2,4),(3,'Първоначално името на езика е било Pearl (произнася се по същия начин – „пърл“). Още преди да излезе първата версия на езика, Лари установява, че съществува език за програмиране на име PEARL. С цел да се избегнат конфликти, а и повлиян от характерната за философията на Unix тенденция към кратки имена, Лари променя името на езика на Perl, без да променя произношението му. Впоследствие се появяват интерпретации на името като съкращение, най-популярната от които е Practical Extraction and Report Language (в превод: практичен език за извличане и отчети). Съществуват и много други интерпретации, включително и такива от самия Лари Уол, като например шеговитото Pathologically Eclectic Rubbish Lister (в превод: патологично многостранен изброител на глупости). Всички те обаче са бекроними. Името на езика не е съкращение от нищо, поради което се пише само с една главна буква – Perl, а не PERL (за разлика от споменатия език PEARL, чието име наистина е съкращение). Съществува също така тънка разлика в значението в зависимост от това дали името е изписано с главна буква в началото или изцяло с малки букви. Perl е името на езика за програмиране, което, както (почти) всяко друго име, се пише с главна буква. Под perl се има предвид интерпретатора на езика Perl, т.е. програмата, която се стартира, за да се изпълни даден код на Perl. Името на интерпретатора спазва неписаното правило имената на команди под Юникс да са изцяло с малки букви.','2021-07-07 12:36:31','2021-07-07 12:36:31',0,2,2),(4,'Първоначално името на езика е било Pearl (произнася се по същия начин – „пърл“). Още преди да излезе първата версия на езика, Лари установява, че съществува език за програмиране на име PEARL. С цел да се избегнат конфликти, а и повлиян от характерната за философията на Unix тенденция към кратки имена, Лари променя името на езика на Perl, без да променя произношението му. Впоследствие се появяват интерпретации на името като съкращение, най-популярната от които е Practical Extraction and Report Language (в превод: практичен език за извличане и отчети). Съществуват и много други интерпретации, включително и такива от самия Лари Уол, като например шеговитото Pathologically Eclectic Rubbish Lister (в превод: патологично многостранен изброител на глупости). Всички те обаче са бекроними. Името на езика не е съкращение от нищо, поради което се пише само с една главна буква – Perl, а не PERL (за разлика от споменатия език PEARL, чието име наистина е съкращение). Съществува също така тънка разлика в значението в зависимост от това дали името е изписано с главна буква в началото или изцяло с малки букви. Perl е името на езика за програмиране, което, както (почти) всяко друго име, се пише с главна буква. Под perl се има предвид интерпретатора на езика Perl, т.е. програмата, която се стартира, за да се изпълни даден код на Perl. Името на интерпретатора спазва неписаното правило имената на команди под Юникс да са изцяло с малки букви.','2021-07-07 12:36:47','2021-07-07 12:36:47',0,2,4),(5,'Първоначално името на езика е било Pearl (произнася се по същия начин – „пърл“). Още преди да излезе първата версия на езика, Лари установява, че съществува език за програмиране на име PEARL. С цел да се избегнат конфликти, а и повлиян от характерната за философията на Unix тенденция към кратки имена, Лари променя името на езика на Perl, без да променя произношението му. Впоследствие се появяват интерпретации на името като съкращение, най-популярната от които е Practical Extraction and Report Language (в превод: практичен език за извличане и отчети). Съществуват и много други интерпретации, включително и такива от самия Лари Уол, като например шеговитото Pathologically Eclectic Rubbish Lister (в превод: патологично многостранен изброител на глупости). Всички те обаче са бекроними. Името на езика не е съкращение от нищо, поради което се пише само с една главна буква – Perl, а не PERL (за разлика от споменатия език PEARL, чието име наистина е съкращение). Съществува също така тънка разлика в значението в зависимост от това дали името е изписано с главна буква в началото или изцяло с малки букви. Perl е името на езика за програмиране, което, както (почти) всяко друго име, се пише с главна буква. Под perl се има предвид интерпретатора на езика Perl, т.е. програмата, която се стартира, за да се изпълни даден код на Perl. Името на интерпретатора спазва неписаното правило имената на команди под Юникс да са изцяло с малки букви.','2021-07-07 13:15:15','2021-07-07 13:15:15',0,10,1),(6,'Първоначално името на езика е било Pearl (произнася се по същия начин – „пърл“). Още преди да излезе първата версия на езика, Лари установява, че съществува език за програмиране на име PEARL. С цел да се избегнат конфликти, а и повлиян от характерната за философията на Unix тенденция към кратки имена, Лари променя името на езика на Perl, без да променя произношението му. Впоследствие се появяват интерпретации на името като съкращение, най-популярната от които е Practical Extraction and Report Language (в превод: практичен език за извличане и отчети). Съществуват и много други интерпретации, включително и такива от самия Лари Уол, като например шеговитото Pathologically Eclectic Rubbish Lister (в превод: патологично многостранен изброител на глупости). Всички те обаче са бекроними. Името на езика не е съкращение от нищо, поради което се пише само с една главна буква – Perl, а не PERL (за разлика от споменатия език PEARL, чието име наистина е съкращение). Съществува също така тънка разлика в значението в зависимост от това дали името е изписано с главна буква в началото или изцяло с малки букви. Perl е името на езика за програмиране, което, както (почти) всяко друго име, се пише с главна буква. Под perl се има предвид интерпретатора на езика Perl, т.е. програмата, която се стартира, за да се изпълни даден код на Perl. Името на интерпретатора спазва неписаното правило имената на команди под Юникс да са изцяло с малки букви.','2021-07-07 13:38:45','2021-07-07 13:38:45',0,10,1),(7,'Първоначално името на езика е било Pearl (произнася се по същия начин – „пърл“). Още преди да излезе първата версия на езика, Лари установява, че съществува език за програмиране на име PEARL. С цел да се избегнат конфликти, а и повлиян от характерната за философията на Unix тенденция към кратки имена, Лари променя името на езика на Perl, без да променя произношението му. Впоследствие се появяват интерпретации на името като съкращение, най-популярната от които е Practical Extraction and Report Language (в превод: практичен език за извличане и отчети). Съществуват и много други интерпретации, включително и такива от самия Лари Уол, като например шеговитото Pathologically Eclectic Rubbish Lister (в превод: патологично многостранен изброител на глупости). Всички те обаче са бекроними. Името на езика не е съкращение от нищо, поради което се пише само с една главна буква – Perl, а не PERL (за разлика от споменатия език PEARL, чието име наистина е съкращение). Съществува също така тънка разлика в значението в зависимост от това дали името е изписано с главна буква в началото или изцяло с малки букви. Perl е името на езика за програмиране, което, както (почти) всяко друго име, се пише с главна буква. Под perl се има предвид интерпретатора на езика Perl, т.е. програмата, която се стартира, за да се изпълни даден код на Perl. Името на интерпретатора спазва неписаното правило имената на команди под Юникс да са изцяло с малки букви.','2021-07-07 13:38:53','2021-07-07 13:38:53',0,10,1),(8,'Първоначално името на езика е било Pearl (произнася се по същия начин – „пърл“). Още преди да излезе първата версия на езика, Лари установява, че съществува език за програмиране на име PEARL. С цел да се избегнат конфликти, а и повлиян от характерната за философията на Unix тенденция към кратки имена, Лари променя името на езика на Perl, без да променя произношението му. Впоследствие се появяват интерпретации на името като съкращение, най-популярната от които е Practical Extraction and Report Language (в превод: практичен език за извличане и отчети). Съществуват и много други интерпретации, включително и такива от самия Лари Уол, като например шеговитото Pathologically Eclectic Rubbish Lister (в превод: патологично многостранен изброител на глупости). Всички те обаче са бекроними. Името на езика не е съкращение от нищо, поради което се пише само с една главна буква – Perl, а не PERL (за разлика от споменатия език PEARL, чието име наистина е съкращение). Съществува също така тънка разлика в значението в зависимост от това дали името е изписано с главна буква в началото или изцяло с малки букви. Perl е името на езика за програмиране, което, както (почти) всяко друго име, се пише с главна буква. Под perl се има предвид интерпретатора на езика Perl, т.е. програмата, която се стартира, за да се изпълни даден код на Perl. Името на интерпретатора спазва неписаното правило имената на команди под Юникс да са изцяло с малки букви.','2021-07-07 13:39:20','2021-07-07 13:39:20',0,10,1),(9,'Първоначално името на езика е било Pearl (произнася се по същия начин – „пърл“). Още преди да излезе първата версия на езика, Лари установява, че съществува език за програмиране на име PEARL. С цел да се избегнат конфликти, а и повлиян от характерната за философията на Unix тенденция към кратки имена, Лари променя името на езика на Perl, без да променя произношението му. Впоследствие се появяват интерпретации на името като съкращение, най-популярната от които е Practical Extraction and Report Language (в превод: практичен език за извличане и отчети). Съществуват и много други интерпретации, включително и такива от самия Лари Уол, като например шеговитото Pathologically Eclectic Rubbish Lister (в превод: патологично многостранен изброител на глупости). Всички те обаче са бекроними. Името на езика не е съкращение от нищо, поради което се пише само с една главна буква – Perl, а не PERL (за разлика от споменатия език PEARL, чието име наистина е съкращение). Съществува също така тънка разлика в значението в зависимост от това дали името е изписано с главна буква в началото или изцяло с малки букви. Perl е името на езика за програмиране, което, както (почти) всяко друго име, се пише с главна буква. Под perl се има предвид интерпретатора на езика Perl, т.е. програмата, която се стартира, за да се изпълни даден код на Perl. Името на интерпретатора спазва неписаното правило имената на команди под Юникс да са изцяло с малки букви.','2021-07-07 14:02:52','2021-07-07 14:02:52',0,10,1),(10,'NESHTO SI TAM','2021-07-08 10:57:04','2021-07-08 10:57:04',0,1,1),(11,'NESHTO SI TAM','2021-07-08 10:58:24','2021-07-08 10:58:24',0,1,1),(12,'NESHTO SI TAM','2021-07-08 10:59:43','2021-07-08 10:59:43',0,1,1),(13,'NESHTO SI TAM','2021-07-08 11:02:18','2021-07-08 11:02:18',0,1,1),(14,'NESHTO SI TAM','2021-07-08 11:02:32','2021-07-08 11:02:32',0,1,1),(15,'NESHTO SI TAM','2021-07-08 11:02:37','2021-07-08 11:02:37',0,1,1),(16,'NESHTO SI TAM','2021-07-08 11:03:13','2021-07-08 11:03:13',0,1,1),(17,'Първоначално името на езика е било Pearl (произнася се по същия начин – „пърл“). Още преди да излезе първата версия на езика, Лари установява, че съществува език за програмиране на име PEARL. С цел да се избегнат конфликти, а и повлиян от характерната за философията на Unix тенденция към кратки имена, Лари променя името на езика на Perl, без да променя произношението му. Впоследствие се появяват интерпретации на името като съкращение, най-популярната от които е Practical Extraction and Report Language (в превод: практичен език за извличане и отчети). Съществуват и много други интерпретации, включително и такива от самия Лари Уол, като например шеговитото Pathologically Eclectic Rubbish Lister (в превод: патологично многостранен изброител на глупости). Всички те обаче са бекроними. Името на езика не е съкращение от нищо, поради което се пише само с една главна буква – Perl, а не PERL (за разлика от споменатия език PEARL, чието име наистина е съкращение). Съществува също така тънка разлика в значението в зависимост от това дали името е изписано с главна буква в началото или изцяло с малки букви. Perl е името на езика за програмиране, което, както (почти) всяко друго име, се пише с главна буква. Под perl се има предвид интерпретатора на езика Perl, т.е. програмата, която се стартира, за да се изпълни даден код на Perl. Името на интерпретатора спазва неписаното правило имената на команди под Юникс да са изцяло с малки букви.','2021-07-08 22:17:38','2021-07-08 22:17:38',0,10,4);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments_likes`
--

DROP TABLE IF EXISTS `comments_likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT current_timestamp(),
  `comments_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`comments_id`,`users_id`),
  KEY `fk_comments_like_comments1_idx` (`comments_id`),
  KEY `fk_comments_like_users1_idx` (`users_id`),
  CONSTRAINT `fk_comments_like_comments1` FOREIGN KEY (`comments_id`) REFERENCES `comments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_like_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments_likes`
--

LOCK TABLES `comments_likes` WRITE;
/*!40000 ALTER TABLE `comments_likes` DISABLE KEYS */;
INSERT INTO `comments_likes` VALUES (1,'2021-07-07 13:32:10',1,4),(3,'2021-07-08 10:49:05',2,1),(4,'2021-07-08 20:21:31',10,5),(6,'2021-07-08 20:22:15',11,3),(7,'2021-07-08 20:22:15',11,2),(8,'2021-07-09 10:22:49',2,4),(9,'2021-07-09 10:30:15',6,4),(10,'2021-07-09 10:30:37',10,4),(11,'2021-07-09 10:30:47',11,4),(12,'2021-07-09 10:31:16',12,4),(25,'2021-07-09 21:25:03',3,4);
/*!40000 ALTER TABLE `comments_likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flag_comments`
--

DROP TABLE IF EXISTS `flag_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flag_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `checked_admin` tinyint(1) NOT NULL DEFAULT 0,
  `checked_owner` tinyint(1) NOT NULL DEFAULT 0,
  `approved` tinyint(1) NOT NULL DEFAULT 0,
  `comments_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`comments_id`,`users_id`),
  KEY `fk_flag_comments_comments1_idx` (`comments_id`),
  KEY `fk_flag_comments_users1_idx` (`users_id`),
  CONSTRAINT `fk_flag_comments_comments1` FOREIGN KEY (`comments_id`) REFERENCES `comments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_flag_comments_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flag_comments`
--

LOCK TABLES `flag_comments` WRITE;
/*!40000 ALTER TABLE `flag_comments` DISABLE KEYS */;
INSERT INTO `flag_comments` VALUES (14,'2021-07-10 16:54:56',0,0,0,3,1),(15,'2021-07-10 17:28:42',1,0,1,2,1);
/*!40000 ALTER TABLE `flag_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flag_posts`
--

DROP TABLE IF EXISTS `flag_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flag_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT current_timestamp(),
  `posts_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`posts_id`,`users_id`),
  KEY `fk_flag_posts_posts1_idx` (`posts_id`),
  KEY `fk_flag_posts_users1_idx` (`users_id`),
  CONSTRAINT `fk_flag_posts_posts1` FOREIGN KEY (`posts_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_flag_posts_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flag_posts`
--

LOCK TABLES `flag_posts` WRITE;
/*!40000 ALTER TABLE `flag_posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `flag_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `friends`
--

DROP TABLE IF EXISTS `friends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_confirm` varchar(45) NOT NULL,
  `reciver_confirm` varchar(45) NOT NULL,
  `start_date` date NOT NULL,
  `user_send_id` int(11) NOT NULL,
  `user_recive_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`user_send_id`,`user_recive_id`),
  KEY `fk_friends_users1_idx` (`user_send_id`),
  KEY `fk_friends_users2_idx` (`user_recive_id`),
  CONSTRAINT `fk_friends_users1` FOREIGN KEY (`user_send_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_friends_users2` FOREIGN KEY (`user_recive_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friends`
--

LOCK TABLES `friends` WRITE;
/*!40000 ALTER TABLE `friends` DISABLE KEYS */;
/*!40000 ALTER TABLE `friends` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'admin'),(2,'user');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locked_post`
--

DROP TABLE IF EXISTS `locked_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locked_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `reason` varchar(45) DEFAULT NULL,
  `posts_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`posts_id`,`users_id`),
  KEY `fk_locked_post_posts_idx` (`posts_id`),
  KEY `fk_locked_post_users1_idx` (`users_id`),
  CONSTRAINT `fk_locked_post_posts` FOREIGN KEY (`posts_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_locked_post_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locked_post`
--

LOCK TABLES `locked_post` WRITE;
/*!40000 ALTER TABLE `locked_post` DISABLE KEYS */;
INSERT INTO `locked_post` VALUES (1,'2021-07-06 19:52:05','none',3,1);
/*!40000 ALTER TABLE `locked_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_cathegory`
--

DROP TABLE IF EXISTS `post_cathegory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_cathegory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_cathegory`
--

LOCK TABLES `post_cathegory` WRITE;
/*!40000 ALTER TABLE `post_cathegory` DISABLE KEYS */;
INSERT INTO `post_cathegory` VALUES (1,'JavaScript',0),(2,'PHP',0),(3,'TCL',0),(4,'Perl',0),(5,'CPP',0),(6,'New name',0),(7,'New_category',0);
/*!40000 ALTER TABLE `post_cathegory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_likes`
--

DROP TABLE IF EXISTS `post_likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT current_timestamp(),
  `posts_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`posts_id`,`users_id`),
  KEY `fk_post_likes_posts1_idx` (`posts_id`),
  KEY `fk_post_likes_users1_idx` (`users_id`),
  CONSTRAINT `fk_post_likes_posts1` FOREIGN KEY (`posts_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_post_likes_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_likes`
--

LOCK TABLES `post_likes` WRITE;
/*!40000 ALTER TABLE `post_likes` DISABLE KEYS */;
/*!40000 ALTER TABLE `post_likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `text` text NOT NULL,
  `date_created` datetime DEFAULT current_timestamp(),
  `date_updated` datetime DEFAULT current_timestamp(),
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `post_cathegory_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`post_cathegory_id`,`users_id`),
  KEY `fk_posts_post_cathegory1_idx` (`post_cathegory_id`),
  KEY `fk_posts_users1_idx` (`users_id`),
  CONSTRAINT `fk_posts_post_cathegory1` FOREIGN KEY (`post_cathegory_id`) REFERENCES `post_cathegory` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_posts_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'TestPost','нещо си там','2021-10-11 00:00:00','2021-10-11 00:00:00',0,1,1),(2,'Promises','I\'m feel like in dark fog','2021-07-03 13:32:07','2021-07-03 13:32:07',0,3,1),(3,'Objects','I\'m feel like in dark fog','2021-07-03 13:34:32','2021-07-03 13:34:32',0,2,1),(4,'Arrays','I\'m feel like in dark fog','2021-07-03 13:35:20','2021-07-03 13:35:20',0,2,1),(5,'Classes','I\'m feel like in dark fog','2021-07-03 13:37:19','2021-07-03 13:37:19',0,4,1),(7,'нещо ново','Тралала - трилала','2021-07-04 20:26:58','2021-07-04 20:26:58',0,2,1),(8,'Правя някакъв опит','Тралала - трилала','2021-07-05 11:16:49','2021-07-05 11:16:49',0,2,4),(9,'Заглавие','Тралала - трилала','2021-07-05 11:49:23','2021-07-05 11:49:23',0,2,4),(10,'TCL ','По своята същност Tcl е интерпретиран език което означава че всяка програма, написана на езика, може да тръгне под коя да е операционна система, за която има подготвен интерпретатор. Tcl споделя качества с други популярни интерпретирани езици като Python, Perl, Ruby, включително и обектно ориентирано програмиране. Езикът има вградени масиви и списъци, като последните, тъй като приемат за ключ обекти от произволен тип, играят ролята и на т.нар. хеш таблици. Едно от основните предимства на езика е че е значително по-лесен за научаване и употреба от останалите. Изграждането на графични интерфейси чрез Tk е забележимо лесно и просто, неимоверно по-просто в сравнение с Python например. Редица програмисти предпочитат Tcl именно заради лесното и бързо създаване на крос платформени програми с графичен интерфейс. Допълнително са налице GUI building tools като Visual Tcl и Spec Tcl, които са напълно безплатни. Понастоящем Tcl се използва от такива компании като IBM, Motorola, Oracle, TiVo и др. От сайта на Active State може да се изтегли безплатно интерпретатора който има версии за различни операционни системи – Windows, Линукс, Mac OS. В повечето от Линукс дистрибуциите, интерпретаторът е вграден и не е нужно допълнителното му сваляне. Под Линукс се извиква с командата tclsh. За да се програмират графични приложения с Tk, обаче е нужно извикването на wish.','2021-07-06 09:51:41','2021-07-06 09:51:41',0,3,4);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `e_mail` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `registration_date` datetime NOT NULL DEFAULT current_timestamp(),
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `groups_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`groups_id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `e_mail_UNIQUE` (`e_mail`),
  KEY `fk_users_groups1_idx` (`groups_id`),
  CONSTRAINT `fk_users_groups1` FOREIGN KEY (`groups_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'fidana','$2b$10$HvkQV4BANGPEENOalgq81.5v0LjSWfK8DaiZq0s4cj8Qj4PNPI3S6','root@fidana.net','Фидана','Иванова','2021-07-02 11:00:10',0,1),(2,'penka','$2b$10$miRBwqe8ZsXTswHgG/7OAu6oF8i9vdwYZIXDfC2GbRCVGYNwA3jhe','penka@fidana.net','Пенка','Пенкова','2021-07-02 11:00:57',0,2),(3,'veska','$2b$10$Wvfuk8W6AwBXbW74VBiOY.oCRjP3KH3pz4uM/99E/QDfFxqlpGgUK','veska@fidana.net','Веска','Ламерова','2021-07-02 11:01:32',1,2),(4,'elka','$2b$10$zJun0ZyGIOfAbl1kgnpO8ObJJj3UfOM9hLE5yFUKovSvMw3wqftwe','elka@fidana.net','Златна','Пампорова','2021-07-02 15:52:37',0,2),(5,'kalina','$2b$10$FfCqXExoKQosxz7IujkERupUe1nADA7fIhjT.GlsC/Lxsvyf8WlVy',NULL,NULL,NULL,'2021-07-07 19:54:22',0,2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-10 19:21:52
