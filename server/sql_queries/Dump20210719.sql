-- MariaDB dump 10.18  Distrib 10.5.8-MariaDB, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: forumdb
-- ------------------------------------------------------
-- Server version	10.5.8-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banned_users`
--

DROP TABLE IF EXISTS `banned_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banned_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `reason` varchar(45) DEFAULT NULL,
  `banned_user_id` int(11) NOT NULL,
  `banned_from_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`banned_user_id`,`banned_from_id`),
  KEY `fk_banned_users_users1_idx` (`banned_user_id`),
  KEY `fk_banned_users_users2_idx` (`banned_from_id`),
  CONSTRAINT `fk_banned_users_users1` FOREIGN KEY (`banned_user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_banned_users_users2` FOREIGN KEY (`banned_from_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banned_users`
--

LOCK TABLES `banned_users` WRITE;
/*!40000 ALTER TABLE `banned_users` DISABLE KEYS */;
INSERT INTO `banned_users` VALUES (1,'2021-07-02 11:02:58','не ме кефи',3,1);
/*!40000 ALTER TABLE `banned_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `date_created` datetime NOT NULL DEFAULT current_timestamp(),
  `date_updated` datetime NOT NULL DEFAULT current_timestamp(),
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `posts_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`posts_id`,`users_id`),
  KEY `fk_comments_posts1_idx` (`posts_id`),
  KEY `fk_comments_users1_idx` (`users_id`),
  CONSTRAINT `fk_comments_posts1` FOREIGN KEY (`posts_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,'Първоначално името на езика е било Pearl (произнася се по същия начин – „пърл“). Още преди да излезе първата версия на езика, Лари установява, че съществува език за програмиране на име PEARL. С цел да се избегнат конфликти, а и повлиян от характерната за философията на Unix тенденция към кратки имена, Лари променя името на езика на Perl, без да променя произношението му. Впоследствие се появяват интерпретации на името като съкращение, най-популярната от които е Practical Extraction and Report Language (в превод: практичен език за извличане и отчети). Съществуват и много други интерпретации, включително и такива от самия Лари Уол, като например шеговитото Pathologically Eclectic Rubbish Lister (в превод: патологично многостранен изброител на глупости). Всички те обаче са бекроними. Името на езика не е съкращение от нищо, поради което се пише само с една главна буква – Perl, а не PERL (за разлика от споменатия език PEARL, чието име наистина е съкращение). Съществува също така тънка разлика в значението в зависимост от това дали името е изписано с главна буква в началото или изцяло с малки букви. Perl е името на езика за програмиране, което, както (почти) всяко друго име, се пише с главна буква. Под perl се има предвид интерпретатора на езика Perl, т.е. програмата, която се стартира, за да се изпълни даден код на Perl. Името на интерпретатора спазва неписаното правило имената на команди под Юникс да са изцяло с малки букви.','2021-07-07 12:33:27','2021-07-07 12:33:27',0,2,1),(2,'Нов текст отново','2021-07-07 12:36:10','2021-07-18 19:06:19',0,2,4),(3,'Първоначално името на езика е било Pearl (произнася се по същия начин – „пърл“). Още преди да излезе първата версия на езика, Лари установява, че съществува език за програмиране на име PEARL. С цел да се избегнат конфликти, а и повлиян от характерната за философията на Unix тенденция към кратки имена, Лари променя името на езика на Perl, без да променя произношението му. Впоследствие се появяват интерпретации на името като съкращение, най-популярната от които е Practical Extraction and Report Language (в превод: практичен език за извличане и отчети). Съществуват и много други интерпретации, включително и такива от самия Лари Уол, като например шеговитото Pathologically Eclectic Rubbish Lister (в превод: патологично многостранен изброител на глупости). Всички те обаче са бекроними. Името на езика не е съкращение от нищо, поради което се пише само с една главна буква – Perl, а не PERL (за разлика от споменатия език PEARL, чието име наистина е съкращение). Съществува също така тънка разлика в значението в зависимост от това дали името е изписано с главна буква в началото или изцяло с малки букви. Perl е името на езика за програмиране, което, както (почти) всяко друго име, се пише с главна буква. Под perl се има предвид интерпретатора на езика Perl, т.е. програмата, която се стартира, за да се изпълни даден код на Perl. Името на интерпретатора спазва неписаното правило имената на команди под Юникс да са изцяло с малки букви.','2021-07-07 12:36:31','2021-07-07 12:36:31',0,2,2),(4,'Нов текст отново','2021-07-07 12:36:47','2021-07-11 12:02:28',0,2,4),(5,'Коментар 1','2021-07-07 13:15:15','2021-07-18 23:37:32',0,10,1),(6,'Коментар 2','2021-07-07 13:38:45','2021-07-18 21:16:33',0,10,3),(7,'Коментар 3','2021-07-07 13:38:53','2021-07-18 21:16:39',0,10,1),(8,'Коментар 4','2021-07-07 13:39:20','2021-07-18 21:16:44',0,10,1),(9,'Коментар 5','2021-07-07 14:02:52','2021-07-18 21:16:49',0,10,1),(10,'NESHTO SI TAM','2021-07-08 10:57:04','2021-07-08 10:57:04',0,4,1),(11,'NESHTO SI TAM','2021-07-08 10:58:24','2021-07-08 10:58:24',0,3,1),(12,'Коментар 1','2021-07-08 10:59:43','2021-07-18 20:34:22',0,1,1),(13,'Коментар 2','2021-07-08 11:02:18','2021-07-08 11:02:18',0,1,4),(14,'Коментар 3','2021-07-08 11:02:32','2021-07-08 11:02:32',0,1,5),(15,'Коментар 4','2021-07-08 11:02:37','2021-07-18 19:21:21',0,1,1),(16,'Коментар 5','2021-07-08 11:03:13','2021-07-15 10:50:43',0,1,1),(17,'Коментар 6','2021-07-08 22:17:38','2021-07-08 22:17:38',0,10,4),(18,'Коментар 6','2021-07-17 16:35:51','2021-07-17 16:35:51',0,1,4),(19,'Коментар 7','2021-07-17 16:41:16','2021-07-17 16:41:16',0,1,5),(20,'Коментар 8','2021-07-17 16:41:16','2021-07-17 16:41:16',0,1,2),(21,'Коментар 9','2021-07-17 16:55:30','2021-07-17 16:55:30',0,1,4),(22,'Коментар 10','2021-07-17 16:55:30','2021-07-17 16:55:30',0,1,1),(23,'Първоначално името на езика е било Pearl (произнася се по същия начин – „пърл“). Още преди да излезе първата версия на езика, Лари установява, че съществува език за програмиране на име PEARL. С цел да се избегнат конфликти, а и повлиян от характерната за философията на Unix тенденция към кратки имена, Лари променя името на езика на Perl, без да променя произношението му. Впоследствие се появяват интерпретации на името като съкращение, най-популярната от които е Practical Extraction and Report Language (в превод: практичен език за извличане и отчети). Съществуват и много други интерпретации, включително и такива от самия Лари Уол, като например шеговитото Pathologically Eclectic Rubbish Lister (в превод: патологично многостранен изброител на глупости). Всички те обаче са бекроними. Името на езика не е съкращение от нищо, поради което се пише само с една главна буква – Perl, а не PERL (за разлика от споменатия език PEARL, чието име наистина е съкращение). Съществува също така тънка разлика в значението в зависимост от това дали името е изписано с главна буква в началото или изцяло с малки букви. Perl е името на езика за програмиране, което, както (почти) всяко друго име, се пише с главна буква. Под perl се има предвид интерпретатора на езика Perl, т.е. програмата, която се стартира, за да се изпълни даден код на Perl. Името на интерпретатора спазва неписаното правило имената на команди под Юникс да са изцяло с малки букви.','2021-07-19 08:26:30','2021-07-19 08:26:30',0,10,1),(24,'TEST','2021-07-19 08:30:25','2021-07-19 08:30:25',0,26,1),(25,'test again','2021-07-19 08:31:00','2021-07-19 08:31:00',0,26,1),(26,'test3','2021-07-19 08:31:48','2021-07-19 08:31:48',0,26,1),(27,'test34','2021-07-19 08:31:59','2021-07-19 08:31:59',0,26,1),(28,'test5','2021-07-19 08:32:07','2021-07-19 08:32:07',0,26,1),(29,'test6','2021-07-19 08:32:31','2021-07-19 08:32:31',0,26,1),(30,'ok','2021-07-19 08:33:09','2021-07-19 08:33:09',1,28,1),(31,'Test2','2021-07-19 08:39:49','2021-07-19 08:39:55',0,28,1),(32,'Test','2021-07-19 08:39:59','2021-07-19 08:39:59',0,28,1),(33,'Test','2021-07-19 08:40:02','2021-07-19 08:40:02',1,28,1),(34,'Test','2021-07-19 08:40:03','2021-07-19 08:40:03',0,28,1),(35,'Test','2021-07-19 08:40:05','2021-07-19 08:40:05',0,28,1),(36,'Test','2021-07-19 08:40:07','2021-07-19 08:40:07',0,28,1),(37,'test','2021-07-19 08:40:47','2021-07-19 08:40:47',0,27,1),(38,'test','2021-07-19 08:42:16','2021-07-19 08:42:16',0,1,1),(39,'test','2021-07-19 08:42:18','2021-07-19 08:42:18',1,1,1),(40,'test','2021-07-19 08:42:20','2021-07-19 08:42:20',1,1,1),(41,'test','2021-07-19 08:42:22','2021-07-19 08:42:22',1,1,1),(42,'test','2021-07-19 08:44:18','2021-07-19 08:44:18',0,25,1),(43,'test','2021-07-19 08:44:20','2021-07-19 08:44:20',0,25,1),(44,'I\'m sure about this','2021-07-19 08:55:00','2021-07-19 08:55:17',0,5,1);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments_likes`
--

DROP TABLE IF EXISTS `comments_likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT current_timestamp(),
  `comments_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`comments_id`,`users_id`),
  KEY `fk_comments_like_comments1_idx` (`comments_id`),
  KEY `fk_comments_like_users1_idx` (`users_id`),
  CONSTRAINT `fk_comments_like_comments1` FOREIGN KEY (`comments_id`) REFERENCES `comments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_like_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=150 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments_likes`
--

LOCK TABLES `comments_likes` WRITE;
/*!40000 ALTER TABLE `comments_likes` DISABLE KEYS */;
INSERT INTO `comments_likes` VALUES (1,'2021-07-07 13:32:10',1,4),(3,'2021-07-08 10:49:05',2,1),(4,'2021-07-08 20:21:31',10,5),(6,'2021-07-08 20:22:15',11,3),(7,'2021-07-08 20:22:15',11,2),(8,'2021-07-09 10:22:49',2,4),(9,'2021-07-09 10:30:15',6,4),(10,'2021-07-09 10:30:37',10,4),(11,'2021-07-09 10:30:47',11,4),(12,'2021-07-09 10:31:16',12,4),(25,'2021-07-09 21:25:03',3,4),(26,'2021-07-17 12:41:28',14,2),(75,'2021-07-17 17:58:00',19,1),(87,'2021-07-17 18:02:46',21,2),(88,'2021-07-17 18:02:46',21,4),(89,'2021-07-17 18:02:46',21,5),(103,'2021-07-17 22:09:33',20,1),(105,'2021-07-17 22:18:22',3,1),(108,'2021-07-17 22:21:35',21,1),(113,'2021-07-18 12:47:31',12,2),(114,'2021-07-18 12:47:33',13,2),(115,'2021-07-18 12:47:39',15,2),(116,'2021-07-18 12:47:40',16,2),(117,'2021-07-18 12:47:41',18,2),(120,'2021-07-18 12:47:47',19,2),(139,'2021-07-18 22:37:15',17,1),(141,'2021-07-18 22:51:37',22,1),(145,'2021-07-18 23:10:18',18,1),(149,'2021-07-19 18:21:47',13,1);
/*!40000 ALTER TABLE `comments_likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flag_comments`
--

DROP TABLE IF EXISTS `flag_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flag_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `checked_admin` tinyint(1) NOT NULL DEFAULT 0,
  `checked_owner` tinyint(1) NOT NULL DEFAULT 0,
  `approved` tinyint(1) NOT NULL DEFAULT 0,
  `comments_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`comments_id`,`users_id`),
  KEY `fk_flag_comments_comments1_idx` (`comments_id`),
  KEY `fk_flag_comments_users1_idx` (`users_id`),
  CONSTRAINT `fk_flag_comments_comments1` FOREIGN KEY (`comments_id`) REFERENCES `comments` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_flag_comments_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flag_comments`
--

LOCK TABLES `flag_comments` WRITE;
/*!40000 ALTER TABLE `flag_comments` DISABLE KEYS */;
INSERT INTO `flag_comments` VALUES (14,'2021-07-10 16:54:56',0,0,0,3,1),(15,'2021-07-10 17:28:42',1,0,1,2,1),(19,'2021-07-11 11:59:43',1,0,1,4,1);
/*!40000 ALTER TABLE `flag_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flag_posts`
--

DROP TABLE IF EXISTS `flag_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flag_posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT current_timestamp(),
  `posts_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`posts_id`,`users_id`),
  KEY `fk_flag_posts_posts1_idx` (`posts_id`),
  KEY `fk_flag_posts_users1_idx` (`users_id`),
  CONSTRAINT `fk_flag_posts_posts1` FOREIGN KEY (`posts_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_flag_posts_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flag_posts`
--

LOCK TABLES `flag_posts` WRITE;
/*!40000 ALTER TABLE `flag_posts` DISABLE KEYS */;
/*!40000 ALTER TABLE `flag_posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `friends`
--

DROP TABLE IF EXISTS `friends`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `friends` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_confirm` tinyint(1) DEFAULT 1,
  `reciver_confirm` tinyint(1) DEFAULT 1,
  `start_date` datetime NOT NULL DEFAULT current_timestamp(),
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  PRIMARY KEY (`id`,`sender`,`receiver`),
  KEY `fk_friends_users1_idx` (`sender`),
  KEY `fk_friends_users2_idx` (`receiver`),
  CONSTRAINT `fk_friends_users1` FOREIGN KEY (`sender`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_friends_users2` FOREIGN KEY (`receiver`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `friends`
--

LOCK TABLES `friends` WRITE;
/*!40000 ALTER TABLE `friends` DISABLE KEYS */;
INSERT INTO `friends` VALUES (2,1,1,'2021-07-19 10:19:28',1,3),(7,1,1,'2021-07-19 11:26:36',1,2),(8,1,1,'2021-07-19 11:26:54',1,4),(9,1,1,'2021-07-19 11:36:35',4,1);
/*!40000 ALTER TABLE `friends` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `groups`
--

LOCK TABLES `groups` WRITE;
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` VALUES (1,'admin'),(2,'user');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `locked_post`
--

DROP TABLE IF EXISTS `locked_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `locked_post` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT current_timestamp(),
  `reason` varchar(45) DEFAULT NULL,
  `posts_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`posts_id`,`users_id`),
  KEY `fk_locked_post_posts_idx` (`posts_id`),
  KEY `fk_locked_post_users1_idx` (`users_id`),
  CONSTRAINT `fk_locked_post_posts` FOREIGN KEY (`posts_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_locked_post_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `locked_post`
--

LOCK TABLES `locked_post` WRITE;
/*!40000 ALTER TABLE `locked_post` DISABLE KEYS */;
INSERT INTO `locked_post` VALUES (1,'2021-07-06 19:52:05','none',3,1),(45,'2021-07-17 22:22:06','neznam',7,1),(90,'2021-07-19 08:50:11','',25,1),(94,'2021-07-19 08:56:16','',24,1);
/*!40000 ALTER TABLE `locked_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_cathegory`
--

DROP TABLE IF EXISTS `post_cathegory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_cathegory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_cathegory`
--

LOCK TABLES `post_cathegory` WRITE;
/*!40000 ALTER TABLE `post_cathegory` DISABLE KEYS */;
INSERT INTO `post_cathegory` VALUES (1,'JavaScript',0),(2,'PHP',0),(3,'TCL',0),(4,'Perl',0),(5,'CPP',0);
/*!40000 ALTER TABLE `post_cathegory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_likes`
--

DROP TABLE IF EXISTS `post_likes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_likes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime DEFAULT current_timestamp(),
  `posts_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`posts_id`,`users_id`),
  KEY `fk_post_likes_posts1_idx` (`posts_id`),
  KEY `fk_post_likes_users1_idx` (`users_id`),
  CONSTRAINT `fk_post_likes_posts1` FOREIGN KEY (`posts_id`) REFERENCES `posts` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_post_likes_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_likes`
--

LOCK TABLES `post_likes` WRITE;
/*!40000 ALTER TABLE `post_likes` DISABLE KEYS */;
INSERT INTO `post_likes` VALUES (1,'2021-07-13 10:29:56',1,2),(8,'2021-07-13 11:44:43',10,3),(9,'2021-07-13 11:45:03',10,5),(11,'2021-07-14 10:48:11',8,2),(12,'2021-07-14 10:48:25',8,4),(13,'2021-07-14 10:49:09',4,1),(14,'2021-07-14 10:49:09',4,2),(15,'2021-07-14 10:49:09',4,3),(16,'2021-07-14 10:49:09',4,4),(91,'2021-07-17 18:14:35',8,1),(94,'2021-07-18 12:45:13',5,2),(96,'2021-07-18 12:47:20',21,2),(97,'2021-07-18 12:47:24',18,2),(98,'2021-07-18 13:50:52',25,2),(107,'2021-07-18 19:24:20',27,1),(109,'2021-07-18 22:37:01',10,1),(110,'2021-07-19 08:39:42',28,1);
/*!40000 ALTER TABLE `post_likes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `text` text NOT NULL,
  `date_created` datetime DEFAULT current_timestamp(),
  `date_updated` datetime DEFAULT current_timestamp(),
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `post_cathegory_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`post_cathegory_id`,`users_id`),
  KEY `fk_posts_post_cathegory1_idx` (`post_cathegory_id`),
  KEY `fk_posts_users1_idx` (`users_id`),
  CONSTRAINT `fk_posts_post_cathegory1` FOREIGN KEY (`post_cathegory_id`) REFERENCES `post_cathegory` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_posts_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'TCL ','По своята същност Tcl е интерпретиран език което означава че всяка програма, написана на езика, може да тръгне под коя да е операционна система, за която има подготвен интерпретатор. Tcl споделя качества с други популярни интерпретирани езици като Python, Perl, Ruby, включително и обектно ориентирано програмиране. Езикът има вградени масиви и списъци, като последните, тъй като приемат за ключ обекти от произволен тип, играят ролята и на т.нар. хеш таблици. Едно от основните предимства на езика е че е значително по-лесен за научаване и употреба от останалите. Изграждането на графични интерфейси чрез Tk е забележимо лесно и просто, неимоверно по-просто в сравнение с Python например. Редица програмисти предпочитат Tcl именно заради лесното и бързо създаване на крос платформени програми с графичен интерфейс. Допълнително са налице GUI building tools като Visual Tcl и Spec Tcl, които са напълно безплатни. Понастоящем Tcl се използва от такива компании като IBM, Motorola, Oracle, TiVo и др. От сайта на Active State може да се изтегли безплатно интерпретатора който има версии за различни операционни системи – Windows, Линукс, Mac OS. В повечето от Линукс дистрибуциите, интерпретаторът е вграден и не е нужно допълнителното му сваляне. Под Линукс се извиква с командата tclsh. За да се програмират графични приложения с Tk, обаче е нужно извикването на wish. Da be','2021-10-11 00:00:00','2021-07-18 22:44:16',0,1,1),(2,'Promises','I\'m feel like in dark fog','2021-07-03 13:32:07','2021-07-03 13:32:07',0,3,1),(3,'Objects','I\'m feel like in dark fog','2021-07-03 13:34:32','2021-07-03 13:34:32',1,2,4),(4,'Arrays','I\'m feel like in dark fog','2021-07-03 13:35:20','2021-07-03 13:35:20',0,2,1),(5,'Classes','I\'m feel like in dark fog 2','2021-07-03 13:37:19','2021-07-18 23:40:07',0,4,1),(7,'нещо ново','Тралала - трилала','2021-07-04 20:26:58','2021-07-04 20:26:58',0,2,1),(8,'Правя някакъв опит','Тралала - трилала','2021-07-05 11:16:49','2021-07-05 11:16:49',0,2,4),(9,'Заглавие','Тралала - трилала','2021-07-05 11:49:23','2021-07-05 11:49:23',0,2,4),(10,'TCL ','По своята същност Tcl е интерпретиран език което означава че всяка програма, написана на езика, може да тръгне под коя да е операционна система, за която има подготвен интерпретатор. Tcl споделя качества с други популярни интерпретирани езици като Python, Perl, Ruby, включително и обектно ориентирано програмиране. Езикът има вградени масиви и списъци, като последните, тъй като приемат за ключ обекти от произволен тип, играят ролята и на т.нар. хеш таблици. Едно от основните предимства на езика е че е значително по-лесен за научаване и употреба от останалите. Изграждането на графични интерфейси чрез Tk е забележимо лесно и просто, неимоверно по-просто в сравнение с Python например. Редица програмисти предпочитат Tcl именно заради лесното и бързо създаване на крос платформени програми с графичен интерфейс. Допълнително са налице GUI building tools като Visual Tcl и Spec Tcl, които са напълно безплатни. Понастоящем Tcl се използва от такива компании като IBM, Motorola, Oracle, TiVo и др. От сайта на Active State може да се изтегли безплатно интерпретатора който има версии за различни операционни системи – Windows, Линукс, Mac OS. В повечето от Линукс дистрибуциите, интерпретаторът е вграден и не е нужно допълнителното му сваляне. Под Линукс се извиква с командата tclsh. За да се програмират графични приложения с Tk, обаче е нужно извикването на wish.','2021-07-06 09:51:41','2021-07-06 09:51:41',0,3,4),(11,'TCL1 ','По своята същност Tcl е интерпретиран език което означава че всяка програма, написана на езика, може да тръгне под коя да е операционна система, за която има подготвен интерпретатор. Tcl споделя качества с други популярни интерпретирани езици като Python, Perl, Ruby, включително и обектно ориентирано програмиране. Езикът има вградени масиви и списъци, като последните, тъй като приемат за ключ обекти от произволен тип, играят ролята и на т.нар. хеш таблици. Едно от основните предимства на езика е че е значително по-лесен за научаване и употреба от останалите. Изграждането на графични интерфейси чрез Tk е забележимо лесно и просто, неимоверно по-просто в сравнение с Python например. Редица програмисти предпочитат Tcl именно заради лесното и бързо създаване на крос платформени програми с графичен интерфейс. Допълнително са налице GUI building tools като Visual Tcl и Spec Tcl, които са напълно безплатни. Понастоящем Tcl се използва от такива компании като IBM, Motorola, Oracle, TiVo и др. От сайта на Active State може да се изтегли безплатно интерпретатора който има версии за различни операционни системи – Windows, Линукс, Mac OS. В повечето от Линукс дистрибуциите, интерпретаторът е вграден и не е нужно допълнителното му сваляне. Под Линукс се извиква с командата tclsh. За да се програмират графични приложения с Tk, обаче е нужно извикването на wish.','2021-07-13 11:37:41','2021-07-13 11:37:41',0,3,1),(12,'TCL4','По своята същност Tcl е интерпретиран език което означава че всяка програма, написана на езика, може да тръгне под коя да е операционна система, за която има подготвен интерпретатор. Tcl споделя качества с други популярни интерпретирани езици като Python, Perl, Ruby, включително и обектно ориентирано програмиране. Езикът има вградени масиви и списъци, като последните, тъй като приемат за ключ обекти от произволен тип, играят ролята и на т.нар. хеш таблици. Едно от основните предимства на езика е че е значително по-лесен за научаване и употреба от останалите. Изграждането на графични интерфейси чрез Tk е забележимо лесно и просто, неимоверно по-просто в сравнение с Python например. Редица програмисти предпочитат Tcl именно заради лесното и бързо създаване на крос платформени програми с графичен интерфейс. Допълнително са налице GUI building tools като Visual Tcl и Spec Tcl, които са напълно безплатни. Понастоящем Tcl се използва от такива компании като IBM, Motorola, Oracle, TiVo и др. От сайта на Active State може да се изтегли безплатно интерпретатора който има версии за различни операционни системи – Windows, Линукс, Mac OS. В повечето от Линукс дистрибуциите, интерпретаторът е вграден и не е нужно допълнителното му сваляне. Под Линукс се извиква с командата tclsh. За да се програмират графични приложения с Tk, обаче е нужно извикването на wish. Da ','2021-07-13 11:39:05','2021-07-18 19:10:44',1,3,1),(13,'None','По своята същност Tcl е интерпретиран език което означава че всяка програма, написана на езика, може да тръгне под коя да е операционна система, за която има подготвен интерпретатор. Tcl споделя качества с други популярни интерпретирани езици като Python, Perl, Ruby, включително и обектно ориентирано програмиране. Езикът има вградени масиви и списъци, като последните, тъй като приемат за ключ обекти от произволен тип, играят ролята и на т.нар. хеш таблици. Едно от основните предимства на езика е че е значително по-лесен за научаване и употреба от останалите. Изграждането на графични интерфейси чрез Tk е забележимо лесно и просто, неимоверно по-просто в сравнение с Python например. Редица програмисти предпочитат Tcl именно заради лесното и бързо създаване на крос платформени програми с графичен интерфейс. Допълнително са налице GUI building tools като Visual Tcl и Spec Tcl, които са напълно безплатни. Понастоящем Tcl се използва от такива компании като IBM, Motorola, Oracle, TiVo и др. От сайта на Active State може да се изтегли безплатно интерпретатора който има версии за различни операционни системи – Windows, Линукс, Mac OS. В повечето от Линукс дистрибуциите, интерпретаторът е вграден и не е нужно допълнителното му сваляне. Под Линукс се извиква с командата tclsh. За да се програмират графични приложения с Tk, обаче е нужно извикването на wish. I kakvo?','2021-07-13 11:40:51','2021-07-18 23:39:29',0,3,1),(14,'None1','По своята същност Tcl е интерпретиран език което означава че всяка програма, написана на езика, може да тръгне под коя да е операционна система, за която има подготвен интерпретатор. Tcl споделя качества с други популярни интерпретирани езици като Python, Perl, Ruby, включително и обектно ориентирано програмиране. Езикът има вградени масиви и списъци, като последните, тъй като приемат за ключ обекти от произволен тип, играят ролята и на т.нар. хеш таблици. Едно от основните предимства на езика е че е значително по-лесен за научаване и употреба от останалите. Изграждането на графични интерфейси чрез Tk е забележимо лесно и просто, неимоверно по-просто в сравнение с Python например. Редица програмисти предпочитат Tcl именно заради лесното и бързо създаване на крос платформени програми с графичен интерфейс. Допълнително са налице GUI building tools като Visual Tcl и Spec Tcl, които са напълно безплатни. Понастоящем Tcl се използва от такива компании като IBM, Motorola, Oracle, TiVo и др. От сайта на Active State може да се изтегли безплатно интерпретатора който има версии за различни операционни системи – Windows, Линукс, Mac OS. В повечето от Линукс дистрибуциите, интерпретаторът е вграден и не е нужно допълнителното му сваляне. Под Линукс се извиква с командата tclsh. За да се програмират графични приложения с Tk, обаче е нужно извикването на wish.','2021-07-13 11:41:20','2021-07-13 11:41:20',1,3,1),(15,'None2','По своята същност Tcl е интерпретиран език което означава че всяка програма, написана на езика, може да тръгне под коя да е операционна система, за която има подготвен интерпретатор. Tcl споделя качества с други популярни интерпретирани езици като Python, Perl, Ruby, включително и обектно ориентирано програмиране. Езикът има вградени масиви и списъци, като последните, тъй като приемат за ключ обекти от произволен тип, играят ролята и на т.нар. хеш таблици. Едно от основните предимства на езика е че е значително по-лесен за научаване и употреба от останалите. Изграждането на графични интерфейси чрез Tk е забележимо лесно и просто, неимоверно по-просто в сравнение с Python например. Редица програмисти предпочитат Tcl именно заради лесното и бързо създаване на крос платформени програми с графичен интерфейс. Допълнително са налице GUI building tools като Visual Tcl и Spec Tcl, които са напълно безплатни. Понастоящем Tcl се използва от такива компании като IBM, Motorola, Oracle, TiVo и др. От сайта на Active State може да се изтегли безплатно интерпретатора който има версии за различни операционни системи – Windows, Линукс, Mac OS. В повечето от Линукс дистрибуциите, интерпретаторът е вграден и не е нужно допълнителното му сваляне. Под Линукс се извиква с командата tclsh. За да се програмират графични приложения с Tk, обаче е нужно извикването на wish.','2021-07-13 11:42:22','2021-07-13 11:42:22',0,5,1),(16,'None3','По своята същност Tcl е интерпретиран език което означава че всяка програма, написана на езика, може да тръгне под коя да е операционна система, за която има подготвен интерпретатор. Tcl споделя качества с други популярни интерпретирани езици като Python, Perl, Ruby, включително и обектно ориентирано програмиране. Езикът има вградени масиви и списъци, като последните, тъй като приемат за ключ обекти от произволен тип, играят ролята и на т.нар. хеш таблици. Едно от основните предимства на езика е че е значително по-лесен за научаване и употреба от останалите. Изграждането на графични интерфейси чрез Tk е забележимо лесно и просто, неимоверно по-просто в сравнение с Python например. Редица програмисти предпочитат Tcl именно заради лесното и бързо създаване на крос платформени програми с графичен интерфейс. Допълнително са налице GUI building tools като Visual Tcl и Spec Tcl, които са напълно безплатни. Понастоящем Tcl се използва от такива компании като IBM, Motorola, Oracle, TiVo и др. От сайта на Active State може да се изтегли безплатно интерпретатора който има версии за различни операционни системи – Windows, Линукс, Mac OS. В повечето от Линукс дистрибуциите, интерпретаторът е вграден и не е нужно допълнителното му сваляне. Под Линукс се извиква с командата tclsh. За да се програмират графични приложения с Tk, обаче е нужно извикването на wish.','2021-07-13 11:42:46','2021-07-13 11:42:46',1,3,1),(17,'Нещо си там','Нещо си там','2021-07-18 12:15:01','2021-07-18 12:15:01',1,3,1),(18,'Проба','Neshto','2021-07-18 12:17:28','2021-07-18 12:17:28',1,1,1),(19,'Още една проба','The useLocation hook returns the location object that represents the current URL. You can think about it like a useState that returns a new location whenever the URL changes.This could be really useful e.g. in a situation where you would like to trigger a new “page view” event using your web analytics tool whenever a new page loads, as in the following example:','2021-07-18 12:20:30','2021-07-18 12:20:30',1,1,1),(20,'TCL 5','По своята същност Tcl е интерпретиран език което означава че всяка програма, написана на езика, може да тръгне под коя да е операционна система, за която има подготвен интерпретатор. Tcl споделя качества с други популярни интерпретирани езици като Python, Perl, Ruby, включително и обектно ориентирано програмиране. Езикът има вградени масиви и списъци, като последните, тъй като приемат за ключ обекти от произволен тип, играят ролята и на т.нар. хеш таблици. Едно от основните предимства на езика е че е значително по-лесен за научаване и употреба от останалите. Изграждането на графични интерфейси чрез Tk е забележимо лесно и просто, неимоверно по-просто в сравнение с Python например. Редица програмисти предпочитат Tcl именно заради лесното и бързо създаване на крос платформени програми с графичен интерфейс. Допълнително са налице GUI building tools като Visual Tcl и Spec Tcl, които са напълно безплатни. Понастоящем Tcl се използва от такива компании като IBM, Motorola, Oracle, TiVo и др. От сайта на Active State може да се изтегли безплатно интерпретатора който има версии за различни операционни системи – Windows, Линукс, Mac OS. В повечето от Линукс дистрибуциите, интерпретаторът е вграден и не е нужно допълнителното му сваляне. Под Линукс се извиква с командата tclsh. За да се програмират графични приложения с Tk, обаче е нужно извикването на wish.','2021-07-18 12:21:35','2021-07-18 12:21:35',1,3,1),(21,'Проба2','12334','2021-07-18 12:22:26','2021-07-18 12:22:26',0,1,1),(22,'Проба3','123','2021-07-18 12:22:49','2021-07-18 12:22:49',0,1,1),(23,'Проба34','212','2021-07-18 12:24:24','2021-07-18 12:24:24',0,1,1),(24,'Проба6','12','2021-07-18 12:27:48','2021-07-18 12:27:48',0,1,1),(25,'Last test I hope','Q*bert /ˈkjuːbərt/ is an arcade game developed and published for the North American market by Gottlieb in 1982. It is a 2D action game with puzzle elements that uses isometric graphics to create a pseudo-3D effect. The objective of each level in the game is to change every cube in a pyramid to a target color by making Q*bert, the on-screen character, hop on top of the cube while avoiding obstacles and enemies. Players use a joystick to control the character.\n\nThe game was conceived by Warren Davis and Jeff Lee. Lee designed the title character and original concept, which was further developed and implemented by Davis. Q*bert was developed under the project name Cubes.[1]\n\nQ*bert was well-received in arcades and among critics. The game was Gottlieb\'s most successful video game and is among the most recognized brands from the golden age of arcade games. It has been ported to numerous platforms. The game\'s success resulted in sequels and the use of the character\'s likeness in merchandising, such as appearances on lunch boxes, toys, and an animated television show. The Q*bert character became known for his \"swearing\" – an incoherent phrase made of synthesized speech generated by the sound chip and a speech balloon of nonsensical characters that appear when he collides with an enemy.','2021-07-18 12:36:03','2021-07-18 12:36:03',0,1,1),(26,'One more test','Nehsto si tam','2021-07-18 12:45:33','2021-07-18 12:45:33',0,2,2),(27,'Again test','CSS (Cascading Style Sheets) е език за описание на стилове (език за стилови листове, style sheet language) – използва се основно за описване на представянето на документ, написан на език за маркиране. Най-често се използва заедно с HTML, но може да се приложи върху произволен XML документ. Официално спецификацията на CSS се поддържа от W3C.\n\nCSS е създаден с цел да бъдат разделени съдържанието и структурата на уеб страниците отделно от тяхното визуално представяне. Преди стандартите за CSS, установени от W3C през 1995 г., съдържанието на сайтовете и стила на техния дизайн са писани в една и съща HTML страницата. В резултат на това HTML кодът се превръща в сложен и нечетлив, а всяка промяна в проекта на даден сайт изисквала корекцията да бъде нанасяна в целия сайт страница по страница. Използвайки CSS, настройките за форматиране могат да бъдат поставени в един-единствен файл и тогава промяната ще бъде отразена едновременно на всички страници, които използват този CSS файл.','2021-07-18 12:46:38','2021-07-18 14:12:40',0,1,2),(28,'One more post','123','2021-07-18 13:31:50','2021-07-18 13:31:50',0,5,2);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `e_mail` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `registration_date` datetime NOT NULL DEFAULT current_timestamp(),
  `is_deleted` tinyint(1) NOT NULL DEFAULT 0,
  `groups_id` int(11) NOT NULL,
  PRIMARY KEY (`id`,`groups_id`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  UNIQUE KEY `e_mail_UNIQUE` (`e_mail`),
  KEY `fk_users_groups1_idx` (`groups_id`),
  CONSTRAINT `fk_users_groups1` FOREIGN KEY (`groups_id`) REFERENCES `groups` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'fidana','$2b$10$HvkQV4BANGPEENOalgq81.5v0LjSWfK8DaiZq0s4cj8Qj4PNPI3S6','root@fidana.net','Фидана','Иванова','2021-07-02 11:00:10',0,1),(2,'penka','$2b$10$miRBwqe8ZsXTswHgG/7OAu6oF8i9vdwYZIXDfC2GbRCVGYNwA3jhe','penka@fidana.net','Пенка','Пенкова','2021-07-02 11:00:57',0,2),(3,'veska','$2b$10$Wvfuk8W6AwBXbW74VBiOY.oCRjP3KH3pz4uM/99E/QDfFxqlpGgUK','veska@fidana.net','Веска','Ламерова','2021-07-02 11:01:32',1,2),(4,'elka','$2b$10$zJun0ZyGIOfAbl1kgnpO8ObJJj3UfOM9hLE5yFUKovSvMw3wqftwe','elka@fidana.net','Златна','Пампорова','2021-07-02 15:52:37',0,2),(5,'kalina','$2b$10$FfCqXExoKQosxz7IujkERupUe1nADA7fIhjT.GlsC/Lxsvyf8WlVy',NULL,NULL,NULL,'2021-07-07 19:54:22',0,2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-19 19:23:15
