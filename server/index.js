import express from 'express';
import cors from 'cors';
import dotenv from 'dotenv';
import passport from 'passport';
import jwtStrategy from './src/auth/strategy.js';
import helmet from 'helmet';
import { userRoute } from './src/controllers/users-controller.js';
import { postRouter } from './src/controllers/posts-controller.js';
import { commentRouter } from './src/controllers/comments-controller.js';
import { storeFirstParams } from './src/middlewares/store-first-params.js';

const constants = dotenv.config().parsed;
const PORT = parseInt(constants.PORT);

passport.use(jwtStrategy);

const app = express();

app.use(cors());
app.use(helmet());
app.use(express.json());

app.use('/api/users', userRoute);
app.use('/api/posts', postRouter);
app.use('/api/posts/:postId/comments', storeFirstParams, commentRouter);

app.listen(PORT, console.log(`Server is arm and ready on port ${PORT}`));