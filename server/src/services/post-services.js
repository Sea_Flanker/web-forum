import errors from './errors.js';
import * as postFunctions from '../data/post-data.js';
import usersGroups from '../common/users-groups.js';
import { addNewActivity } from '../data/user-data.js';

/**
 * Create group.
 * @param {postFunctions} postData 
 * @returns 
 */
export const createGroup = (postData) => async (groupName) => {
  if (await postData.checkGroupExist(groupName)) {
    return {
      error: errors.DUPLICATE_RECORD,
      data: null,
    };
  }

  const result = await postData.createPostGroup(groupName);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

/**
 * Update group by id.
 * @param {postFunctions} postData 
 * @returns 
 */
export const updateGroup = (postData) => async (data) => {

  // if (await postData.checkGroupExist(data.body.category)) {
  //   return {
  //     error: errors.DUPLICATE_RECORD,
  //     data: null,
  //   };
  // }

  if (!(await postData.checkGroupById(data.params.id))) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  };

  const result = await postData.updateGroupById(data.body.category, data.params.id);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

/**
 * Delete group by id.
 * @param {postFunctions} postData 
 * @returns 
 */

export const deleteGroup = (postData) => async(groupId) => {
  if (!(await postData.checkGroupById(groupId))) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  };

  const result = await postData.deleteGroupById(groupId);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  return {
    error: null,
    data: result,
  };
};


/**
 * Change the category of a single post, by it's id.
 * @param { postFunctions } postData 
 * @returns 
 */
export const changePostGroup = (postData) => async (userId, groupId, postId) => {
  if (!await postData.checkGroupById(groupId)) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }

  const {post_cathegory_id} = await postData.getPostGroup(postId);
  if (post_cathegory_id === groupId) {
    return {
      error: errors.DUPLICATE_RECORD,
      data: null,
    };
  }

  const result = await postData.movePostInOtherGroup(groupId, postId);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  }

  const addInfo = await addAdditionalInfo(postData)(userId, postId);
  return {
    error: null,
    data: {...result[0], ...addInfo},
  }; 
};

/**
 * Create post.
 * @param {postFunctions} postData
 */
export const createNewPost = (postData) => async (postInfo, authorInfo) => {
  if (await postData.checkPostNameExist(postInfo.title)) {
    return {
      error: errors.DUPLICATE_RECORD,
      data: null,
    };
  }

  const result = await postData.createNewPost(postInfo, authorInfo);
  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  }

  //Saves activity
  // addNewActivity(authorInfo.id, 'Created a post');
  

  const addInfo = await addAdditionalInfo(postData)(authorInfo.id, result[0].id);
  return {
    error: null,
    data: {...result[0], ...addInfo},
  }; 
};

/**
 * Get all posts. Query parameter count is used to limit how many posts 
 * will be returned for every group.
 * @param {*} postData 
 * @returns 
 */
export const getAllPosts = (postData) => async (queryData) => {
  if (!queryData.count) {
    queryData.count = 5;
  };

  if (+queryData.count < 0) {
    return {
      error: errors.OPERATION_NOT_ALLOWED,
      data: null,
    };
  };
  
  const result = await postData.getAllPostsByGroup(queryData.count);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  return {
    error: null,
    data: result,
  };
};

/**
 * Get a post by id.
 * @param {*} postData 
 * @returns 
 */
export const getSinglePost = (postData) => async (userId, postId) => {
  if (!postId) {
    return {
      error: errors.OPERATION_NOT_ALLOWED,
      data: null,
    };
  };

  const result = await postData.getPostsByID(postId);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };
  
  if (!result.length) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  };

  const addInfo = await addAdditionalInfo(postData)(userId, postId);
  return {
    error: null,
    data: {...result[0], ...addInfo},
  };

};

/**
 * Delete post by id.
 * @param {*} postData 
 * @returns 
 */
export const deleteSinglePost = (postData) => async (userId, postId) => {
  const result = await postData.deletePostById(postId);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  const addInfo = await addAdditionalInfo(postData)(userId, postId);
  return {
    error: null,
    data: {...result[0], ...addInfo},
  };
};

/**
 * Flag post by id. 
 * @param {*} postData 
 * @returns 
 */
export const flagSinglePost = (postData) => async (userId, postId) => {  
  if (!(await postData.checkPostExistById(postId))) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  };

  const result = await postData.flagPostById(postId);
  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  }

  return {
    error: null,
    data: result,
  };
};

/**
 * Lock post
 * @param { postFunctions } postData 
 * @returns 
 */
export const lockPost = (postData) => async (data) => {
  
  const checkPost = await postData.checkPostLock(data.postId);
  
  if (checkPost) {
    return {
      error: errors.DUPLICATE_RECORD,
      data: null,
    };
  };

  const result = await postData.lockPostById(data);
  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  const addInfo = await addAdditionalInfo(postData)(data.userId, data.postId);
  return {
    error: null,
    data: {...result[0], ...addInfo},
  };
};

/**
 * Unlock post
 * @param {*} postData 
 * @returns 
 */
export const unlockPost = (postData) => async (data) => {
  const checkPost = await postData.checkPostLock(data.postId);
  
  if (!checkPost) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }

  const whoLocked = await postData.getUserGroup(checkPost);
  
  if (whoLocked !== data.userGroup && data.userGroup !== usersGroups.ADMIN) {
    return {
      error: errors.OPERATION_NOT_ALLOWED,
      data: null,
    };
  }

  const result = await postData.unlockPostById(data);
  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  const addInfo = await addAdditionalInfo(postData)(data.userId, data.postId);
  return {
    error: null,
    data: {...result[0], ...addInfo},
  };
};


/**
 * Like post
 * @param {*} postData 
 * @returns 
 */
export const likePostById = (postData) => async (userId, postId) => {
  const checkPost = await postData.checkPostExistById(postId);

  if (!checkPost) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }


  const checkLocked = await postData.checkPostLock(postId);

  if (checkLocked) {
    return {
      error: errors.LOCKED_POST,
      data: null,
    };
  }


  const checkLike = await postData.checkUserLikeThisPost(userId, postId);
  
  if (checkLike) {
    return {
      error: errors.DUPLICATE_RECORD,
      data: null,
    };
  }
  
  const {users_id} = (await postData.getAuthorId(postId))[0];
  
  if (users_id === userId) {
    return {
      error: errors.OPERATION_NOT_ALLOWED,
      data: null,
    };
  }

  const result = await postData.likePost(postId, userId);
  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  const addInfo = await addAdditionalInfo(postData)(userId, postId);
  return {
    error: null,
    data: {...result[0], ...addInfo},
  };
};


/**
 * Insert additional info for post
 * @param {*} postData 
 * @returns 
 */
const addAdditionalInfo = (postData) => async (userId, postId) => {
  const postOwner = await postData.getAuthorId(postId);
  const infoObject = {
    postIsMine: postOwner[0].users_id === userId,
    locked: await postData.checkPostLock(postId) ? true : false,
    lockedReason: await postData.getLockedReason(postId),
    likedByMe: await postData.checkUserLikeThisPost(userId, postId),
    likes: await postData.postLikesCount(postId),
    whoLike: await postData.whoLikeThisPost(postId),
    comments: await postData.postCommentsCount(postId),
  };

  return infoObject;
};


/**
 * unLike post
 * @param {*} postData 
 * @returns 
 */
export const unLikePostById = (postData) => async (userId, postId) => {
  const checkPost = await postData.checkPostExistById(postId);

  if (!checkPost) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }


  const checkLocked = await postData.checkPostLock(postId);

  if (checkLocked) {
    return {
      error: errors.LOCKED_POST,
      data: null,
    };
  }


  const checkLike = await postData.checkUserLikeThisPost(userId, postId);
  
  if (!checkLike) {
    return {
      error: errors.DUPLICATE_RECORD,
      data: null,
    };
  }
  
  const {users_id} = (await postData.getAuthorId(postId))[0];
  
  if (users_id === userId) {
    return {
      error: errors.OPERATION_NOT_ALLOWED,
      data: null,
    };
  }

  const result = await postData.unLikePost(postId, userId);
  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  const addInfo = await addAdditionalInfo(postData)(userId, postId);
  return {
    error: null,
    data: {...result[0], ...addInfo},
  };
};


/**
 * Get all posts from group by groupId
 * @param {*} postData 
 * @returns 
 */
export const getAllPostsByGroupId = (postData) => async (userId, groupId, start, count) => {
  if (!await postData.checkGroupById(groupId)) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  }

  const result = await postData.postsFromCategory(userId, groupId, start, count);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  const final = await Promise.all(result.map(async (x) => {
    const isLocked = (await postData.checkPostLock(x.postId)) ? true : false;
    return {...x, isLocked};
  }));  

  return {
    error: null,
    data: final,
  };

};


/**
 * Update post by id
 * @param {*} postData 
 * @returns 
 */
export const updatePostById = (postData) => async (userId, postId, bodyParams) => {  
  const checkLocked = await postData.checkPostLock(postId);
  
  if (checkLocked) {
    return {
      error: errors.LOCKED_POST,
      data: null,
    };
  }

  const result = await postData.updatePost(postId, bodyParams);
  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  const addInfo = await addAdditionalInfo(postData)(userId, postId);
  return {
    error: null,
    data: {...result[0], ...addInfo},
  };
};