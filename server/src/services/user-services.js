import errors from './errors.js';
import userGroups from '../common/users-groups.js';
import bcrypt from 'bcrypt';
import { logServer } from '../common/help-functions.js';
import { addNewActivity, checkOldTokens } from '../data/user-data.js';

/**
 * Register user
 * @param {*} userData 
 * @returns 
 */
export const registerUser = (userData) => async (user) => {
  if (await userData.hasThisUser(user.username)) {    
    return {
      error: errors.DUPLICATE_RECORD,
      data: null,
    };
  };

  const createdUser = await userData.createUser(user, userGroups.USER);
  return {
    error: null,
    data: createdUser,
  };
};

/**
 * Password check
 * @param {*} userData 
 * @returns 
 */
export const userPassCheck = (userData) => async (user) => {
  // Check user exist
  if (!await userData.hasThisUser(user.username)) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  };

  // Check for user in banned list
  const checkForBan = await userData.checkUserForBan(user.username);
  if (checkForBan.length) {
    return {
      error: errors.BANNED_USER,
      data: checkForBan[0],
    };
  };

  // Check user password
  const checkPass = await userData.getUserByUsername(user.username);
  
  if (await bcrypt.compare(user.password, checkPass.password)) {
    return {
      error: null,
      data: checkPass,
    };
  } else {
    checkPass.groups_id === userGroups.ADMIN ? logServer.red('INVALID ADMIN LOGIN!') : null;
    return {
      error: errors.WRONG_CREDENTIALS,
      data: null,
    };
  }
  
};

/**
 * Update user info
 * @param {*} userData 
 * @returns 
 */
export const updateUserInfo = (userData) => async (userInfo, body) => {
  const checkMail = await userData.checkEmailDuplicate(userInfo.id, body.email);
  if (checkMail) {
    return {
      error: errors.DUPLICATE_EMAIL,
      data: null,
    };
  };

  const updatedInfo = await userData.updateUserInfo(userInfo, body);

  if (!updatedInfo) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  return {
    error: null,
    data: updatedInfo,
  };

};

/**
 * Search for a user
 * @param {*} userData 
 * @returns 
 */
export const searchForUser = (userData) => async (parameter) => {
  const result = await userData.searchInUsers(parameter);
  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null
    };
  };

  return {
    error: null,
    data: result,
  };
};

/**
 * Gets the banned list
 * @param {*} userData 
 * @returns 
 */
export const getBanList = (userData) => async (name) => {
  const result = await userData.getBanList(name);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null
    };
  };

  if (!result.length) {
    return {
      error: errors.NOT_FOUND,
      data: null
    };
  };

  return {
    error: null,
    data: result,
  };
};

/**
 * Ban a user
 * @param {*} userData 
 * @returns 
 */
export const banUser = (userData) => async (bodyParams, userParams) => {
  // Check user exist
  if (!await userData.hasThisUser(bodyParams.username)) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  };

  // Check for user in banned list
  const checkForBan = await userData.checkUserForBan(bodyParams.username);
  
  if (checkForBan.length) {
    return {
      error: errors.BANNED_USER,
      data: null,
    };
  };

  const bannedUserData = await userData.getUserByUsername(bodyParams.username);
  const result = await userData.createBan(bannedUserData.id, bodyParams.reason, userParams.id);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  return {
    error: null,
    data: result,
  }  ;
};

/**
 * Remove user ban.
 * @param {*} userData 
 * @returns 
 */
export const removeBan = (userData) => async (bodyParams) => {
  // Check user exist
  if (!await userData.hasThisUser(bodyParams.username)) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  };

  // Check for user in banned list
  const checkForBan = await userData.checkUserForBan(bodyParams.username);
  
  if (!checkForBan.length) {
    return {
      error: errors.BANNED_USER,
      data: null,
    };
  };
  
  const bannedUserData = await userData.getUserByUsername(bodyParams.username);
  const result = await userData.deleteBan(bannedUserData.id);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  return {
    error: null,
    data: result,
  };
};

/**
 * Delete user
 * @param {*} userData 
 * @returns 
 */
export const deleteUser = (userData) => async (userId) => {  

  if (!(await userData.getFullUserInfoById(userId)).length) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  };
  
  const result = await userData.deleteUser(userId);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  return {
    error: null,
    data: result,
  };
};

/**
 * Save a token in the used tokens database
 * @param {*} userData 
 * @returns 
 */
export const saveToken = (userData) => async (token) => {

  const savedToken = await userData.saveOldToken(token);

  if (!savedToken) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  return {
    error: null,
    data: savedToken,
  };
};

/**
 * Add a friend by id.
 * @param {*} userData 
 * @returns 
 */
export const addFriend = (userData) => async (userId, friendId) => {

  if(userId === friendId) {
    return {
    error: errors.OPERATION_NOT_ALLOWED,
    data: null,
    };
  };

  const addedFriend = await userData.addNewFriend(userId, friendId);

  return {
    error: null,
    data: addedFriend,
  };
};

/**
 * Delete a friend.
 * @param {*} userData 
 * @returns 
 */
export const deleteFriend = (userData) => async (userId, friendId) => {

  const deletedFriend = await userData.deleteSingleFriend(userId, friendId);

  return {
    error: null,
    data: deletedFriend,
  };
};
export const checkFriend = (userData) => async (userId, friendId) => {

  const friendCheck = await userData.checkFriendship(userId, friendId);

  return {
    error: null,
    data: friendCheck,
  };

};


//Add activity
// export const addActivity = (userData) => async (userId, activity) => {

//   const addedActivity = await userData.addNewActivity(userId, activity);

//   return {
//     error: null,
//     data: addedActivity,
//   };
// };

export const getUserInfoById = (userData) => async (userId) => {
  if (!await userData.userExistsById(userId)) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  };

  const result = await userData.getFullUserInfoById(userId);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  return {
    error: null,
    data: result,
  };
};


/**
 * Return friend list
 * @param {*} userData 
 * @returns 
 */
export const getUserFriendsById = (userData) => async (userId) => {  
  const result = await userData.friendsList(userId);  

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  return {
    error: null,
    data: result,
  };
};


/**
 * Add friend
 * @param {*} userData 
 * @returns 
 */
export const addNewFriend = (userData) => async (myId, userId) => {

  if (+myId === +userId) {
    return {
      error: errors.INVALID_PARAM,
      data: null,
    };
  }

  if (!await userData.userExistsById(userId)) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  };
  
  if (await userData.checkFriendsPair(myId, userId)) {
    return {
      error: errors.DUPLICATE_RECORD,
      data: null,
    };
  }

  const result = await userData.createNewFriendship(myId, userId);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  return {
    error: null,
    data: result[0],
  };

};


/**
 * Remove friend
 * @param {*} userData 
 * @returns 
 */
export const removeFriend = (userData) => async (myId, userId) => {
  if (+myId === +userId) {
    return {
      error: errors.INVALID_PARAM,
      data: null,
    };
  }

  if (!await userData.userExistsById(userId)) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  };
  
  if (!await userData.checkFriendsPair(myId, userId)) {
    return {
      error: errors.DUPLICATE_RECORD,
      data: null,
    };
  }

  const result = await userData.removeFriendship(myId, userId);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  return {
    error: null,
    data: result[0],
  };
};

/**
 * Return last ten registered users
 * @param {*} userData 
 * @returns 
 */
export const lastTenUsers = (userData) => async () => {
  const result = await userData.getLastUsers();

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  return {
    error: null,
    data: result,
  };
};

/**
 * Return users start with specific letter
 * @param {*} userData 
 * @returns 
 */
export const getByFirstLetter = (userData) => async (letter) => {
  if (!isNaN(letter)) {
    return {
      error: errors.INVALID_PARAM,
      data: null,
    };
  }

  const result = await userData.nameStartWith(letter);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  return {
    error: null,
    data: result,
  };
};