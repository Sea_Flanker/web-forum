import errors from './errors.js';
import * as insideFunctions from '../data/comment-data.js';
import { addNewActivity } from '../data/user-data.js';

/**
 * Inject a additional info about comments
 * @param { insideFunctions } commentData
 */
const addAdditionalInfo = (commentData) => {
  /**
 * 
 * @param { number } userId 
 * @param { number } commentId
 * @returns { object } 
 */
  return async (userId, commentId) => {
    
    const commentIsMine = await commentData.checkCommentIsMine(userId, commentId);
    const flaggedList = await commentData.flagUserList(commentId);
    
    const infoObject = {
      likedBy: await commentData.getUsersLikedCommentById(commentId),
      commentIsMine,
      alreadyLikeThisComment: await commentData.checkLikeThisComment(userId, commentId),
      // flagged: flaggedList.length > 0 ? true : false,
      flaggedByMe: flaggedList.some(x => x.userId === userId),
      // usersWhoFlagged: [],
    };

    // if (commentIsMine) {
    //   infoObject.userWhoFlagged = flaggedList;
    // }

    return infoObject;
  };
};


/**
 * Get all comments for post with Id
 * @param { insideFunctions } commentData
 */
export const getAllCommentsForPost = (commentData) => async (data) => {
  const comments = await commentData
    .getAllCommentsIdsForPost(data.firstParams.postId, data.query.start, data.query.count);  

  if (!comments) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  const result = await Promise.all(comments.map(async (x) => {
    const post = await commentData.getCommentById(data.firstParams.postId, x.id, 10);
    const addInfo = await addAdditionalInfo(commentData)(data.user.id, x.id);
    return {...post[0], ...addInfo};
  })); 
  
  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  return {
    error: null,
    data: result.flat(),
  };
};


/**
 * Create new comment for post by ID
 * @param {*} commentData 
 * @returns 
 */
export const createComment = (commentData) => async (data) => {
  const sendData = {
    text: data.body.text,
    postId: data.firstParams.postId,
    userId: data.user.id,
  };

  const result = await commentData.addCommentToPostById(sendData);
  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  const addInfo = await addAdditionalInfo(commentData)(data.user.id, result[0].commentId);

  //Save activity
  //addNewActivity(data.user.id, 'Created a new comment');

  return {
    error: null,
    data: {...result[0], ...addInfo},
  };
};


/**
 * Get comment data
 * @param {*} commentData 
 * @returns 
 */
export const getCommentById = (commentData) => async (data) => {
  const checkData = await commentData
    .checkPostIdAndCommentId(data.firstParams.postId, data.params.id);

  if (!checkData) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  };

  if ('likes' in data.query && isNaN(data.query.likes)) {
    return {
      error: errors.INVALID_PARAM,
      data: null,
    };
  } else {
    if (parseInt(data.query.likes) < 0) {
      data.query.likes = 10;
    };
  };

  const result = await commentData
    .getCommentById(data.firstParams.postId, data.params.id, data.query.likes);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };
  
  const addInfo = await addAdditionalInfo(commentData)(data.user.id, data.params.id);
  
  return {
    error: null,
    data: {...result[0], ...addInfo},
  };
};


/**
 * Update comment
 * @param {*} commentData 
 * @returns 
 */
export const updateCommentById = (commentData) => async (data) => {  
  if (!await commentData.checkPostIdAndCommentId(data.firstParams.postId, data.params.id)) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  };

  const result = await commentData.updateCommentById(data);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  const addInfo = await addAdditionalInfo(commentData)(data.user.id, data.params.id);
  
  return {
    error: null,
    data: {...result[0], ...addInfo},
  };
};


/**
 * Delete comment by ID
 */
export const deleteCommentById = (commentData) => async (postId, commentId, userId) => {
  if (!await commentData.checkPostIdAndCommentId(postId, commentId)) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  };

  const result = await commentData.deleteCommentById(postId, commentId);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  const addInfo = await addAdditionalInfo(commentData)(userId, commentId);
  

  
  return {
    error: null,
    data: {...result[0], ...addInfo},
  };
};

/**
 * Like comment
 * @param {*} commentData 
 * @returns 
 */
export const likeCommentById = ((commentData) => async (postId, commentId, userId) => {
  const result = await commentData.likeComment(postId, commentId, userId);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  const addInfo = await addAdditionalInfo(commentData)(userId, commentId);
  
  return {
    error: null,
    data: {...result[0], ...addInfo},
  };
});


/**
 * Unlike comment
 * @param {*} commentData 
 * @returns 
 */
export const unLikeCommentById = ((commentData) => async (postId, commentId, userId) => {
  const result = await commentData.unLikeComment(postId, commentId, userId);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  const addInfo = await addAdditionalInfo(commentData)(userId, commentId);
  
  return {
    error: null,
    data: {...result[0], ...addInfo},
  };
});


/**
 * Flag comment
 * @param {*} commentData 
 * @returns 
 */
export const flagCommentById = ((commentData) => async (postId, commentId, userId) => {
  const result = await commentData.flagComment(postId, commentId, userId);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  const addInfo = await addAdditionalInfo(commentData)(userId, commentId);
  
  return {
    error: null,
    data: {...result[0], ...addInfo},
  };
});


/**
 * Remove flag from comment
 * @param {*} commentData 
 * @returns 
 */
export const unFlagCommentById = ((commentData) => async (postId, commentId, userId) => {
  const result = await commentData.unFlagComment(postId, commentId, userId);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  const addInfo = await addAdditionalInfo(commentData)(userId, commentId);
  
  return {
    error: null,
    data: {...result[0], ...addInfo},
  };

});

/**
 * Approve comment by ID /admin Only/
 * @param {*} commentData 
 * @returns 
 */
export const approveCommentById = (commentData) => async (postId, commentId, userId) => {

  const flaggedList = await commentData.flagUserList(commentId);

  if (!await commentData.checkPostIdAndCommentId (postId, commentId) || !flaggedList.length) {
    return {
      error: errors.NOT_FOUND,
      data: null,
    };
  };

  const result = await commentData.approveComment(postId, commentId);

  if (!result) {
    return {
      error: errors.PROBLEM,
      data: null,
    };
  };

  const addInfo = await addAdditionalInfo(commentData)(userId, commentId);
  
  return {
    error: null,
    data: {...result[0], ...addInfo},
  };
};