/**
 * Checks the parameters on registration.
 */
export default {
  username: (value) => (typeof value === 'string' && value.length >= 4
    && value.length <= 20 && RegExp(/^[a-z]+[a-z0-9_]+$/g).test(value))
    ? true : `username must be string in range 4-20 symbols, and must contain only small letters, 
numbers and from special characters only _. Must start with letter!`,
  
  password: (value) => (typeof value === 'string' && value.length >= 6)
    ? true : 'password must be string with minimum 6 symbols'
};