/**
 * Validates if the comment data parameters are using the correct format. 
 */
export default {    
  text: (value) => (typeof value === 'string' && value.length >= 1)
    ? true
    : 'text must be string with min ONE symbol',
};
