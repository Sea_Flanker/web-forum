/**
 * Validates the input data for the personal information.
 */
export default {
  firstName: (value) => (typeof value === 'string' && value.length >= 1 && value.length <= 20)
    ? true : 'firstName must be string in range 1-20',

  lastName: (value) => (typeof value === 'string' && value.length >= 1 && value.length <= 20)
    ? true : 'lastName must be string in range 1-20',
    
  email: (value) => RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/).test(value)
    ? true : 'invalid email'
};


