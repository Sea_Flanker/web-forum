/**
 * Validates if the user id and the friend id data parameters are using the correct format. 
 */
 export default {
    userId: (value) => (typeof value === 'number' && value > 0)
      ? true : 'post Id must be number > 0',
  
    friendId: (value) => (typeof value === 'number' && value > 0)
      ? true : 'category Id must be number > 0',
  };