/**
 * Validates if the category data parameters are using the correct format. 
 */
export default {
  category: (value) => (typeof value === 'string' && value.length >= 1 &&
  value.length <= 20 && RegExp(/^[A-Za-z0-9_ -]*$/).test(value))
    ? true
    : category `name must be string contain only letters and numbers in range 
1-20 symbols. From special characters allow only _ and -`,
};


