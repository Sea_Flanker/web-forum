/**
 * Validates if the post data parameters are using the correct format. 
 */
export default {
  title: (value) => (typeof value === 'string' && value.length >= 4 && value.length <= 20)
    ? true : 'title must be string in range 4-20',
  text: (value) => (typeof value === 'string' && value.length >= 1)
    ? true : 'text must be string with min ONE symbol',
};
