/**
 * Validates if the username data parameters are using the correct format. 
 */
export default {
  postId: (value) => (typeof value === 'number' && value > 0)
    ? true : 'post Id must be number > 0',

  categoryId: (value) => (typeof value === 'number' && value > 0)
    ? true : 'category Id must be number > 0',
};