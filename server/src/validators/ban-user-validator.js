/**
 * Validates if the username data parameters are using the correct format. 
 * @param {*} value 
 * @returns 
 */
const username = (value) => (typeof value === 'string' && value.length >= 4
  && value.length <= 20 && RegExp(/^[a-z]+[a-z0-9_]+$/g).test(value))
  ? true
  : `username must be string in range 4-20 symbols, and must contain only small letters, 
  numbers and from special characters only _. Must start with letter!`;

/**
 * Validates if the username and reason are input using the correct data format
 * , when we add a ban.
 */
const addBan = {
    username: username,
    reason: (value) => (typeof value === 'string' && value.length >= 4)
    ? true : 'reason must be string with minimum 4 symbols'
};

/**
 * Validates if the username data parameters are using the correct format when
 * we want to remove a ban.
 */
const removeBan = {
    username: username,
};

export default { addBan, removeBan };