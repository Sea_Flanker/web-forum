import express from 'express';
import { authMiddleware } from '../auth/authMiddleware.js';
import * as commentData from '../data/comment-data.js';
import { bodyValidator } from '../middlewares/body-validator.js';
import commentDataValidator from '../validators/comment-data-validator.js';
import * as commentsServices from '../services/comments-services.js';
import { fileLogger } from '../middlewares/file-logger.js';
import errors from '../services/errors.js';
import { checkUserPermission, commentOwnerOrAdmin }
  from '../middlewares/user-permission-validator.js';
import { flagStatusValidator,likesStatusValidator }
  from '../middlewares/comments-props-validator.js';
import usersGroups from '../common/users-groups.js';
import { oldTokenCheck } from '../middlewares/used-token-check.js';

export const commentRouter = express.Router();

commentRouter.use(fileLogger);
commentRouter.use(authMiddleware);
commentRouter.use(oldTokenCheck);


/**
 * Get all comments for specified post
 */
commentRouter.get('/', async (req, res) => {
  const checkQueryValues = Object.values(req.query).some(x => isNaN(x) || x < 0);
  const checkQueryKeys = Object.keys(req.query).filter(x=> x === 'start' || x === 'count').length;

  if(checkQueryValues) {
    return res.status(404).send({ message: `invalid query parameter, must be num > zero!` });
  }

  if(checkQueryKeys !== 2) {
    return res.status(404).send({ message: `invalid query parameter, must be start and count` });
  }

  const { error, data } = await commentsServices.getAllCommentsForPost(commentData)(req);

  if (error === errors.PROBLEM) {
    return res.status(400).send({ message: `can't do your request now. Try latter` });
  } 
  
  return res.status(200).send(data);
});


/**
 * Create comment for specified post 
 */
commentRouter.post('/create', bodyValidator(commentDataValidator), async (req, res) => {
  const { error, data } = await commentsServices.createComment(commentData)(req);

  if (error === errors.PROBLEM) {
    return res.status(400).send({ message: `can't do your request now. Try latter` });
  } 
  
  return res.status(200).send(data);
});


/**
 * GET full comment information
 */
commentRouter.get('/:id', async (req, res) => {
  const { error, data } = await commentsServices.getCommentById(commentData)(req);

  if (error === errors.NOT_FOUND) {
    return res.status(404)
      .send(
        {
          message: `post ID ${req.firstParams.postId} don't have a comment with id ${req.params.id}`
        });
  }

  if (error === errors.INVALID_PARAM) {
    return res.status(404)
      .send({ message: `invalid query parameter, must be number bigger from zero!` });
  }

  if (error === errors.PROBLEM) {
    return res.status(400).send({ message: `can't do your request now. Try latter` });
  } 
  
  return res.status(200).send(data);
});


/**
 * Update comment.
 */
commentRouter.put('/update/:id',
  commentOwnerOrAdmin, bodyValidator(commentDataValidator), async (req, res) => {
    const { error, data } = await commentsServices.updateCommentById(commentData)(req);

    if (error === errors.NOT_FOUND) {
      return res.status(404)
        .send({ message: `post with ID ${req.firstParams.postId}
          don't have comment with id ${req.params.id}` });
    }

    if (error === errors.PROBLEM) {
      return res.status(400).send({ message: `can't do your request now. Try latter` });
    } 
    
    return res.status(200).send(data);
});


/**
 * Delete comment.
 */
commentRouter.delete('/delete/:id', commentOwnerOrAdmin, async (req, res) => {
  const { error, data } = await commentsServices.
    deleteCommentById(commentData)(req.firstParams.postId, req.params.id, req.user.id);

  if (error === errors.NOT_FOUND) {
    return res.status(404)
      .send({ message: `post with ID ${req.firstParams.postId}
        don't have comment with id ${req.params.id}` });
  }
  
  if (error === errors.PROBLEM) {
    return res.status(400).send({ message: `can't do your request now. Try latter` });
  } 
  
  return res.status(200).send(data);
});


/**
 * Like comment.
 */
commentRouter.post('/like/:id', likesStatusValidator('like'), async (req, res) => {
  const { error, data } = await commentsServices
    .likeCommentById(commentData)(req.firstParams.postId, req.params.id, req.user.id);

  if (error === errors.PROBLEM) {
    return res.status(400).send({ message: `can't do your request now. Try latter` });
  } 
  
  return res.status(200).send(data);
});


/**
 * Unlike comment
 */
commentRouter.delete('/unlike/:id', likesStatusValidator('unlike'), async (req, res) => {
  const { error, data } = await commentsServices
    .unLikeCommentById(commentData)(req.firstParams.postId, req.params.id, req.user.id);

  if (error === errors.PROBLEM) {
    return res.status(400).send({ message: `can't do your request now. Try latter` });
  } 
  
  return res.status(202).send(data);
});


/**
 * Flag comment
 */
commentRouter.post('/flag/:id', flagStatusValidator('flag'), async (req, res) => {
  const { error, data } = await commentsServices
    .flagCommentById(commentData)(req.firstParams.postId, req.params.id, req.user.id);

    if (error === errors.PROBLEM) {
      return res.status(400).send({ message: `can't do your request now. Try latter` });
    } 
    
    return res.status(202).send(data);
});

/**
 * Remove flag from comment
 */
commentRouter.delete('/flag/:id', flagStatusValidator('unFlag'), async (req, res) => {
  const { error, data } = await commentsServices
    .unFlagCommentById(commentData)(req.firstParams.postId, req.params.id, req.user.id);

    if (error === errors.PROBLEM) {
      return res.status(400).send({ message: `can't do your request now. Try latter` });
    } 
    
    return res.status(202).send(data);
});


commentRouter.put('/flag/:id',checkUserPermission(usersGroups.ADMIN), async (req, res) => {
  const { error, data } = await commentsServices
    .approveCommentById(commentData)(req.firstParams.postId, req.params.id, req.user.id);

  if (error === errors.NOT_FOUND) {
    return res.status(404)
      .send(
        {
          message: `post ID ${req.firstParams.postId} don't have a comment with id ${req.params.id}`
        });
  }


  if (error === errors.PROBLEM) {
    return res.status(400).send({ message: `can't do your request now. Try latter` });
  } 
  
  return res.status(202).send(data);
});