import express from 'express';
import { authMiddleware } from '../auth/authMiddleware.js';
import { logServer } from '../common/help-functions.js';
import usersGroups from '../common/users-groups.js';
import { bodyValidator } from '../middlewares/body-validator.js';
import { fileLogger } from '../middlewares/file-logger.js';
import { checkUserPermission, postOwnerOrAdmin } from '../middlewares/user-permission-validator.js';
import groupValidator from '../validators/post-groups-validator.js';
import postDataValidator from '../validators/post-data-validator.js';
import * as postData from '../data/post-data.js';
import { changePostGroup, createGroup, createNewPost, deleteGroup,
    deleteSinglePost, getAllPosts, getSinglePost, likePostById, lockPost, unlockPost,
    updateGroup, unLikePostById, getAllPostsByGroupId,
    updatePostById } from '../services/post-services.js';

import errors from '../services/errors.js';
import accessParameters from '../common/access-parameters.js';
import movePostValidator from '../validators/move-post-validator.js';
import { oldTokenCheck } from '../middlewares/used-token-check.js';


export const postRouter = express.Router();

postRouter.use(fileLogger);
postRouter.use(authMiddleware);
postRouter.use(oldTokenCheck);

/**
 * Create post group
 */
postRouter.post('/category/create', checkUserPermission(usersGroups.ADMIN),
  bodyValidator(groupValidator),async (req, res) => {
    const { error, data } = await createGroup(postData)(req.body.category);
    
    if (error === errors.DUPLICATE_RECORD) {
      return res.status(400).send({ message: `Already have group with name ${req.body.category}` });
    }

    if (error === errors.PROBLEM) {
      return res.status(400).send({ message: `can't do your request now. Try latter` });
    }

    logServer.green('NEW POST CATEGORY CREATED');
    return res.status(201).send(data);
});


/**
 * update category
 */
postRouter.put('/category/:id', checkUserPermission(usersGroups.ADMIN),
  bodyValidator(groupValidator), async (req, res) => {
    const { error, data } = await updateGroup(postData)(req);

    if (error === errors.DUPLICATE_RECORD) {
      return res.status(400).send({ message: `Already have group with name ${req.body.category}` });
    }
    
    if (error === errors.NOT_FOUND) {
      return res.status(404).send({ message: `category with this id don't exist!` });
    }

    if (error === errors.PROBLEM) {
      return res.status(400).send({ message: `can't do your request now. Try latter` });
    }

    return res.status(200).send(data);
});


/**
 * delete category
 */
postRouter.delete('/category/:id', checkUserPermission(usersGroups.ADMIN), async (req, res) => {
    const { error, data } = await deleteGroup(postData)(req.params.id);
    if (error === errors.NOT_FOUND) {
      return res.status(404).send({ message: `category with this id don't exist!` });
    }
    
    if (error === errors.PROBLEM) {
      return res.status(400).send({ message: `can't do your request now. Try latter` });
    }

    return res.status(200).send(data);
});


/**
 * Move post in other category
 */
postRouter.put('/move', bodyValidator(movePostValidator),
  postOwnerOrAdmin(usersGroups.ADMIN, accessParameters.postBodyId), async (req, res) => {
    const { error, data } = await changePostGroup(postData)
      (req.user.id, req.body.categoryId, req.body.postId);
    if (error === errors.NOT_FOUND) {
      return res.status(404).send({ message: `category with this id don't exist!` });
    }
    
    if (error === errors.DUPLICATE_RECORD) {
      return res.status(400).send({ message: `this post is already in this category` });
    }

    if (error === errors.PROBLEM) {
      return res.status(400).send({ message: `can't do your request now. Try latter` });
    }

    return res.status(200).send(data);
});


/**
 * Create post
 */
postRouter.post('/create', bodyValidator(postDataValidator), async (req, res) => {
    const { error, data } = await createNewPost(postData)(req.body, req.user);
    
    if (error === errors.DUPLICATE_RECORD) {
      return res.status(400)
      .send({ message: `post with title '${req.body.title}' already exists!` });
    }

    if (error === errors.PROBLEM) {
      return res.status(400).send({ message: `can't do your request now. Try latter` });
    }
    
    logServer.green('NEW POST CREATED');
    return res.status(201).send(data);
});


/**
 * Get all posts sorted by groups and date. Option: max posts from group.
 */
postRouter.get('/all', async (req, res) => {
  const { error, data } = await getAllPosts(postData)(req.query);

  if (error === errors.OPERATION_NOT_ALLOWED) {
    return res.status(400).send({ message: `invalid query parameter` });
  }

  if (error === errors.PROBLEM) {
    return res.status(400).send({ message: `can't do your request now. Try latter` });
  }
  
  return res.status(200).send(data);
});

/**
 * Read individual post
 */
postRouter.get('/:id', async (req, res) => {
  const { error, data } = await getSinglePost(postData)(req.user.id, req.params.id);
  
  if (error === errors.OPERATION_NOT_ALLOWED) {
    return res.status(400).send({ message: `invalid query parameter` });
  }

  if (error === errors.NOT_FOUND) {
    return res.status(404).send({ message: `post with this id not found` });
  }

  if (error === errors.PROBLEM) {
    return res.status(400).send({ message: `can't do your request now. Try latter` });
  }
  
  return res.status(200).send(data);
});

/**
 * Delete post
 */
postRouter.delete('/:postId',
  postOwnerOrAdmin(usersGroups.ADMIN, accessParameters.postId), async (req, res) => {  
    const { error, data } = await deleteSinglePost(postData)(req.user.id, req.params.postId);

    if (error === errors.NOT_FOUND) {
      return res.status(404).send({ message: `post with this id not found` });
    }

    if (error === errors.OPERATION_NOT_ALLOWED) {
      return res.status(401).send({ message: `you don't have permissions for this post!` });
    }

    return res.status(200).send(data);
});


/**
 * Flag post
 */
postRouter.delete('/:postId',
  postOwnerOrAdmin(usersGroups.ADMIN, accessParameters.postId), async (req, res) => {  
    const { error, data } = await flagSinglePost(postData)(req.user.id, req.params.postId);

    if (error === errors.NOT_FOUND) {
      return res.status(404).send({ message: `post with this id not found` });
    }

    if (error === errors.OPERATION_NOT_ALLOWED) {
      return res.status(401).send({ message: `you don't have permissions for this post!` });
    }

    return res.status(200).send(data);
});

/**
 * Lock post
 */
postRouter.post('/lock/:postId',
  postOwnerOrAdmin(usersGroups.ADMIN, accessParameters.postId), async (req, res) => {
    const sendData = {
      userId: req.user.id,    
      postId: req.params.postId,
      reason: req.body.reason,
    };
    
    const { error, data } = await lockPost(postData)(sendData);

    if (error === errors.DUPLICATE_RECORD) {
      return res.status(404).send({ message: `this post is already locked` });
    };

    if (error === errors.PROBLEM) {
      return res.status(400).send({ message: `can't do your request now. Try latter` });
    }
    
    return res.status(200).send(data);
});


/**
 * Delete post
 */
postRouter.delete('/unlock/:postId',
  postOwnerOrAdmin(usersGroups.ADMIN, accessParameters.postId), async (req, res) => {
  const sendData = {
    userId: req.user.id,
    userGroup: req.user.group,
    postId: req.params.postId,
  };

  const { error, data } = await unlockPost(postData)(sendData);
  
  if (error === errors.NOT_FOUND) {
    return res.status(404).send({ message: `this post is not locked` });
  };

  if (error === errors.OPERATION_NOT_ALLOWED) {
    return res.status(401).send({ message: `this post is locked by admin, you can't unlock it` });
  };

  if (error === errors.PROBLEM) {
    return res.status(400).send({ message: `can't do your request now. Try latter` });
  };
  
  return res.status(200).send(data);

});


/**
 * Like post
 */
postRouter.post('/like/:id', async (req, res) => {
  const { error, data } = await likePostById(postData)(req.user.id, req.params.id);

  if (error === errors.NOT_FOUND) {
    return res.status(404).send({ message: `post with this id not found` });
  };

  if (error === errors.LOCKED_POST) {
    return res.status(404).send({ message: `can't like locked post` });
  };

  if (error === errors.DUPLICATE_RECORD) {
    return res.status(404).send({ message: `you already like this post` });
  };

  if (error === errors.OPERATION_NOT_ALLOWED) {
    return res.status(404).send({ message: `can't like or unlike your own posts` });
  };

  if (error === errors.PROBLEM) {
    return res.status(400).send({ message: `can't do your request now. Try latter` });
  };
  
  return res.status(200).send(data);  
});


/**
 * unLike post
 */
postRouter.delete('/unlike/:id', async (req, res) => {
  const { error, data } = await unLikePostById(postData)(req.user.id, req.params.id);

  if (error === errors.NOT_FOUND) {
    return res.status(404).send({ message: `post with this id not found` });
  };

  if (error === errors.LOCKED_POST) {
    return res.status(404).send({ message: `can't like locked post` });
  };

  if (error === errors.DUPLICATE_RECORD) {
    return res.status(404).send({ message: `you must first LIKE this post` });
  };

  if (error === errors.OPERATION_NOT_ALLOWED) {
    return res.status(404).send({ message: `can't like or unlike your own posts` });
  };

  if (error === errors.PROBLEM) {
    return res.status(400).send({ message: `can't do your request now. Try latter` });
  };
  
  return res.status(200).send(data);  
});


/**
 * Get all post from category ordered by date
 * option - <start> start from number
 * option - <count> count of posts after start position
 */
postRouter.get('/category/:id', async (req, res) => {
  if ('start' in req.query) {
    if (isNaN(req.query.start) || req.query.start < 0 || !Number.isInteger(+req.query.start)) {
      return res.status(400).send({ message: `invalid query parameters` });
    }
  } else {
    req.query.start = 0;
  }

  if ('count' in req.query) {
    if (isNaN(req.query.count) || req.query.count < 0 || !Number.isInteger(+req.query.count)) {
      return res.status(400).send({ message: `invalid query parameters` });
    }
  } else {
    req.query.count = 10;
  }

  const { error, data } = await getAllPostsByGroupId(postData)
    (req.user.id, req.params.id, +req.query.start, +req.query.count);

    if (error === errors.NOT_FOUND) {
      return res.status(404).send({ message: `group with this id not found` });
    };

  
    if (error === errors.PROBLEM) {
      return res.status(400).send({ message: `can't do your request now. Try latter` });
    };
    
    return res.status(200).send(data);
});


/**
 * Update post
 */
postRouter.put('/:postId', bodyValidator(postDataValidator),
  postOwnerOrAdmin(usersGroups.ADMIN, accessParameters.postId),
  fileLogger, async (req, res) => {
  
  const { error, data } = await updatePostById(postData)(req.user.id, req.params.postId, req.body);

  if (error === errors.LOCKED_POST) {
    return res.status(404).send({ message: `can't update locked post` });
  };

  if (error === errors.PROBLEM) {
    return res.status(400).send({ message: `can't do your request now. Try latter` });
  };
  
  return res.status(200).send(data);

});
