import express from 'express';
import { bodyValidator } from '../middlewares/body-validator.js';
import registerUserValidator from '../validators/register-user-validator.js';
import personalInformationValidator from '../validators/personal-info-validator.js';
import { logServer } from '../common/help-functions.js';
import { addFriend, banUser, checkFriend, deleteFriend, deleteUser, getBanList, registerUser,
  removeBan, getUserInfoById, saveToken, searchForUser, updateUserInfo, userPassCheck,
  getUserFriendsById, addNewFriend, removeFriend, lastTenUsers, getByFirstLetter }
  from '../services/user-services.js';
import * as userData from '../data/user-data.js';
import errors from '../services/errors.js';
import { fileLogger } from '../middlewares/file-logger.js';
import createToken from '../auth/create_token.js';
import { authMiddleware } from '../auth/authMiddleware.js';
import { checkUserPermission, rightUserOrAdmin } from '../middlewares/user-permission-validator.js';
import usersGroups from '../common/users-groups.js';
import banUserValidator from '../validators/ban-user-validator.js';
import accessParameters from '../common/access-parameters.js';
import { oldTokenCheck } from '../middlewares/used-token-check.js';
// import { saveToken } from '../services/user-services.js';

export const userRoute = express.Router();
userRoute.use(fileLogger);

/**
 * Register user.
 */
userRoute.post('/register', bodyValidator(registerUserValidator), async (req, res) => {
  const { error, data } = await registerUser(userData)(req.body);

  if (error === errors.DUPLICATE_RECORD) {
    return res.status(400).send({
      message: `User with username ${req.body.username} already exists!`,
    });
  };

  logServer.green('USER CREATED');
  res.status(201).send(data);

});


/**
 * Login user
 */
userRoute.post('/login', bodyValidator(registerUserValidator), async(req, res) => {
  const { error, data } = await userPassCheck(userData)(req.body);
  
  if (error === errors.NOT_FOUND) {
    return res.status(404).send({ message: `this username don't exists!` });
  };

  if (error === errors.BANNED_USER) {
    return res.status(403).send(
      {
        message: `this username is banned`,
        reason: data.reason,
        date: data.date.toLocaleDateString('fr-CA'),
        advice: `you can send a complaint to pop@armenia.com`
      });
  };

  if (error === errors.WRONG_CREDENTIALS) {
    return res.status(404).send({ message: `invalid credentials!` });
  };

  // Create the token
  const token = createToken({
    id: data.id,
    username: data.name,
    group: data.groups_id,
    firstName: data.first_name,
    lastName: data.last_name,
  });

  return res.status(200).send ({ token });
});


/**
 * Logout user
 */
userRoute.delete('/logout', authMiddleware, oldTokenCheck, async (req, res) => {
  const {error, data} = await saveToken(userData)(req.headers.authorization.replace('Bearer ', ''));
  
  if (error === errors.PROBLEM) {
    return res.status(404).send({ message: `can't do your request now. Try latter` });
  }

  return res.status(200).send({ message: 'success logout' });
});

/**
 * Update user information
 */
userRoute.put('/update/:id', authMiddleware, oldTokenCheck,
  rightUserOrAdmin(accessParameters.userIdToken, accessParameters.userIdParam),
  bodyValidator(personalInformationValidator), async (req, res) => {
    const { error, data } = await updateUserInfo(userData)(req.params, req.body);

    if (error === errors.DUPLICATE_EMAIL) {
      return res.status(404).send({ message: 'this e-mail already used by another user!' });
    }

    if (error === errors.PROBLEM) {
      return res.status(404).send({ message: `can't do your request now. Try latter` });
    }

    return res.status(200).send(data);
});

/**
 * Search for user in personal info and username.
 */
userRoute.get('/search', authMiddleware, oldTokenCheck, async (req, res) => {
  const { error, data } = await searchForUser(userData)(req.query.name);
  
  if (error === errors.PROBLEM) {
    return res.status(404).send({ message: `Can't do your request now. Try latter.` });
  };

  res.status(200).send(data);
  
});


/**
 * Show ban list. Admins only.
 */
userRoute.get('/bans/list', authMiddleware, oldTokenCheck,
  checkUserPermission(usersGroups.ADMIN),async (req, res) => {
    const { error, data } = await getBanList(userData)(req.query.name);

    if (error === errors.NOT_FOUND) {
      return res.status(404).send({ message: `user not found or banned list is empty` });
    }

    if (error === errors.PROBLEM) {
      return res.status(404).send({ message: `can't do your request now. Try latter` });
    }

    return res.status(200).send(data);
});


/**
 * Ban a user. Admins only.
 */
userRoute.post('/bans/add', authMiddleware, oldTokenCheck, checkUserPermission(usersGroups.ADMIN),
  bodyValidator(banUserValidator.addBan), async (req, res) => {  
    const { error, data } = await banUser(userData)(req.body, req.user);

    if (error === errors.NOT_FOUND) {
      return res.status(404).send({ message: `this username don't exists!` });
    }

    if (error === errors.BANNED_USER) {
      return res.status(404).send({ message: `this user is already in ban list` });
    }

    if (error === errors.PROBLEM) {
      return res.status(404).send({ message: `can't do your request now. Try latter` });
    }

    return res.status(200).send(data);
});


/**
 * Remove a user from ban list. Admins only.
 */
userRoute.delete('/bans/remove', authMiddleware, oldTokenCheck,
  checkUserPermission(usersGroups.ADMIN),
  bodyValidator(banUserValidator.removeBan), async (req, res) => {
    const { error, data } = await removeBan(userData)(req.body);

    if (error === errors.NOT_FOUND) {
      return res.status(404).send({ message: `this username don't exists!` });
    }

    if (error === errors.BANNED_USER) {
      return res.status(404).send({ message: `this user don't exists in ban list` });
    }

    if (error === errors.PROBLEM) {
      return res.status(404).send({ message: `can't do your request now. Try latter` });
    }

    return res.status(200).send(data);
});


/**
 * Delete a user. Admins only.
 */
userRoute.delete('/:id', authMiddleware, oldTokenCheck,
  checkUserPermission(usersGroups.ADMIN), async(req, res) => {
    const { error, data } = await deleteUser(userData)(req.params.id);
    
    if (error === errors.NOT_FOUND) {
      return res.status(404).send({ message: `this username don't exists!` });
    }

    if (error === errors.PROBLEM) {
      return res.status(404).send({ message: `can't do your request now. Try latter` });
    }

    return res.status(200).send(data);
});


/**
 * Add friend by id.
 */
userRoute.post('/:id/friends', authMiddleware, oldTokenCheck, async(req, res) => {

  const { error, data} = await addFriend(userData)(req.params.id, req.body.id);

  //Saves the activity
  userData.addNewActivity(req.params.id, 'New friend');

  return res.status(200).send(data);
});


/**
 * Delete a friend by id.
 */
userRoute.delete('/:id/friends', authMiddleware, oldTokenCheck, async(req, res) => {
  if(!checkFriend){
    return res.status(404).send({ message: `can't do your request now. Try latter` });
  }

  const { error, data} = await deleteFriend(userData)(req.params.id, req.body.id);

  //Saves the activity
  userData.addNewActivity(req.params.id, 'Deleted friend');

  return res.status(200).send(data);
});

/**
 * Read user activities by ID. Admins only. 
 */
userRoute.get('/:id/activities', authMiddleware, oldTokenCheck,
  checkUserPermission(usersGroups.ADMIN), async(req, res) => {
  
  const temp = await userData.getActivitiesByID(req.params.id);
  
  userData.getActivitiesByID(req.params.id);

  return res.status(200).send(temp);
});


/**
 * Get user info
 */
userRoute.get('/info/:id', authMiddleware, oldTokenCheck, async(req, res) => {
  const { error, data } = await getUserInfoById(userData)(req.params.id);
  
  if (error === errors.NOT_FOUND) {
    return res.status(404).send({ message: `this username don't exists!` });
  }

  if (error === errors.PROBLEM) {
    return res.status(404).send({ message: `can't do your request now. Try latter` });
  }

  return res.status(200).send(data);
});


/**
 * Get my friends list
 */
userRoute.get('/friends', authMiddleware, oldTokenCheck, async (req, res) => {
  const { error, data } = await getUserFriendsById(userData)(req.user.id);
  
  if (error === errors.PROBLEM) {
    return res.status(404).send({ message: `can't do your request now. Try latter` });
  }

  return res.status(200).send(data);
});


/**
 * Add new friend
 */
userRoute.post('/friends/:id', authMiddleware, oldTokenCheck, async (req, res) => {
  const { error, data } = await addNewFriend(userData)(req.user.id, req.params.id);

  if (error === errors.NOT_FOUND) {
    return res.status(404).send({ message: `this user don't exists!` });
  }

  if (error === errors.INVALID_PARAM) {
    return res.status(404).send({ message: `you cannot be friends with yourself!` });
  }
  
  if (error === errors.PROBLEM) {
    return res.status(404).send({ message: `can't do your request now. Try latter` });
  }

  if (error === errors.DUPLICATE_RECORD) {
    return res.status(404).send({ message: `this users is already friends` });
  }

  return res.status(200).send(data);
});


/**
 * Remove friend
 */
userRoute.delete('/friends/:id', authMiddleware, oldTokenCheck, async (req, res) => {
  const { error, data } = await removeFriend(userData)(req.user.id, req.params.id);

  if (error === errors.NOT_FOUND) {
    return res.status(404).send({ message: `this user don't exists!` });
  }

  if (error === errors.INVALID_PARAM) {
    return res.status(404).send({ message: `you cannot be friends with yourself!` });
  }
  
  if (error === errors.DUPLICATE_RECORD) {
    return res.status(404).send({ message: `this users is NOT friends` });
  }

  if (error === errors.PROBLEM) {
    return res.status(404).send({ message: `can't do your request now. Try latter` });
  }

  return res.status(200).send(data);
});


/**
 * Get last 10 registered users
 */
userRoute.get('/last', authMiddleware, oldTokenCheck, async (req, res) => {
  const { error, data } = await lastTenUsers(userData)();

  if (error === errors.PROBLEM) {
    return res.status(404).send({ message: `can't do your request now. Try latter` });
  }

  return res.status(200).send(data);
});


/**
 * Get users with first symbol
 */
userRoute.get('/names/:letter', authMiddleware, oldTokenCheck, async (req, res) => {
  const { error, data } = await getByFirstLetter(userData)(req.params.letter);
  if (error === errors.INVALID_PARAM) {
    return res.status(404).send({ message: `Invalid letter, must be symbol in range A - Z` });
  }

  if (error === errors.PROBLEM) {
    return res.status(404).send({ message: `can't do your request now. Try latter` });
  }

  return res.status(200).send(data);
});