/* eslint-disable max-len */
import mariadb from 'mariadb';
import dotenv from 'dotenv';
import { logServer } from './help-functions.js';

const constants = dotenv.config().parsed;

const tempPool = mariadb.createPool({
    host: constants.HOST,
    port: +constants.DBPORT,
    user: constants.USER,
    password: constants.PASSWORD,
    multipleStatements: true,
});


const sqlQuery = `
  CREATE DATABASE  IF NOT EXISTS forumdb /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
  USE forumdb;
  -- MariaDB dump 10.18  Distrib 10.5.8-MariaDB, for Linux (x86_64)
  --
  -- Host: 127.0.0.1    Database: forumdb
  -- ------------------------------------------------------
  -- Server version	10.5.8-MariaDB

  /*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
  /*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
  /*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
  /*!40101 SET NAMES utf8 */;
  /*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
  /*!40103 SET TIME_ZONE='+00:00' */;
  /*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
  /*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
  /*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
  /*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

  --
  -- Table structure for table banned_users
  --

  DROP TABLE IF EXISTS banned_users;
  /*!40101 SET @saved_cs_client     = @@character_set_client */;
  /*!40101 SET character_set_client = utf8 */;
  CREATE TABLE banned_users (
    id int(11) NOT NULL AUTO_INCREMENT,
    date datetime NOT NULL DEFAULT current_timestamp(),
    reason varchar(45) DEFAULT NULL,
    banned_user_id int(11) NOT NULL,
    banned_from_id int(11) NOT NULL,
    PRIMARY KEY (id,banned_user_id,banned_from_id),
    KEY fk_banned_users_users1_idx (banned_user_id),
    KEY fk_banned_users_users2_idx (banned_from_id),
    CONSTRAINT fk_banned_users_users1 FOREIGN KEY (banned_user_id) REFERENCES users (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT fk_banned_users_users2 FOREIGN KEY (banned_from_id) REFERENCES users (id) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
  /*!40101 SET character_set_client = @saved_cs_client */;

  --
  -- Dumping data for table banned_users
  --

  LOCK TABLES banned_users WRITE;
  /*!40000 ALTER TABLE banned_users DISABLE KEYS */;
  INSERT INTO banned_users VALUES (1,'2021-07-31 13:52:09','Аман от ламери',5,6),(2,'2021-07-31 14:07:49','Що за име е Кошичкин?',9,1);
  /*!40000 ALTER TABLE banned_users ENABLE KEYS */;
  UNLOCK TABLES;

  --
  -- Table structure for table comments
  --

  DROP TABLE IF EXISTS comments;
  /*!40101 SET @saved_cs_client     = @@character_set_client */;
  /*!40101 SET character_set_client = utf8 */;
  CREATE TABLE comments (
    id int(11) NOT NULL AUTO_INCREMENT,
    text text NOT NULL,
    date_created datetime NOT NULL DEFAULT current_timestamp(),
    date_updated datetime NOT NULL DEFAULT current_timestamp(),
    is_deleted tinyint(1) NOT NULL DEFAULT 0,
    posts_id int(11) NOT NULL,
    users_id int(11) NOT NULL,
    PRIMARY KEY (id,posts_id,users_id),
    KEY fk_comments_posts1_idx (posts_id),
    KEY fk_comments_users1_idx (users_id),
    CONSTRAINT fk_comments_posts1 FOREIGN KEY (posts_id) REFERENCES posts (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT fk_comments_users1 FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
  /*!40101 SET character_set_client = @saved_cs_client */;

  --
  -- Dumping data for table comments
  --

  LOCK TABLES comments WRITE;
  /*!40000 ALTER TABLE comments DISABLE KEYS */;
  INSERT INTO comments VALUES (1,'Интересно','2021-07-31 12:39:00','2021-07-31 12:39:00',0,1,2),(2,'Надмина и Perl по нечетим синтаксис','2021-07-31 12:40:00','2021-07-31 12:40:00',0,5,2),(3,'Разработчиците на динамични уеб страници (страниците, чиито съдържание се произвежда на момента на тяхното извикване – т.е. динамично) често използват Perl поради големия брой безплатни скриптове, както и общността на разработчици предлагащи помощ на своите колеги. Богатата библиотека с модули, споменатата мощна текстообработка, която често се налага при генерирането на HTML страници, както и фактът, че е слабо типизиран и интерпретиран език, правят възможна бързата разработка на приложения в много области, включително и настолни графични приложения.\n\nФактът, че Perl се интерпретира при изпълнение, налага уеб сървърът да стартира на интерпретатора perl за всяка уеб страница, която трябва да се генерира. При по-натоварени сайтове това може да доведе до значително натоварване на системата, както откъм памет, така и откъм процесорно време. За да се избегне това, специално за Perl е създаден модула mod_perl за сървъра Apache, който на практика представлява интерпретатор за Perl, вграден в кода на Apache. Това позволява на уеб сървъра да изпълнява код на Perl, без да се налага да стартира нов процес за всяка заявена уеб страница. Това води до значително увеличение на производителността при намалени ресурси. Mod_perl отива дори още по-надалеч и дава на скриптовете на Perl достъп до всички „вътрешности“ на самия Apache сървър, като им позволява да се намесват във всеки един етап от доставянето на HTML страницата, както и да използват конструкции на Perl в конфигурационните файлове на Apache.','2021-07-31 12:41:32','2021-07-31 12:41:32',0,6,3),(4,'Какво означава # в името?','2021-07-31 12:42:11','2021-07-31 12:42:11',0,4,3),(5,'Айде сега се пробвай да откриеш разликите между това което ти е дадено като пример и това над което си медитирал, и можеш да разкрие загадката на измисления код...','2021-07-31 12:45:33','2021-07-31 12:45:33',0,7,4),(6,'Нямам идея','2021-07-31 12:47:42','2021-07-31 12:47:42',0,4,5),(7,'Ами да се чудят гламавите','2021-07-31 13:49:07','2021-07-31 13:49:07',0,9,6),(8,'Не ползвай нещо готово а си го напиши ти, както ти е кеф и с каквато функционалност желаеш.\n','2021-07-31 13:50:29','2021-07-31 13:50:29',0,8,6),(9,'Не ме занимавай с глупости освен ако нямаш основателна причина','2021-07-31 13:51:15','2021-07-31 13:51:15',0,4,6),(10,'Така си е.','2021-07-31 13:51:41','2021-07-31 13:51:41',0,5,6),(11,'Symfony или Laravel.\n','2021-07-31 13:54:28','2021-07-31 13:54:28',0,7,7),(12,'Хубавото при Symfony е, че е базирано на компоненти. Ако не иска да използва целия фреймуорк и последва съвета на @thrawn бих казал, че само с компонентите (които са независими от самия framework) на Symfony може да постигне целта си.\n\nВсе пак не е нужно да се открива топлата вода за всяко едно нещо.','2021-07-31 13:54:46','2021-07-31 13:54:46',0,7,7),(13,'Това което питаш практически е безсмислено - прави се обратно от усерфрендли адресът да се получи дървото с явни параметри. Отделно това id=1-2-3 силно мяза на съставен параметър, който после сплитваш на 1, 2 и 3.\n','2021-07-31 13:59:21','2021-07-31 13:59:21',0,11,1),(14,'Да бе, много изчерпателно','2021-07-31 14:01:18','2021-07-31 14:01:18',0,1,8),(15,'Така е','2021-07-31 14:02:06','2021-07-31 14:02:06',0,7,8),(16,'Леко тъпо е да се ползва два пъти една и съща променлива (id) използвай различни и си направи регулярни израз както ти трябва.\n','2021-07-31 14:03:14','2021-07-31 14:03:14',0,7,6),(17,'Защо да е тъпо, Фиданке?','2021-07-31 14:04:35','2021-07-31 14:04:35',0,7,9),(18,'Скука','2021-07-31 14:06:33','2021-07-31 14:06:33',0,7,10);
  /*!40000 ALTER TABLE comments ENABLE KEYS */;
  UNLOCK TABLES;

  --
  -- Table structure for table comments_likes
  --

  DROP TABLE IF EXISTS comments_likes;
  /*!40101 SET @saved_cs_client     = @@character_set_client */;
  /*!40101 SET character_set_client = utf8 */;
  CREATE TABLE comments_likes (
    id int(11) NOT NULL AUTO_INCREMENT,
    date datetime DEFAULT current_timestamp(),
    comments_id int(11) NOT NULL,
    users_id int(11) NOT NULL,
    PRIMARY KEY (id,comments_id,users_id),
    KEY fk_comments_like_comments1_idx (comments_id),
    KEY fk_comments_like_users1_idx (users_id),
    CONSTRAINT fk_comments_like_comments1 FOREIGN KEY (comments_id) REFERENCES comments (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT fk_comments_like_users1 FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
  /*!40101 SET character_set_client = @saved_cs_client */;

  --
  -- Dumping data for table comments_likes
  --

  LOCK TABLES comments_likes WRITE;
  /*!40000 ALTER TABLE comments_likes DISABLE KEYS */;
  INSERT INTO comments_likes VALUES (1,'2021-07-31 12:42:30',1,3),(2,'2021-07-31 12:47:26',4,5),(3,'2021-07-31 13:48:17',5,6),(4,'2021-07-31 13:51:33',2,6),(5,'2021-07-31 13:54:09',5,7),(6,'2021-07-31 13:55:32',2,7),(7,'2021-07-31 13:57:08',1,6),(8,'2021-07-31 14:01:41',11,8),(9,'2021-07-31 14:03:18',15,6),(10,'2021-07-31 14:04:14',12,9),(11,'2021-07-31 14:04:16',15,9),(12,'2021-07-31 14:06:18',16,10),(13,'2021-07-31 14:06:40',12,10);
  /*!40000 ALTER TABLE comments_likes ENABLE KEYS */;
  UNLOCK TABLES;

  --
  -- Table structure for table flag_comments
  --

  DROP TABLE IF EXISTS flag_comments;
  /*!40101 SET @saved_cs_client     = @@character_set_client */;
  /*!40101 SET character_set_client = utf8 */;
  CREATE TABLE flag_comments (
    id int(11) NOT NULL AUTO_INCREMENT,
    date datetime NOT NULL DEFAULT current_timestamp(),
    checked_admin tinyint(1) NOT NULL DEFAULT 0,
    checked_owner tinyint(1) NOT NULL DEFAULT 0,
    approved tinyint(1) NOT NULL DEFAULT 0,
    comments_id int(11) NOT NULL,
    users_id int(11) NOT NULL,
    PRIMARY KEY (id,comments_id,users_id),
    KEY fk_flag_comments_comments1_idx (comments_id),
    KEY fk_flag_comments_users1_idx (users_id),
    CONSTRAINT fk_flag_comments_comments1 FOREIGN KEY (comments_id) REFERENCES comments (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT fk_flag_comments_users1 FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  /*!40101 SET character_set_client = @saved_cs_client */;

  --
  -- Dumping data for table flag_comments
  --

  LOCK TABLES flag_comments WRITE;
  /*!40000 ALTER TABLE flag_comments DISABLE KEYS */;
  /*!40000 ALTER TABLE flag_comments ENABLE KEYS */;
  UNLOCK TABLES;

  --
  -- Table structure for table flag_posts
  --

  DROP TABLE IF EXISTS flag_posts;
  /*!40101 SET @saved_cs_client     = @@character_set_client */;
  /*!40101 SET character_set_client = utf8 */;
  CREATE TABLE flag_posts (
    id int(11) NOT NULL AUTO_INCREMENT,
    date datetime DEFAULT current_timestamp(),
    posts_id int(11) NOT NULL,
    users_id int(11) NOT NULL,
    PRIMARY KEY (id,posts_id,users_id),
    KEY fk_flag_posts_posts1_idx (posts_id),
    KEY fk_flag_posts_users1_idx (users_id),
    CONSTRAINT fk_flag_posts_posts1 FOREIGN KEY (posts_id) REFERENCES posts (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT fk_flag_posts_users1 FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  /*!40101 SET character_set_client = @saved_cs_client */;

  --
  -- Dumping data for table flag_posts
  --

  LOCK TABLES flag_posts WRITE;
  /*!40000 ALTER TABLE flag_posts DISABLE KEYS */;
  /*!40000 ALTER TABLE flag_posts ENABLE KEYS */;
  UNLOCK TABLES;

  --
  -- Table structure for table friends
  --

  DROP TABLE IF EXISTS friends;
  /*!40101 SET @saved_cs_client     = @@character_set_client */;
  /*!40101 SET character_set_client = utf8 */;
  CREATE TABLE friends (
    id int(11) NOT NULL AUTO_INCREMENT,
    sender_confirm tinyint(1) DEFAULT 1,
    reciver_confirm tinyint(1) DEFAULT 1,
    start_date datetime NOT NULL DEFAULT current_timestamp(),
    sender int(11) NOT NULL,
    receiver int(11) NOT NULL,
    PRIMARY KEY (id,sender,receiver),
    KEY fk_friends_users1_idx (sender),
    KEY fk_friends_users2_idx (receiver),
    CONSTRAINT fk_friends_users1 FOREIGN KEY (sender) REFERENCES users (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT fk_friends_users2 FOREIGN KEY (receiver) REFERENCES users (id) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
  /*!40101 SET character_set_client = @saved_cs_client */;

  --
  -- Dumping data for table friends
  --

  LOCK TABLES friends WRITE;
  /*!40000 ALTER TABLE friends DISABLE KEYS */;
  INSERT INTO friends VALUES (1,1,1,'2021-07-31 12:38:24',2,1),(2,1,1,'2021-07-31 12:44:48',4,3),(3,1,1,'2021-07-31 12:47:08',5,4),(4,1,1,'2021-07-31 12:47:11',5,1),(5,1,1,'2021-07-31 13:52:58',1,6),(7,1,1,'2021-07-31 13:55:45',7,4),(8,1,1,'2021-07-31 13:55:49',7,1),(11,1,1,'2021-07-31 13:56:19',7,2),(12,1,1,'2021-07-31 13:56:27',7,6),(13,1,1,'2021-07-31 14:06:49',10,9),(14,1,1,'2021-07-31 14:06:54',10,3),(15,1,1,'2021-07-31 14:07:16',1,8);
  /*!40000 ALTER TABLE friends ENABLE KEYS */;
  UNLOCK TABLES;

  --
  -- Table structure for table groups
  --

  DROP TABLE IF EXISTS groups;
  /*!40101 SET @saved_cs_client     = @@character_set_client */;
  /*!40101 SET character_set_client = utf8 */;
  CREATE TABLE groups (
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(45) NOT NULL,
    PRIMARY KEY (id)
  ) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
  /*!40101 SET character_set_client = @saved_cs_client */;

  --
  -- Dumping data for table groups
  --

  LOCK TABLES groups WRITE;
  /*!40000 ALTER TABLE groups DISABLE KEYS */;
  INSERT INTO groups VALUES (1,'admin'),(2,'user');
  /*!40000 ALTER TABLE groups ENABLE KEYS */;
  UNLOCK TABLES;

  --
  -- Table structure for table locked_post
  --

  DROP TABLE IF EXISTS locked_post;
  /*!40101 SET @saved_cs_client     = @@character_set_client */;
  /*!40101 SET character_set_client = utf8 */;
  CREATE TABLE locked_post (
    id int(11) NOT NULL AUTO_INCREMENT,
    date datetime NOT NULL DEFAULT current_timestamp(),
    reason varchar(45) DEFAULT NULL,
    posts_id int(11) NOT NULL,
    users_id int(11) NOT NULL,
    PRIMARY KEY (id,posts_id,users_id),
    KEY fk_locked_post_posts_idx (posts_id),
    KEY fk_locked_post_users1_idx (users_id),
    CONSTRAINT fk_locked_post_posts FOREIGN KEY (posts_id) REFERENCES posts (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT fk_locked_post_users1 FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8;
  /*!40101 SET character_set_client = @saved_cs_client */;

  --
  -- Dumping data for table locked_post
  --

  LOCK TABLES locked_post WRITE;
  /*!40000 ALTER TABLE locked_post DISABLE KEYS */;
  INSERT INTO locked_post VALUES (156,'2021-07-31 13:49:17','Стига глупости',9,6);
  /*!40000 ALTER TABLE locked_post ENABLE KEYS */;
  UNLOCK TABLES;

  --
  -- Table structure for table post_cathegory
  --

  DROP TABLE IF EXISTS post_cathegory;
  /*!40101 SET @saved_cs_client     = @@character_set_client */;
  /*!40101 SET character_set_client = utf8 */;
  CREATE TABLE post_cathegory (
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(45) NOT NULL,
    is_deleted tinyint(1) NOT NULL DEFAULT 0,
    PRIMARY KEY (id)
  ) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
  /*!40101 SET character_set_client = @saved_cs_client */;

  --
  -- Dumping data for table post_cathegory
  --

  LOCK TABLES post_cathegory WRITE;
  /*!40000 ALTER TABLE post_cathegory DISABLE KEYS */;
  INSERT INTO post_cathegory VALUES (1,'JavaScript',0),(2,'PHP',0),(3,'TCL',0),(4,'C#',0),(5,'Python',0),(6,'Perl',0),(7,'Common',0);
  /*!40000 ALTER TABLE post_cathegory ENABLE KEYS */;
  UNLOCK TABLES;

  --
  -- Table structure for table post_likes
  --

  DROP TABLE IF EXISTS post_likes;
  /*!40101 SET @saved_cs_client     = @@character_set_client */;
  /*!40101 SET character_set_client = utf8 */;
  CREATE TABLE post_likes (
    id int(11) NOT NULL AUTO_INCREMENT,
    date datetime DEFAULT current_timestamp(),
    posts_id int(11) NOT NULL,
    users_id int(11) NOT NULL,
    PRIMARY KEY (id,posts_id,users_id),
    KEY fk_post_likes_posts1_idx (posts_id),
    KEY fk_post_likes_users1_idx (users_id),
    CONSTRAINT fk_post_likes_posts1 FOREIGN KEY (posts_id) REFERENCES posts (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT fk_post_likes_users1 FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;
  /*!40101 SET character_set_client = @saved_cs_client */;

  --
  -- Dumping data for table post_likes
  --

  LOCK TABLES post_likes WRITE;
  /*!40000 ALTER TABLE post_likes DISABLE KEYS */;
  INSERT INTO post_likes VALUES (1,'2021-07-31 12:38:41',1,2),(2,'2021-07-31 12:39:26',5,2),(3,'2021-07-31 12:41:36',6,3),(4,'2021-07-31 12:41:54',4,3),(5,'2021-07-31 12:42:20',2,3),(6,'2021-07-31 12:42:25',1,3),(7,'2021-07-31 12:44:42',1,4),(8,'2021-07-31 12:47:22',4,5),(9,'2021-07-31 13:48:16',7,6),(10,'2021-07-31 13:50:31',8,6),(11,'2021-07-31 13:50:51',4,6),(12,'2021-07-31 13:54:06',7,7),(13,'2021-07-31 13:55:20',1,7),(14,'2021-07-31 13:55:34',5,7),(15,'2021-07-31 13:57:04',1,6),(16,'2021-07-31 13:59:35',11,1),(17,'2021-07-31 14:01:03',1,8),(18,'2021-07-31 14:06:42',7,10);
  /*!40000 ALTER TABLE post_likes ENABLE KEYS */;
  UNLOCK TABLES;

  --
  -- Table structure for table posts
  --

  DROP TABLE IF EXISTS posts;
  /*!40101 SET @saved_cs_client     = @@character_set_client */;
  /*!40101 SET character_set_client = utf8 */;
  CREATE TABLE posts (
    id int(11) NOT NULL AUTO_INCREMENT,
    title varchar(45) NOT NULL,
    text text NOT NULL,
    date_created datetime DEFAULT current_timestamp(),
    date_updated datetime DEFAULT current_timestamp(),
    is_deleted tinyint(1) NOT NULL DEFAULT 0,
    post_cathegory_id int(11) NOT NULL,
    users_id int(11) NOT NULL,
    PRIMARY KEY (id,post_cathegory_id,users_id),
    KEY fk_posts_post_cathegory1_idx (post_cathegory_id),
    KEY fk_posts_users1_idx (users_id),
    CONSTRAINT fk_posts_post_cathegory1 FOREIGN KEY (post_cathegory_id) REFERENCES post_cathegory (id) ON DELETE NO ACTION ON UPDATE NO ACTION,
    CONSTRAINT fk_posts_users1 FOREIGN KEY (users_id) REFERENCES users (id) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
  /*!40101 SET character_set_client = @saved_cs_client */;

  --
  -- Dumping data for table posts
  --

  LOCK TABLES posts WRITE;
  /*!40000 ALTER TABLE posts DISABLE KEYS */;
  INSERT INTO posts VALUES (1,'Какво е JavaScript','JavaScript (ДжаваСкрипт) е интерпретируем език за програмиране, разпространяван с повечето Уеб браузъри. Поддържа обектно-ориентиран и функционален стил на програмиране. Създаден е в Netscape през 1995 г. Най-често се прилага към HTML-а на Интернет страница с цел добавяне на функционалност и зареждане на данни. Може да се ползва също за писане на сървърни скриптове JSON, както и за много други приложения. JavaScript не трябва да се бърка с Java, съвпадението на имената е резултат от маркетингово решение на Netscape. Javascript е стандартизиран под името EcmaScript.\nJavaScript е разработен първоначално от Брендан Айк под името Mocha, като по-късно е преименуван на LiveScript и накрая на JavaScript. LiveScript е официалното име на езика когато за първи път бива пуснат в бета версиите на Netscape Navigator 2.0 през септември 1995 г., но е преименуван на JavaScript на 4 декември 1995 г.\nJavaScript е програмен език, който позволява динамична промяна на поведението на браузъра в рамките на дадена HTML страницата. JavaScript се зарежда, интерпретира и изпълнява от уеб браузъра, който му осигурява достъп до Обектния модел на браузъра. JavaScript функции могат да се свържат със събития на страницата (например: движение/натискане на мишката, клавиатурата или елемент от страницата, и други потребителски действия). JavaScript е най-широко разпространеният език за програмиране в Интернет. Прието е JavaScript програмите да се наричат скриптове.','2021-07-31 12:28:48','2021-07-31 12:29:30',0,1,1),(2,'Какво е PHP','PHP е скриптов език върху сървърната (обслужваща) страна. Той е език с отворен код, който е проектиран за уеб програмиране и е широко използван за създаване на сървърни приложения и динамично уеб-съдържание. Автор на езика е канадецът от датски произход Размус Лердорф. PHP е рекурсивен акроним от PHP: Hypertext Preprocessor (като в самото начало има значение, дадено от създателите му, на Personal Home Page). Пример за PHP приложение е МедияУики – софтуерът, използван от Уикипедия.PHP е скриптов език със синтаксис базиран на C и Perl. Използва се предимно в Web-среда за реализиране на широк кръг от услуги. Той е един от най-популярните езици за програмиране в Интернет и популярността му расте непрекъснато.\n\nPHP се разпространява под отворен лиценз (PHP License), който по своята същност е BSD лиценза и който позволява безплатно разпространяване на програмния код на интерпретатора на езика, както и създаването на производни интерпретатори под други лицензи с уговорката, че тези интерпретатори не могат да включват PHP в името си. Фактът, че PHP се разпространява свободно, го прави удачен избор за изграждане на Web-сървър базиран изцяло на свободни продукти – GNU/Linux, Apache, MySQL/PostgreSQL и др.\n\nПри поискване, кодът, който е написан на PHP се интерпретира от уеб сървъра на който е качен, и резултатът се връща на уеб браузърa. Потребителят не може да види чистия PHP код без да има достъп до самия файл в който той е записан. По този начин е помислено за сигурността. PHP файловете могат да съдържат текст, HTML, CSS, JavaScript и PHP код. PHP файловете имат разширение .php.\n\nСамият език е преносим на много изчислителни архитектури и операционни системи като GNU/Linux, UNIX, Mac OS X, Windows.\n\nСъществуват множество модули (разширения) за PHP, които добавят различни функционалности и позволяват много по-бързо и ефективно разработване. Такива допълнителни функционалности към езика са:\n\nфункции за обработка (създаване, редактиране ...) на изображения\nфункции за работа с низове и регулярни изрази\nфункции за работа с XML съдържание\nфункции за работа със сокети (гнезда)\nфункции за дата и час\nматематически функции\nфункции за управление на сесии и работа с бисквитки (cookies)\nфункции за компресия и шифриране/дешифриране\nфункции за COM и .NET за (Windows)\nфункции за SOAP\nфункции за работа с различни бази данни\nфункции за работа с принтер\nфункции за създаване на приложения с графичен потребителски интерфейс, базирани на библиотеката GTK\nфункции за изпращане на e-mail съобщения\nхранилище за разширения и приложения на PHP: PEAR\nPHP може да работи с повечето модерни бази данни – MySQL, PostgreSQL, Microsoft SQL Server, Oracle, SQLite и д.р.\n\nНа официалния сайт на PHP се намира обширна библиотека с информация за езика и модулите му, която може да се използва както за основно запознаване с езика, така и като справочник по време на работата с него.\n\nПоради отворения характер на езика съществуват множество потребителски групи в България и по света посветени на програмирането с PHP, където всеки може да получи помощ в работата си с този език.','2021-07-31 12:30:54','2021-07-31 12:30:54',0,2,1),(3,'Какво е TCL','Tcl е скриптов език за програмиране, създаден през 1988 г. от проф. Джон Оустерхаут в Университета на щата Калифорния, гр. Бъркли. Произнася се като „тикъл“ или „тисиел“ е съкращение от Tool Command Language. Джон Оустерхаут забелязал, че много програми имплементират свой собствен скриптов език и поради тази причина първоначално Tcl е замислен като интерпретируем език, предназначен за лесно вграждане в други програми. След широко публично одобрение, Tcl днес се използва и за скриптиране, бързо прототипиране, изграждане на графични интерфейси, тестиране и др.\n\nКомбинацията от Tcl и Tk (графична библиотека) се нарича Tcl/Tk.\n\nПо своята същност Tcl е интерпретиран език което означава че всяка програма, написана на езика, може да тръгне под коя да е операционна система, за която има подготвен интерпретатор. Tcl споделя качества с други популярни интерпретирани езици като Python, Perl, Ruby, включително и обектно ориентирано програмиране. Езикът има вградени масиви и списъци, като последните, тъй като приемат за ключ обекти от произволен тип, играят ролята и на т.нар. хеш таблици.\n\nЕдно от основните предимства на езика е че е значително по-лесен за научаване и употреба от останалите. Изграждането на графични интерфейси чрез Tk е забележимо лесно и просто, неимоверно по-просто в сравнение с Python например. Редица програмисти предпочитат Tcl именно заради лесното и бързо създаване на крос платформени програми с графичен интерфейс. Допълнително са налице GUI building tools като Visual Tcl и Spec Tcl, които са напълно безплатни.\n\nПонастоящем Tcl се използва от такива компании като IBM, Motorola, Oracle, TiVo и др.\n\nОт сайта на Active State може да се изтегли безплатно интерпретатора който има версии за различни операционни системи – Windows, Линукс, Mac OS. В повечето от Линукс дистрибуциите, интерпретаторът е вграден и не е нужно допълнителното му сваляне. Под Линукс се извиква с командата tclsh. За да се програмират графични приложения с Tk, обаче е нужно извикването на wish.','2021-07-31 12:31:52','2021-07-31 12:31:52',0,3,1),(4,'Какво е C#','C# (C Sharp, произнася се Си Шарп) е обектно-ориентиран език за програмиране, разработен от Microsoft, като част от софтуерната платформа .NET. Стремежът още при създаването на C# езика е бил да се създаде един прост, модерен, обектно-ориентиран език с общо предназначение. Основа за C# са C++, Java и донякъде езици като Delphi, VB.NET и C. Той е проектиран да балансира мощност (C++) с възможност за бързо разработване (Visual Basic и Java). Те представляват съвкупност от дефиниции на класове, които съдържат в себе си методи, а в методите е разположена програмната логика – инструкциите, които компютърът изпълнява. Програмите на C# представляват един или няколко файла с разширение .cs., в които се съдържат дефиниции на класове и други типове. Тези файлове се компилират от компилатора на C# (csc) до изпълним код и в резултат се получават асемблита – файлове със същото име, но с различно разширение (.exe или .dll).\nПървата версия на C# е разработена от Microsoft в периода 1999 – 2002 г. и е пусната официално в употреба през 2002 г., като част от .NET платформата, която има за цел да улесни съществено разработката на софтуер за Windows среда чрез качествено нов подход към програмирането, базиран на концепциите за „виртуална машина“ и „управляван код“. По това време езикът и платформата Java, изградени върху същите концепции, се радват на огромен успех във всички сфери на разработката на софтуер и разработката на C# и .NET е естественият отговор на Microsoft срещу успехите на Java технологията. Едно от най-големите предимства на .NET Framework е вграденото автоматично управление на паметта. То предпазва програмистите от сложната задача сами да заделят памет за обектите и да търсят подходящия момент за нейното освобождаване. Това сериозно повишава производителността на програмистите и увеличава качеството на програмите, писани на C#.\n\n','2021-07-31 12:32:58','2021-07-31 12:33:10',0,4,1),(5,'Какво е Python','Python е интерпретативен, интерактивен, обектно ориентиран език за програмиране, създаден от Гуидо ван Росум в началото на 90-те години. Кръстен е на телевизионното шоу на BBC Monty Python’s Flying Circus. Често бива сравняван с Tcl, Perl, Scheme, Java и Ruby.Идеята за Python се заражда в края на 1980-те[1] години, като реалното осъществяване започва през декември 1989 г.[2] от Гуидо ван Росум в CWI (Centrum Wiskunde & Informatica – международно признат изследователски институт по математика и компютърни науки, локализиран в Амстердам, Холандия). Python имал за цел да се превърне в наследник на ABC (език за програмиране, от своя страна вдъхновен от SETL), който да бъде способен да обработва изключения и да е съвместим с операционната система Amoeba[3]. Ван Росум е основният автор на Python, а неговата продължаваща централна роля в развитието на езика е ясно отразена в титлата, дадена му от Python общността – „пожизнен доброжелателен диктатор“ (benevolent dictator for life, BDFL).Python предлага добра структура и поддръжка за разработка на големи приложения. Той притежава вградени сложни типове данни като гъвкави масиви и речници, за които биха били необходими дни, за да се напишат ефикасно на C.\n\nPython позволява разделянето на една програма на модули, които могат да се използват отново в други програми. Също така притежава голям набор от стандартни модули, които да се използват като основа на програмите. Съществуват и вградени модули, които обезпечават такива събития и елементи като файлов вход/изход (I/O), различни системни функции, сокети (sockets), програмни интерфейси към GUI-библиотеки като Тк и др.\n\nПоради факта, че Python е интерпретативен език, се спестява значително време за разработка, тъй като не са необходими компилиране и свързване (linking) за тестването на дадено приложение. Програмната му идеология е сходна с тази на Java и всяко приложение, написано на него, е сравнително лесно преносимо и в други платформи (или операционни системи).\n\nПрограмите, написани на Python, са доста компактни и четими, като често те са и по-кратки от своите еквиваленти, написани на C/C++. Това е така, понеже:\n\nналичните сложни типове данни позволяват изразяването на сложни действия с един-единствен оператор;\nгрупирането на изразите се извършва чрез отстъп, вместо чрез начални и крайни скоби или някакви ключови думи (друг език, използващ такъв начин на подредба, е Haskell);\nне са необходими декларации на променливи или аргументи;\nPython съдържа прости конструкции, характерни за функционалния стил на програмиране, които му придават допълнителна гъвкавост.\nВсеки модул на Python се компилира преди изпълнение до код за съответната виртуална машина. Този код се записва за повторна употреба като .pyc файл.\n\nПрограмите, написани на Python, представляват съвкупност от файлове с изходен код. При първото си изпълнение този код се компилира до байткод, а при всяко следващо се използва кеширана версия. Байткодът се изпълнява от интерпретатор на Python. Ще отбележим още и следните особености:\n\nЕзикът е строго типизиран (strong typing) – при несъответствие между типовете е необходимо изрично конвертиране.\nЕзикът поддържа и динамична типизация (dynamic typing) – типовете на данните се определят по време на изпълнението. Работата се основава на принципа duck typing – типът на обектите се оценява според техните свойства.\nПоддържа се garbage collector – вътрешната реализация на езика се грижи за управлението на паметта.\nБлоковете се формират посредством отстъп. Като разграничител между програмните фрагменти се използва нов ред.','2021-07-31 12:35:33','2021-07-31 12:35:33',0,5,1),(6,'Какво е Perl','Perl (произнася се „пърл“) е универсален, интерпретативен език за програмиране, създаден от Лари Уол през 1987 г. Лари е трябвало да създава отчети за системата, която е поддържал тогава, но не е съществувал подходящ инструмент за целта (програмата awk не е можела да отваря и затваря файлове въз основа на информацията в тях), което го подтиква да създаде свой специализиран инструмент, който по-късно разпространява безплатно.Първоначално името на езика е било Pearl (произнася се по същия начин – „пърл“). Още преди да излезе първата версия на езика, Лари установява, че съществува език за програмиране на име PEARL. С цел да се избегнат конфликти, а и повлиян от характерната за философията на Unix тенденция към кратки имена, Лари променя името на езика на Perl, без да променя произношението му.\n\nВпоследствие се появяват интерпретации на името като съкращение, най-популярната от които е Practical Extraction and Report Language (в превод: практичен език за извличане и отчети). Съществуват и много други интерпретации, включително и такива от самия Лари Уол, като например шеговитото Pathologically Eclectic Rubbish Lister (в превод: патологично многостранен изброител на глупости). Всички те обаче са бекроними. Името на езика не е съкращение от нищо, поради което се пише само с една главна буква – Perl, а не PERL (за разлика от споменатия език PEARL, чието име наистина е съкращение).\n\nСъществува също така тънка разлика в значението в зависимост от това дали името е изписано с главна буква в началото или изцяло с малки букви. Perl е името на езика за програмиране, което, както (почти) всяко друго име, се пише с главна буква. Под perl се има предвид интерпретатора на езика Perl, т.е. програмата, която се стартира, за да се изпълни даден код на Perl. Името на интерпретатора спазва неписаното правило имената на команди под Юникс да са изцяло с малки букви.В Perl са заимствани концепции от доста езици – C, awk, sed, Lisp и др. Най-силните му черти са регулярните изрази (англ. regular expression, често съкращавано на regex), вградените сложни структури от данни (обикновени и асоциативни масиви) и един от най-големите в света набори от свободно достъпни модули CPAN. На Perl може да се пише процедурно, обектно-ориентирано и функционално (поддържа затваряния, познати още като обвивки, една от най-мощните абстракции в компютърната наука). Perl е слабо типизиран език. Той е интерпретиран, като програмата първо се компилира до машиннонезависими инструкции (байткод), които се изпълняват от интерпретатора. За разлика от Java обаче байткодът не е лесно достъпен, благодарение на което са избегнати редица проблеми с обратната съвместимост. Интерпретаторът на Perl е написан на C и е преносим на огромен брой платформи и операционни системи. Програмата perl2exe и модулът pp могат да бъдат използвани за произвеждане (генериране) на изпълними програми от скриптове на Perl.','2021-07-31 12:36:54','2021-07-31 12:36:54',0,6,1),(7,'Въпрос','Написах една малка игра на JavaScript, обаче освен да мога да играя от името на двамата противници, ми се ще да напиша вариант, в който противник да ми е компютъра - това означава, че трябва да опиша чрез JavaScript начина ми на игра и когато е избран Радио-бутон \"Comp.\" да играе като мой противник.\nДонякъде работи - в някои случаи взема решения, в други още не - има още доста да се програмира ....\n\nСтигнах до момент, в който си задавам въпроса, дали бих могъл да стартирам функции в цикъл, като имам имената на функциите в стрингов масив ? ( В смисъл, че правя проверки в цикъл и при определени условия да стартирам функция, като имам нейното име в масив използвайки управляващата променлива на цикъла за индекс в масива )','2021-07-31 12:43:50','2021-07-31 12:43:50',0,1,3),(8,'Избор на  Framework','Здравейте, сравнително нова съм и не съм запознат особенно добре с Framework-ците в PHP и имам нужда от малко помощ при избор на такъв. До момента съм понаписвал предимно процедурен код и не съм се занимавал с фреъмуърци. Това което искам на 1-во четене да има нещата които търся:\n1. Мултиезичност.\n2. Конфигурационните файлове да са .php с евентуално <?php return array(); ?>\n3. Редактиране и запис на конфигурационните файлове в горния формат подобно на Zend\\Config\\Writer\\PhpArray.\n4. Темплейт системата да е PHP.\n5. Да мога да си избирам ако се наложи да ми връща HTML или JSON.\n6. Да работи добре с MSSQL бази.\n7. Да е сравнително добре защитен.\n\nПроекта който мисля да прехвърля от процедурен код към ООП (ако изобщо успея да се справя) не е особенно голям и няма някакви кой знае какви особенности.\n\nПопаднах на една табличка със сравнения обаче не е много пълна и ще ви помоля ако знаете нещо друго подобно където мога да разгледам както и да споделяте лични впечатления.\n\nБлагодаря за отделеното време.\n','2021-07-31 12:46:27','2021-07-31 12:46:27',0,2,4),(9,'Tool Command L','Чудя се какво е това ограничение до 20 символа за заглавие на тема?','2021-07-31 12:48:29','2021-07-31 13:48:54',0,3,5),(10,'Откровения','Много хубаво не е на хубаво.\nПървият програмист седял под клоните на едно дърво предъвквайки стръкче от цвят на недефинирано растение.\nВерният алгоритъм се шматкал наоколо и се чудел какво да прави.\nВсичко било конфигурирано идеално.\nТоплите лъчи на следобедното слънце преминавали през клоните на дърветата и придавали топли тонове на зелената трева.\nБило идеално време за магарии.\nИ тъкмо когато Първият бил пред дилемата: \"На тая кълка ли ми е по-удобно, или на другата\", те се изтърсили.\nНа една не много отдалечена полянка Единствената програмистка подреждала сорс от цветенца за компилация на букет.\nОт един храст наблизо се появила една от версиите на Лукавия.\nДолазил той до Единствената, и почнал с диващините си:\n\"Hi. a/s/l?\"\nОбаче Единствената била умна жена, и то с характер. Погледнала го мило и с меден глас му казала: \"Чупка бе, кретен.\"\nИ всичко можело да свърши дотук, ама оная версия на Лукавия не предвиждала такъв отговор, и зациклила:\n\"Hi. a/s/l? Hi. a/s/l? Hi. a/s/l? Hi. a/s/l? Hi. a/s/l? Hi. a/s/l?\"...\nИ понеже Единствената била добра душа, решила да отърве горкото създание от безкраен цикъл, и му отговорила:\n\"timeGetTime()/f/localhost.\"\nОбаче версията продължила с дивотиите:\n\"Кво си правиш?\".\n\"Компилирам маргаритки\" простичко казала Единствената.\nИ все в тоя дух продължила версията на Лукавия, ама няма да ви занимавам със скучни въпроси, щото всички ги знаете.\nАма дошла по едно време странна констатация:\n\"Живот си живеете вие с Първия. Почти всичко си имате\".\nЕдинствената се ококорила: \"Как така почти? Ние всичко си имаме?\"\nИ тогава Лукавия нанесъл своя удар. Щото знаел как да влезе по кожата на жена.\nПочнал той да мрънка, да се инати и да прилага умения за които министър на външните работи само може да мечтае:\n\"Ми нали да де, то както каза по-рано, аз не исках да те засягам, ама нали не можете, то си имате, щото ти да не се обидиш ако ти го кажа, то никой не може ей-така да получи, ама нали сама, каза че не пипате забраненото, ама не се обиждай, и ако вземеш да го разбереш по друг начин, ще излезе че те свалям, ама не си мисли такова нещо, искам да кажа и аз не си го мисля, въпреки че ако нямаш планове тая вечер, то и аз съм свободен... ъъъъ, кво ще правиш с тоя нож?\"\nКакто се убеждавате сами, много внимателно бил премислил думите си Лукавия. Знаел той, че жената е същество което може да следи многоканална асинхронна трансмисия (била тя криптирана или не), и посял в Единствената прекъсванията на чувството, че се опитва да я отнесе от разговора за почтивсичкоимането.\nПо нататък било елементарно, той я държал вече в ръцете си.\nЕ, да де, като се абстрахирамe от това че Единствената го държала за шията и блъскала главата му в едно много неподатливо дърво, той я държал в ръцете си.\nТя по едно време спряла да го блъска, и му дала втори опит.\nТой всичко си казал.\nИ Единствената се убедила, че на всяка цена трябва да опита забраненото.\nПо-късно тя галантно убедила и Първият. Той се съгласил още щом го хванала за шията.\n(Трябва да се отбележи от гледна точка на разумната мисъл, че Първият отдавна бил хвърлил око на забраненото, тъй като бил програмист следователно любопитен. Но явно не искал да спомене че му се иска да опита забраненото, щото Единствената щяла да му избие тази мисъл, или което е по-лошо - можела да го убеди сам да си я избие).\nИ двамата веднага опитали забраненото.\nА АДМИНИСТРАТОРЪТ прочел логовете.\nЯвил се той пред двамата и се почнало... (Следва точен цитат)\n***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА***\n***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА*** ***ЦЕНЗУРА***\nОт тези му думи листата на дърветата окапали, а слънцето обявило залеза предсрочно.\nПогледнал после Единствената учудено и казал:\n\"Какво му се смееш, и за тебе се отнася\".\nПоел си АДМИНИСТРАТОРЪТ малко дъх, и заключил:\n\"И да не сте ми стъпили вече в провайдера.\"\nПървият и Единствената стояли омърлушени пред него и се чудели какво да кажат.\nАма добре че не се сетили.\nНакрая АДМИНИСТРАТОРЪТ трябвало да им направи мръсно, както по-рано им бил казал и следното нещо измислил:\nВзел той Верният Алгоритъм и го разделил на три.\nПървата част нарекъл Красота, и я подал на Единствената.\nВтората част нарекъл Логика, и я подал на Първият.\nТретата част нарекъл Вдъхновение, и я пръснал по света.\nИ казал им следното, с усмивка на шеф преди съкращения:\n\"Живот досега си живеехте, ама сега да ви видя. Първият програмист с логика ще блести, ама дизайна няма да му се отдава. Единствената красоти ще сътворява, ама с ум няма да блести. Двамата да се търсите, и все да се не намирате, а ако се намерите не споделяте ли вдъхновение, един друг да си издерете очите.\"\nПосле kick-ban от Рая в различни посоки.\nПо-късто през нощта, следните събития станали:\nПървият програмист лежеше под едно дърво и си мислеше: \"Тц. Лявата кълка е по-удобна\".\nЕдинствената програмистка плуваше в едно езерце на лунна светлина, и си мислеше: \"Трябваше аз да ударя RESET на Лукавия\".\nА пред портите райски дребната фигурка на частица от Верния Алгоритъм се тресеше от рев, и викаше неуморно:\n\"Пусни ме вътре бе идиоооооооооооот! Аз не съм ти пипал скапания Apple!!!!\"','2021-07-31 13:47:55','2021-07-31 13:47:55',0,7,6),(11,'Презапис на адрес','Здравейте.\n\nКак може да презапишат в htaccess адресите\nсега са така\n\nhttp://sait.com/index.php?id=1-2-3\n\nда се получи\n\nhttp://sait.com/1/2/3/\n\n\nПредварително благодаря','2021-07-31 13:58:34','2021-07-31 13:58:34',0,2,6);
  /*!40000 ALTER TABLE posts ENABLE KEYS */;
  UNLOCK TABLES;

  --
  -- Table structure for table tokens
  --

  DROP TABLE IF EXISTS tokens;
  /*!40101 SET @saved_cs_client     = @@character_set_client */;
  /*!40101 SET character_set_client = utf8 */;
  CREATE TABLE tokens (
    id int(11) NOT NULL AUTO_INCREMENT,
    token varchar(255) NOT NULL,
    PRIMARY KEY (id)
  ) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
  /*!40101 SET character_set_client = @saved_cs_client */;

  --
  -- Dumping data for table tokens
  --

  LOCK TABLES tokens WRITE;
  /*!40000 ALTER TABLE tokens DISABLE KEYS */;
  INSERT INTO tokens VALUES (1,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJmaWRhbmEiLCJncm91cCI6MSwiZmlyc3ROYW1lIjoi0KTQuNC00LDQvdCwIiwibGFzdE5hbWUiOiLQmNCy0LDQvdC-0LLQsCIsImlhdCI6MTYyNzcyMDc4MSwiZXhwIjoxNjI3ODA3MTgxfQ.gp_sNL9dkCHK58EFJ5s4Ptv7IePfauiZOmonJK3FYP0'),(2,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJyb290IiwiZ3JvdXAiOjIsImZpcnN0TmFtZSI6bnVsbCwibGFzdE5hbWUiOm51bGwsImlhdCI6MTYyNzcyMzQ3MiwiZXhwIjoxNjI3ODA5ODcyfQ.gxhHJbkZX6R2ZeRMdHxqdtZYpP8Jm1sL7E2l52BjRJY'),(3,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJyb290IiwiZ3JvdXAiOjEsImZpcnN0TmFtZSI6ItCk0LjQtNCw0L3QsCIsImxhc3ROYW1lIjoi0JjQstCw0L3QvtCy0LAiLCJpYXQiOjE2Mjc3MjM1NDAsImV4cCI6MTYyNzgwOTk0MH0.XMTb9HgIh8fOiUg0Et4QGCY_umfICguXRr76WYTNugs'),(4,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidXNlcm5hbWUiOiJlbGthIiwiZ3JvdXAiOjIsImZpcnN0TmFtZSI6bnVsbCwibGFzdE5hbWUiOm51bGwsImlhdCI6MTYyNzcyNDI3MSwiZXhwIjoxNjI3ODEwNjcxfQ.ypVj8R8szMGZmSykZG0CM51dOiEbGBl_zR5iv9mnp3g'),(5,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NCwidXNlcm5hbWUiOiJtYXlhIiwiZ3JvdXAiOjIsImZpcnN0TmFtZSI6bnVsbCwibGFzdE5hbWUiOm51bGwsImlhdCI6MTYyNzcyNDY2MSwiZXhwIjoxNjI3ODExMDYxfQ.geTHOZvNX1cv1QzHodWJ-R_ZPLiUKVhyBYPR1DmVP2U'),(6,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NSwidXNlcm5hbWUiOiJsYW1lciIsImdyb3VwIjoyLCJmaXJzdE5hbWUiOm51bGwsImxhc3ROYW1lIjpudWxsLCJpYXQiOjE2Mjc3MjQ4MTAsImV4cCI6MTYyNzgxMTIxMH0._i8J6_g_5r8MnVi8PDXYv8pj7QiKuknmsC8Qjsn9mqo'),(7,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NiwidXNlcm5hbWUiOiJmaWRhbmEiLCJncm91cCI6MiwiZmlyc3ROYW1lIjpudWxsLCJsYXN0TmFtZSI6bnVsbCwiaWF0IjoxNjI3NzI4MzY5LCJleHAiOjE2Mjc4MTQ3Njl9.Yy1G8TpHr4j4_sZobDBajWNHr3MqKM97s_3Z0UvJGPo'),(8,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NiwidXNlcm5hbWUiOiJmaWRhbmEiLCJncm91cCI6MSwiZmlyc3ROYW1lIjoi0KTQuNC00LDQvdCwIiwibGFzdE5hbWUiOiLQmNCy0LDQvdC-0LLQsCIsImlhdCI6MTYyNzcyODQxMCwiZXhwIjoxNjI3ODE0ODEwfQ.ijjbrwQ0_0VjZ04fPdD0XXaXbW5urqMdAEfjmpO8mU4'),(9,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJyb290IiwiZ3JvdXAiOjEsImZpcnN0TmFtZSI6ItCk0LjQtNCw0L3QsCIsImxhc3ROYW1lIjoi0JjQstCw0L3QvtCy0LAiLCJpYXQiOjE2Mjc3Mjg3NTUsImV4cCI6MTYyNzgxNTE1NX0.n4xinLpnPBfxV-RihbKN7nXKuLv4LKE_8q1bv_fk9Ss'),(10,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywidXNlcm5hbWUiOiJwZW5rYSIsImdyb3VwIjoyLCJmaXJzdE5hbWUiOm51bGwsImxhc3ROYW1lIjpudWxsLCJpYXQiOjE2Mjc3Mjg4MTcsImV4cCI6MTYyNzgxNTIxN30.gWBTmvkjxXgaxRzNp3Y8OqflhJil90LL8-DmBxN-iME'),(11,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NiwidXNlcm5hbWUiOiJmaWRhbmEiLCJncm91cCI6MSwiZmlyc3ROYW1lIjoi0KTQuNC00LDQvdCwIiwibGFzdE5hbWUiOiLQmNCy0LDQvdC-0LLQsCIsImlhdCI6MTYyNzcyODk5NSwiZXhwIjoxNjI3ODE1Mzk1fQ.yht-ax5zsL7sGtgM_oFxjshY3fFM7791Hh9l4rrg5_s'),(12,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJyb290IiwiZ3JvdXAiOjEsImZpcnN0TmFtZSI6ItCk0LjQtNCw0L3QsCIsImxhc3ROYW1lIjoi0JjQstCw0L3QvtCy0LAiLCJpYXQiOjE2Mjc3MjkxMzksImV4cCI6MTYyNzgxNTUzOX0.nrZdImPXKri1sNnuJQmlfbtoLn83YA_M_omIwB4F1pM'),(13,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OCwidXNlcm5hbWUiOiJ6YXJrYSIsImdyb3VwIjoyLCJmaXJzdE5hbWUiOm51bGwsImxhc3ROYW1lIjpudWxsLCJpYXQiOjE2Mjc3MjkyMTIsImV4cCI6MTYyNzgxNTYxMn0._O00cOAp0BJXzCDtUiZLys_thSSS8EZNvKEOyJEUpp8'),(14,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NiwidXNlcm5hbWUiOiJmaWRhbmEiLCJncm91cCI6MSwiZmlyc3ROYW1lIjoi0KTQuNC00LDQvdCwIiwibGFzdE5hbWUiOiLQmNCy0LDQvdC-0LLQsCIsImlhdCI6MTYyNzcyOTM0NywiZXhwIjoxNjI3ODE1NzQ3fQ.kkbiRS8n-FNRyKu1pI46cP2BxzuFTCjt3J8Sjczw0MI'),(15,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OSwidXNlcm5hbWUiOiJ2YXNrbyIsImdyb3VwIjoyLCJmaXJzdE5hbWUiOm51bGwsImxhc3ROYW1lIjpudWxsLCJpYXQiOjE2Mjc3Mjk0MTYsImV4cCI6MTYyNzgxNTgxNn0.NidrkOAyYIS3vGw0fIw_e_BR54_4saUrSaP92mfcXsk'),(16,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTAsInVzZXJuYW1lIjoiaWxrYSIsImdyb3VwIjoyLCJmaXJzdE5hbWUiOm51bGwsImxhc3ROYW1lIjpudWxsLCJpYXQiOjE2Mjc3Mjk1MzEsImV4cCI6MTYyNzgxNTkzMX0.vhXqAqcbJUFL0gVi2If5DKBrZ0ERs-13uaJx7Y3J0Fg'),(17,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJyb290IiwiZ3JvdXAiOjEsImZpcnN0TmFtZSI6ItCk0LjQtNCw0L3QsCIsImxhc3ROYW1lIjoi0JjQstCw0L3QvtCy0LAiLCJpYXQiOjE2Mjc3Mjk2MjgsImV4cCI6MTYyNzgxNjAyOH0.GDangXgAqRNeZoGzFbEE6xKvRlL1jJzCtq8DuWhjr8I');
  /*!40000 ALTER TABLE tokens ENABLE KEYS */;
  UNLOCK TABLES;

  --
  -- Table structure for table users
  --

  DROP TABLE IF EXISTS users;
  /*!40101 SET @saved_cs_client     = @@character_set_client */;
  /*!40101 SET character_set_client = utf8 */;
  CREATE TABLE users (
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(45) NOT NULL,
    password varchar(255) NOT NULL,
    e_mail varchar(45) DEFAULT NULL,
    first_name varchar(45) DEFAULT NULL,
    last_name varchar(45) DEFAULT NULL,
    registration_date datetime NOT NULL DEFAULT current_timestamp(),
    is_deleted tinyint(1) NOT NULL DEFAULT 0,
    groups_id int(11) NOT NULL,
    PRIMARY KEY (id,groups_id),
    UNIQUE KEY name_UNIQUE (name),
    UNIQUE KEY e_mail_UNIQUE (e_mail),
    KEY fk_users_groups1_idx (groups_id),
    CONSTRAINT fk_users_groups1 FOREIGN KEY (groups_id) REFERENCES groups (id) ON DELETE NO ACTION ON UPDATE NO ACTION
  ) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
  /*!40101 SET character_set_client = @saved_cs_client */;

  --
  -- Dumping data for table users
  --

  LOCK TABLES users WRITE;
  /*!40000 ALTER TABLE users DISABLE KEYS */;
  INSERT INTO users VALUES (1,'root','$2b$10$XWgXVDmpwB2EaIcNgKwzw.1YHAjPHqu5je.smMyvzKSFHzK4TIwbi','root@fidana.net','Фидана','Иванова','2021-07-31 12:24:32',0,1),(2,'elka','$2b$10$97BlVWhPF1V2Mn9PQPduTOBSMcKXTRTylz4W1d8/t1.dPK9lyG3h6','elka@fidana.net','Елена','Маринова','2021-07-31 12:37:51',0,2),(3,'galina','$2b$10$RIpEKM5fWtzVFS2avVxKruX8OxxBm9bB7DXVeTaWQKazHUVBlEEDy','galia@fidana.net','Галина','Бумбалова','2021-07-31 12:40:29',0,2),(4,'maya','$2b$10$RjFqNK/d5TEAb22k4HH1a.6jHq6hJ.t/33rV6.94ejePu01uuCooq','root@microsoft.com','Мая','Николова','2021-07-31 12:44:20',0,2),(5,'lamer','$2b$10$2Ir7LOGWldSvYCxFI2BoV.MWSAE3J3M.e/4Xv18V7ov7sW5haWZHS',NULL,NULL,NULL,'2021-07-31 12:46:50',0,2),(6,'fidana','$2b$10$Xde160r.qtDBhFDvn9h9Cu8/eYDUjEh4jQ7PUFkAcrmNgIZDLZvTW','fidana@fidana.net','Фидана','Иванова','2021-07-31 13:46:09',0,1),(7,'penka','$2b$10$e2TWIXn1BuZPSrQ3qqYCXuTYryM7lVZOgoAHge26U5PsbVyRd3DpW','penka@yahoo.com','Пенка','Паткова','2021-07-31 13:53:37',0,2),(8,'zarka','$2b$10$tKI9e.Kj930R4XkpLzMDWe2YAaT/bbOX/wdBRgAQUossBwHljkO8C','zara@dir.bg','Захаринка','Бундова','2021-07-31 14:00:11',0,2),(9,'vasko','$2b$10$pumgGG7mpuUhjzVYSwEUs.OykX4555cekmDiECUmGZ8t2t5J4aity','vasia@yandex.ru','Василий','Кошичкин','2021-07-31 14:03:36',0,2),(10,'ilka','$2b$10$AMHhkY7aY0g162oqJEDMg.Y/fOOMnXOAMJ96rRX170YNeJqRTbIQm','ilka@linuxmail.org','Илка','Зунг Льонг','2021-07-31 14:05:31',0,2);
  /*!40000 ALTER TABLE users ENABLE KEYS */;
  UNLOCK TABLES;
  /*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

  /*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
  /*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
  /*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
  /*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
  /*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
  /*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
  /*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

  -- Dump completed on 2021-07-31 14:31:57`;



logServer.yellow('STARING CREATING DATABASE');
logServer.yellow('-------------------------');

tempPool.query(sqlQuery)
  .then(x => {
    logServer.green('DATABASE CREATED');
    tempPool.end();
  })
  .catch(err => logServer.red(err.message));