export const logServer = {
    /**
     * Logging on console with red color
     * @param { String } text string for logging
     */
    red(text) {
      console.log (`\x1b[31m${text}\x1b[0m`);
    },
  
    /**
     * Logging on console with green color
     * @param { String } text string for logging
     */
    green(text) {
      console.log (`\x1b[32m${text}\x1b[0m`);
    },
  
    /**
     * Logging on console with yellow color
     * @param { String } text string for logging
     */
    yellow(text) {
      console.log (`\x1b[33m${text}\x1b[0m`);
    },
  
    /**
     * Logging on console with blue color
     * @param { String } text string for logging
     */
    blue(text) {
      console.log (`\x1b[34m${text}\x1b[0m`);
    },
  
    /**
     * Logging on console with magenta color
     * @param { String } text string for logging
     */
    magenta(text) {
      console.log (`\x1b[35m${text}\x1b[0m`);
    },
  
    /**
     * Logging on console with cyan color
     * @param { String } text string for logging
     */
    cyan(text) {
      console.log (`\x1b[36m${text}\x1b[0m`);
    },
  
  };
