export default {
  userIdToken: 'user.id',
  userIdParam: 'params.id',
  postAuthorId: 'params.authorId',
  postId: 'params.postId',
  postBodyId: 'body.postId'
};