import express from 'express';
import usersGroups from '../common/users-groups.js';
// import accessParameters from '../common/access-parameters.js';
import { checkPostExistById, getAuthorId } from '../data/post-data.js';
import { checkCommentIsMine } from '../data/comment-data.js';


/**
 * Allow only members of specific group to continue
 * @param { number } group Group number who have permission to use this resource
 */
export const checkUserPermission = (group) => {
    /**
   * 
   * @param {express.Request} req 
   * @param {express.Response} res 
   * @param {express.NextFunction} next 
   */
  return async (req, res, next) => {
    if (group !== req.user.group) {
      return res.status(403).send({ message: 'You don\'t have permission to use this resource!' });
    }  
    
    next();
  };
};

/**
 * Allow only user who own this resource or admin to continue based on req.params
 * @param {express.Request} req 
 * @param {express.Response} res 
 * @param {express.NextFunction} next 
 */
export const rightUserOrAdmin = (firstComp, SecondComp) => (req, res, next) => {
  const fParams = firstComp.split('.');
  const sParams = SecondComp.split('.');
  
  const firstTarget = req[fParams[0]][fParams[1]];
  const secondTarget = req[sParams[0]][sParams[1]];
  
  if (+firstTarget !== +secondTarget && +req.user.group !== usersGroups.ADMIN) {
    return res.status(401).send ({ message: `You're not authorized to do that!` });
  }

  next();
};


/**
 * Checks if the user is owner or admin
 * @param {*} userGroup 
 * @param {*} postId 
 * @returns 
 */
export const postOwnerOrAdmin = (userGroup, postId) => async (req, res, next) => {
  const fParams = postId.split('.');
  const pId = req[fParams[0]][fParams[1]];

  if (!await checkPostExistById(pId)) {
    return res.status(404).send ({ message: `This post don't exist!` });
  }

  if (userGroup === req.user.group) {
    return next();
  }

  const authorId = ((await getAuthorId(pId))[0]).users_id;
  if (req.user.id === authorId) {
    return next();
  }
  
  return res.status(401).send ({ message: `You're not authorized to do that!` });
};


/**
 * Allow only user who own this comment or admin to continue
 * @param {express.Request} req 
 * @param {express.Response} res 
 * @param {express.NextFunction} next 
 */
export const commentOwnerOrAdmin = async (req, res, next) => {
  if (!await checkCommentIsMine(req.user.id, req.params.id) && req.user.group !== usersGroups.ADMIN)
  {
    return res.status(404).send ({ message: `You're not authorized to do that!` });
  }
  return next();
};