import express from 'express';
import usersGroups from '../common/users-groups.js';
import { checkCommentIsMine, checkFlagThisComment, checkLikeThisComment,
    checkPostIdAndCommentId, commentIsApproved, flagUserList} from '../data/comment-data.js';


/**
 * Check about possible actions over this comment
 * @param { string } action 
 * @returns
 */
export const likesStatusValidator = (action) => {  
  /**
   * @param { express.Request } req
   * @param { express.Response } res
   * @param { express.NextFunction } next
   */
  return async (req, res, next) => {
    if (!await checkPostIdAndCommentId (req.firstParams.postId, req.params.id)) {
      return res.status(404)
        .send(
          { message:
            `post with ID ${req.firstParams.postId} don't have comment with id ${req.params.id}`
          });
    };

    if (await checkCommentIsMine(req.user.id, req.params.id)) {
      return res.status(403).send({ message: `can't like or unlike you own comment!` });
    };

    const checkLike = await checkLikeThisComment(req.user.id, req.params.id);

    if (action === 'like' && checkLike) {
      return res.status(403).send({ message: `you already like this comment!` });
    };

    if (action === 'unlike' && !checkLike) {
      return res.status(403).send({ message: `first must LIKE this comment!` });
    };

    return next();
  };
};



export const flagStatusValidator = (action) => {
  /**
   * @param { express.Request } req
   * @param { express.Response } res
   * @param { express.NextFunction } next
   */
  return async (req, res, next) => {
    if (!await checkPostIdAndCommentId (req.firstParams.postId, req.params.id)) {
      return res.status(404)
        .send(
          { message:
            `post with ID ${req.firstParams.postId} don't have comment with id ${req.params.id}`
          });
    };

    if (await commentIsApproved(req.params.id)) {
      return res.status(403).send({ message: `This post already approved by admin!` });
    }

    if (await checkCommentIsMine(req.user.id, req.params.id)) {
      return res.status(403).send({ message: `can't flag or unFlag you own comment!` });
    };

    const checkFlag = await checkFlagThisComment(req.user.id, req.params.id);
    if (action === 'flag' && checkFlag) {
      return res.status(403).send({ message: `you already flag this comment!` });
    };
    

    if (action === 'unFlag' && !checkFlag) {
      return res.status(403).send({ message: `first must FLAG this comment!` });
    };
    
    return next();
  };
};