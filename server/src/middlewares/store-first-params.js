import express from 'express';
import { checkPostExistById, checkPostLock } from '../data/post-data.js';

/**
 * Get first params from Request and store it like new prop
 * firstParams IF Post exist and is not locked
 * @param {express.Request} req 
 * @param {express.Response} res 
 * @param {express.NextFunction} next 
 */
export const storeFirstParams = async (req, res, next) => {

  if (!await checkPostExistById(req.params.postId)) {
    return res.status(404).send ({ message: `This post don't exists!` });
  };
  
  if (await checkPostLock(req.params.postId) && req.method !== 'GET') {
    return res
      .status(404).send ({ message: `Can't add, change or delete comments from locked post` });
  };

  req.firstParams = req.params;
  next();
};