import fs from 'fs';
import express from 'express';
import dotenv from 'dotenv';
import { logServer } from '../common/help-functions.js';

/**
 * Check for log folder and if don't exists create it
 */
const logDir = dotenv.config().parsed.LOG_DIR;
if (!(fs.existsSync(logDir))) {
  fs.mkdirSync(logDir);
  logServer.yellow(`Log directory ${logDir} NOT found - created it now!`);
} else {
  logServer.green(`Log directory ${logDir} found`);
};



/**
 * Create record in log file
 * @param {express.Request} req 
 * @param {express.Response} res 
 * @param {express.NextFunction} next
 */
export const fileLogger = (req, _, next) => {
  const path = `${logDir}/${new Date().toLocaleDateString('fr-CA')}-express.log`;
  
  const time = new Date().toLocaleTimeString('en-GB');

  const logString = `${time} - ${req.method} - ${req.originalUrl} - ${req.path} - ${req.headers['user-agent']}.\n`;

  fs.appendFile(path, logString, (err) => err ? err.message : true);
  
  next();
};




