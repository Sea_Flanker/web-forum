import { logServer } from '../common/help-functions.js';
import { checkOldTokens } from '../data/user-data.js';

export const oldTokenCheck = async (req, res, next) => {
    const myToken = req.headers.authorization.replace('Bearer ', '');
    
    if(await checkOldTokens(myToken)) {
        logServer.red ('SOMEONE USE TOKEN FROM BLACK LIST!');
        return res.status(401).send ({ message: 'Used token. Create a new one.'});
    }
    return next();
};