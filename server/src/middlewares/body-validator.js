/**
 * Validator of the information in the body
 * @param {*} validator 
 * @returns 
 */
export const bodyValidator = (validator) => async (req, res, next) => {  
  let errorMessage = [];

  Object.keys(validator).forEach(x => {
    if(validator[x](req.body[x]) !== true) {
      errorMessage.push(validator[x](req.body[x]));
    }
  });
    
  if (errorMessage.length) {
    return res.status(404).json({ message: errorMessage.join(`; `) });
  }
  
  await next();
};


