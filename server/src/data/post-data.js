import { logServer } from '../common/help-functions.js';
import { pool } from './pool.js';


/**
 * Check if a group exists
 * @param {*} groupName 
 * @returns 
 */
export const checkGroupExist = async (groupName) => {
  const sqlQuery = `SELECT name FROM post_cathegory WHERE name = ?`;
  return (await pool.query(sqlQuery, [groupName]))[0] ? true : false;
};


/**
 * Check group with ID exists
 * @param { number } groupId 
 * @returns { boolean } true if group exists, false if not
 */
export const checkGroupById = async (groupId) => {
  const sqlQuery = `SELECT id FROM post_cathegory WHERE id = ? AND is_deleted = 0`;
  return (await pool.query(sqlQuery, [groupId]))[0] ? true : false;
};


/**
 * Return post group ID
 * @param { number } postId 
 * @returns { object } Object with id field
 */
export const getPostGroup = async (postId) => {
  const sqlQuery = `SELECT post_cathegory_id FROM posts WHERE id = ? AND is_deleted = 0`;
  return (await pool.query(sqlQuery, [postId]))[0];
};


/**
 * Check this user already like this post
 * @param { number } userId 
 * @param { number } postId
 * @returns { Promise <boolean> }
 */
export const checkUserLikeThisPost = async (userId, postId) => {
  const sqlQuery = `SELECT * FROM post_likes WHERE users_id = ? AND posts_id = ?`;
  const result =  await pool.query(sqlQuery, [userId, postId]);
  return result[0] ? true : false;
};


/**
 * Check if post name exists
 * @param {*} postTitle 
 * @returns 
 */
export const checkPostNameExist = async (postTitle) => {
  const sqlQuery = `SELECT title FROM posts WHERE title = ?`;
  return (await pool.query(sqlQuery, [postTitle]))[0] ? true : false;
};


/**
 * Get user group
 * @param { number } userId 
 * @returns { number } group ID
 */
export const getUserGroup = async (userId) => {
  const sqlQuery = `
    SELECT g.id FROM users as u JOIN groups as g ON g.id = u.groups_id WHERE u.id = ?`;
  
  return (await pool.query(sqlQuery, [userId]))[0].id;
};


/**
 * Check post is locked
 * @param { number } postId 
 * @returns { Promise <number> } User who lock post ID or null if post is not locked
 */
export const checkPostLock = async (postId) => {
  const cPost = (await pool.query(`SELECT users_id FROM locked_post WHERE posts_id = ?`, [postId]));
  return cPost.length ? cPost[0].users_id : null;
};


/**
 * Return reason for locking
 * @param { number } postId 
 * @returns 
 */
export const getLockedReason = async (postId) => {
  if (await checkPostLock(postId)) {
    const sqlQuery = `SELECT reason FROM locked_post WHERE posts_id = ?`;
    return (await pool.query(sqlQuery, [postId]))[0].reason;
  } else {
    return null;
  }
};


/**
 * Create post group
 * @param {*} groupName 
 * @returns 
 */
export const createPostGroup = async (groupName) => {

  const sqlQuery = `INSERT INTO post_cathegory(name) VALUES(?)`;
  // const sqlQueryResult = `SELECT id as id, name as groupName FROM post_cathegory WHERE id = ?`;
  const sqlQueryResult = `SELECT pc.id as categoryId, pc.name as category,
  (SELECT COUNT(*) FROM posts WHERE post_cathegory_id = pc.id AND is_deleted = 0) as postsCount,
  (SELECT COUNT(*) FROM comments as c JOIN posts as p ON c.posts_id = p.id
	WHERE p.post_cathegory_id = pc.id and p.is_deleted = 0 AND c.is_deleted = 0) as commentsCount,
  ((SELECT pz.date_updated FROM posts as pz WHERE pz.post_cathegory_id = pc.id and pz.is_deleted = 0
    UNION
    SELECT c.date_updated FROM comments as c JOIN posts as p ON c.posts_id = p.id
    WHERE p.post_cathegory_id = pc.id and p.is_deleted = 0 AND c.is_deleted = 0
    ORDER BY date_updated DESC LIMIT 1))as lastActivity
  FROM post_cathegory as pc
  WHERE is_deleted = 0 AND pc.id = ?`;
  
  try {
    const newGroup = await pool.query(sqlQuery, [groupName]);
    const result = await pool.query(sqlQueryResult, [newGroup.insertId]);
    return result;
  } catch (err) {
    console.log (err.message);
    return null;
  }
};


/**
 * Update group name
 * @param {string} newData 
 * @param {number} groupId 
 * @returns 
 */
export const updateGroupById = async (newData, groupId) => {
  const sqlQuery = `UPDATE post_cathegory SET name = ? WHERE id = ?`;
  const sqlQueryResult = `SELECT id as id, name as groupName FROM post_cathegory WHERE id = ?`;
  try {
    await pool.query(sqlQuery, [newData, groupId]);
    const result = await pool.query(sqlQueryResult, [groupId]);
    return result;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};


/**
 * Delete group by id
 * @param { number } groupId 
 * @returns 
 */

export const deleteGroupById = async (groupId) => {
  const sqlQueryResult = `SELECT id as id, name as groupName FROM post_cathegory WHERE id = ?`;
  const sqlQueryDelete = `UPDATE post_cathegory SET is_deleted = 1 WHERE id = ?`;
  const sqlQueryPosts = `UPDATE posts SET is_deleted = 1 WHERE post_cathegory_id = ?`;

  try {
    const result = await pool.query(sqlQueryResult, [groupId]);

    await pool.query(sqlQueryDelete, [groupId]);
    await pool.query(sqlQueryPosts, [groupId]);
    return result;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};


/**
 * Change post group
 * @param { number } groupId 
 * @param { number } postId 
 */
export const movePostInOtherGroup = async (groupId, postId) => {
  const sqlQuery = `UPDATE posts SET post_cathegory_id = ? WHERE id = ?`;

  try {
    await pool.query(sqlQuery, [groupId, postId]);
    return await getPostsByID(postId);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};


/**
 * Create new post
 * @param {*} newPostData 
 * @param {*} author 
 * @returns 
 */
export const createNewPost = async (newPostData, author) => {
  const sqlCreate = `
    INSERT INTO posts(title, text, post_cathegory_id, users_id)
    VALUES (?, ?, ?, ?)`;

  const sqlGetInfo = `
  SELECT
  p.id, p.title, p.text, p.date_created as createdDate, p.date_updated as lastUpdated,
  u.id as authorId, u.name as authorUsername,
  pc.id as categoryId, pc.name as categoryName
  FROM posts as p
  JOIN users as u ON p.users_id=u.id
  JOIN post_cathegory as pc ON p.post_cathegory_id=pc.id
  WHERE p.id = ?`;

  try {
    const newPost = await pool
      .query(sqlCreate, [newPostData.title, newPostData.text, newPostData.category, author.id]);
    const result = await pool.query(sqlGetInfo, [newPost.insertId]);
    return result;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};


/**
 * Get all posts
 * @param {number} count 
 * @returns 
 */
export const getAllPostsByGroup = async (count = 1) => {
  const sqlCategory = `
  SELECT pc.id as categoryId, pc.name as category,
  (SELECT COUNT(*) FROM posts WHERE post_cathegory_id = pc.id AND is_deleted = 0) as postsCount,

  (SELECT COUNT(*) FROM comments as c JOIN posts as p ON c.posts_id = p.id
	WHERE p.post_cathegory_id = pc.id and p.is_deleted = 0 AND c.is_deleted = 0) as commentsCount,

  ((SELECT pz.date_updated FROM posts as pz WHERE pz.post_cathegory_id = pc.id and pz.is_deleted = 0
    UNION
    SELECT c.date_updated FROM comments as c JOIN posts as p ON c.posts_id = p.id
    WHERE p.post_cathegory_id = pc.id and p.is_deleted = 0 AND c.is_deleted = 0
    ORDER BY date_updated DESC LIMIT 1))as lastActivity
  FROM post_cathegory as pc
  WHERE is_deleted = 0`;
  const sqlPosts = `
  SELECT p.id as postId, p.title, p.date_updated as lastUpdated, u.name as author FROM posts as p
  JOIN users AS u ON u.id=p.users_id
  WHERE p.post_cathegory_id = ?
  ORDER BY p.date_updated DESC
  LIMIT ?;`;

  try {
    const categoryArray = await pool.query(sqlCategory);

    const result = await Promise.all(categoryArray.map(async (element) => {
      const tempVar = await pool.query(sqlPosts,[element.categoryId, +count]);
      if (tempVar.length) {
        element.posts = tempVar;
      } else {
        element.posts = [];
      }
      return element;
    }));

    return result;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};


/**
 * Get post by id.
 * @param {number} id 
 * @returns 
 */
export const getPostsByID = async (id) => {
  // const sqlCategory = `SELECT id as categoryId, name as category FROM post_cathegory`;
  const sqlPosts = `
  SELECT p.id as postId, p.title as postTitle, p.text as postText,
  p.date_created as createdOn, p.date_updated as lastUpdateOn,
  c.id as categoryId, c.name as categoryName, u.id as authorId, u.name as authorName
  FROM posts as p
  JOIN post_cathegory as c ON p.post_cathegory_id=c.id
  JOIN users as u ON p.users_id=u.id
  WHERE p.id = ? AND p.is_deleted = 0`;

  try {
      const result = await pool.query(sqlPosts,[+id]);
      return result;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};


/**
 * Get post author ID
 * @param {number} postId 
 * @returns 
 */
export const getAuthorId = async (postId) => {
  const sqlQuery = `SELECT p.users_id FROM posts as p WHERE id = ?`;
  const result = await pool.query(sqlQuery,[postId]);
  return result;
};


/**
 * Check post exist
 * @param {number} postId 
 * @returns 
 */
export const checkPostExistById = async (postId) => {
  const sqlQuery = `SELECT * FROM posts WHERE id = ? AND is_deleted = 0`;
  const result = await pool.query(sqlQuery, [postId]);
  if (result.length) {
    return true;
  };
  return false;
};


/**
 * Delete post by id
 * @param {number} postId 
 * @returns 
 */
export const deletePostById = async (postId) => {
  const sqlQuery = `UPDATE posts SET is_deleted = 1 WHERE id = ?`;
  
  try {
    const result = await getPostsByID(postId);
    await pool.query(sqlQuery, [postId]);
    return result;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  };

};


/**
 * Flag post by id
 * @param {number} postId 
 * @returns 
 */
export const flagPostById = async (postId) => {
  const sqlQuery = `UPDATE flag_posts SET is_flagged = 1 WHERE id = ?`;
  
  try {
    const result = await getPostsByID(postId);
    const deletedPost = await pool.query(sqlQuery, [postId]);
    return result;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  };
};


/**
 * Lock post by id
 * @param {object} data 
 * @returns 
 */
export const lockPostById = async (data) => {
  const sqlQuery = `INSERT INTO locked_post(reason, posts_id, users_id) VALUES(?, ?, ?)`;

  try {
    await pool.query(sqlQuery, [data.reason, data.postId, data.userId]);
    const result = await getPostsByID(data.postId);
    return result;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  };
};

/**
 * Unlock post by ID
 * @param {object} data 
 * @returns 
 */
export const unlockPostById = async (data) => {
  const sqlQueryDelete = `DELETE FROM locked_post WHERE posts_id = ?`;

  try {
    await pool.query(sqlQueryDelete, [+data.postId]);
    const result = await getPostsByID(data.postId);
    return result;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }

};

/**
 * Check likes count for post by id
 * @param { number } postId 
 * @returns { Promise <number> }
 */
export const postLikesCount = async (postId) => {
  const sqlQuery = `SELECT COUNT(*) as count FROM post_likes where posts_id = ?`;

  try {
    const result = (await pool.query(sqlQuery, [postId]))[0];
    return result.count;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};


/**
 * 
 * @param { number } postId 
 * @returns { Promise <Array> }
 */
export const whoLikeThisPost = async (postId) => {
  const sqlQuery = `
  SELECT u.id as userId, u.name as userName
  FROM post_likes as pl
  JOIN users as u ON pl.users_id = u.id
  WHERE pl.posts_id = ?
  LIMIT 10`;

  try {
    const result = await pool.query(sqlQuery, [postId]);
    return result;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};


/**
 * Check likes count for post by id
 * @param { number } postId 
 * @returns { Promise <number> }
 */
export const postCommentsCount = async (postId) => {
  const sqlQuery = `SELECT COUNT(*) as count FROM comments WHERE posts_id = ? AND is_deleted=0`;

  try {
    const result = (await pool.query(sqlQuery, [postId]))[0];
    return result.count;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};


/**
 * Add like to post
 * @param { number } postId 
 * @param { number } userId 
 */
export const likePost = async (postId, userId) => {
  const sqlQuery = `INSERT INTO post_likes(posts_id, users_id) VALUES (?, ?)`;

  try {
    await pool.query(sqlQuery, [postId, userId]);
    return await getPostsByID(postId);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};

/**
 * Remove like from post
 * @param { number } postId 
 * @param { number } userId 
 */
export const unLikePost = async (postId, userId) => {
  const sqlQuery = `DELETE FROM post_likes WHERE posts_id = ? AND users_id = ?`;

  try {
    const result = await getPostsByID(postId);
    await pool.query(sqlQuery, [postId, userId]);
    return result;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};


/**
 * Return all post from group by group ID
 * @param { number } groupId 
 * @param { number } start 
 * @param { number } count 
 */
export const postsFromCategory = async (userId, groupId, start, count) => {
  const sqlQuery = `
  SELECT
  p.id as postId, p.title as postTitle, p.date_updated as lastUpdated, p.users_id as authorId,
  u.name as authorName, pc.id as categoryId, pc.name as categoryName,
  (SELECT COUNT(*) FROM post_likes WHERE posts_id = p.id) as likes,
  (SELECT COUNT(*) FROM comments WHERE posts_id = p.id AND is_deleted = 0) as comments,
  CASE WHEN p.users_id = ? THEN true ELSE false END AS postIsMine
  FROM posts as p
  JOIN users as u ON p.users_id = u.id
  JOIN post_cathegory as pc ON p.post_cathegory_id = pc.id
  WHERE p.post_cathegory_id = ? AND p.is_deleted = 0
  ORDER BY p.date_updated DESC
  LIMIT ?, ?`;

  try {
    return await pool.query(sqlQuery, [userId, groupId, start, count]);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};


export const updatePost = async (postId, bodyParams) => {
  const currentTime = new Date();
  const sqlQuery = `UPDATE posts SET title = ?, text = ?, date_updated = ? WHERE id = ?`;

  try {
    await pool.query(sqlQuery, [bodyParams.title, bodyParams.text, currentTime, postId]);
    return await getPostsByID(postId);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};