import mariadb from 'mariadb';
import dotenv from 'dotenv';

const constants = dotenv.config().parsed;

/**
 * Creates a pool, with the necessary settings and a connection limit of 20
 */
export const pool = mariadb.createPool({
    host: constants.HOST,
    port: +constants.DBPORT,
    user: constants.USER,
    password: constants.PASSWORD,
    database: constants.DATABASE,
    connectionLimit: 20,
});
