import { logServer } from "../common/help-functions.js";
import { pool } from "./pool.js";

/**
 * Check this post have comment with this id
 * @param { number } postId 
 * @param { number } commentId 
 */
export const checkPostIdAndCommentId = async (postId, commentId) => {
  const sqlQuery = `SELECT * FROM comments WHERE posts_id = ? AND id = ? AND is_deleted = 0`;
  const result = await pool.query(sqlQuery, [postId, commentId]);
  return result.length ? true : false;
};

/**
 * Check current user is a owner of this comment
 * @async
 * @param { number } userId 
 * @param { number } commentId
 * @returns { Promise <boolean> }
 */
export const checkCommentIsMine = async (userId, commentId) => {
  const sqlQuery = `SELECT * FROM comments WHERE users_id = ? AND id = ?`;
  const result = await pool.query(sqlQuery, [userId, commentId]);
  return result.length ? true : false;
};


/**
 * Check current user is like this comment
 * @param { number } userId 
 * @param { number } commentId
 * @returns { boolean }
 */
export const checkLikeThisComment = async (userId, commentId) => {
  const sqlQuery = `SELECT * FROM comments_likes WHERE users_id = ? AND comments_id = ?`;
  const result = await pool.query(sqlQuery, [userId, commentId]);
  return result.length ? true : false;
};


/**
 * Check current user is flag this comment
 * @param { number } userId 
 * @param { number } commentId
 * @returns { boolean }
 */
export const checkFlagThisComment = async (userId, commentId) => {
  const sqlQuery = `SELECT * FROM flag_comments WHERE users_id = ? AND comments_id = ?`;
  const result = await pool.query(sqlQuery, [userId, commentId]);
  return result.length ? true : false;
};


/**
 * Return full information for comment by ID
 * @param { number } id Comment ID
 * @returns 
 */
export const getCommentById = async (postId, commentId, likes) => {
  const sqlQuery = `
  SELECT
    c.id as commentId, c.text as commentText, c.date_created as createdOn,
    c.date_updated as lastUpdatedOn,
    u.id as authorId, u.name as author,
    p.id as postId, p.title as postTitle,
    COALESCE((SELECT count(*) FROM comments_likes WHERE
      comments_id=c.id GROUP BY comments_id), 0) as likes
  FROM comments as c
  JOIN users as u ON u.id = c.users_id
  JOIN posts as p ON p.id = c.posts_id
  WHERE p.id = ? AND c.id = ? AND c.is_deleted = 0`;

  try {
    const result = await pool.query(sqlQuery,[postId, commentId]);
    // result[0].likedBy = await getUsersLikedCommentById(commentId, likes);
    return result;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  };
};


/**
 * Return all users who like this comment
 * @param { number } id
 * @returns { Array }
 */
export const getUsersLikedCommentById = async (id, limit = 10) => {
  const sqlQuery =`
  SELECT u.id as userId, u.name as username
  FROM comments_likes as cl
  JOIN users as u ON cl.users_id=u.id
  WHERE cl.comments_id = ? ORDER BY cl.date LIMIT ? `;
  
  return await pool.query(sqlQuery, [id, +limit]);
};


/**
 * Create new comment for post with id
 * @param { object } data Include comment text, post ID and author ID
 * @returns { JSON }
 */
export const addCommentToPostById = async (data) => {
  const sqlQuery = `INSERT INTO comments(text, posts_id, users_id) VALUES(?, ?, ?)`;

  try {
    const newComment = await pool.query(sqlQuery,[data.text, data.postId, data.userId]);    
    return await getCommentById(data.postId, newComment.insertId, 0);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  };
};

/**
 * Update comment by id
 * @param {*} data 
 * @returns 
 */
export const updateCommentById = async (data) => {
  const updateTime = new Date();
  const sqlQuery = `UPDATE comments SET text = ?, date_updated = ? WHERE id = ?`;
  try {
    await pool.query(sqlQuery,[data.body.text, updateTime, data.params.id]);
    return await getCommentById(data.firstParams.postId, data.params.id, 10);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  };  
};

/**
 * Return all comments ID's for post with ID
 * @async
 * @param { number } postId 
 * @param { number } start 
 * @param { number } count
 */
export const getAllCommentsIdsForPost = async(postId, start, count) => {
  const sqlQuery = `SELECT id FROM comments WHERE posts_id = ? AND is_deleted = 0 LIMIT ?, ?`;

  try {
    return await pool.query(sqlQuery,[postId, +start, +count]);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }; 
};

/**
 * @async
 * @param { number } postId 
 * @param { number } commentId 
 * @param { number } userId 
 */
export const deleteCommentById = async (postId, commentId) => {
  const sqlQuery = `UPDATE comments SET is_deleted = 1 WHERE id = ?`;

  try {
    const result = await getCommentById(postId, commentId, 10);
    await pool.query(sqlQuery, [commentId]);
    return result;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  };
};

/**
 * @async
 * @param { number } postId 
 * @param { number } commentId 
 * @param { number } userId 
 * @returns 
 */
export const likeComment = async (postId, commentId, userId) => {
  const sqlQuery = `INSERT INTO comments_likes(comments_id, users_id) VALUES (?, ?)`;

  try {
    await pool.query(sqlQuery, [commentId, userId]);
    return await getCommentById(postId, commentId);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};

/**
 * @async
 * @param { number } postId 
 * @param { number } commentId 
 * @param { number } userId 
 * @returns 
 */
export const unLikeComment = async (postId, commentId, userId) => {
  const sqlQuery = `DELETE FROM comments_likes WHERE comments_id =? AND users_id = ?`;
  
  try {
    await pool.query(sqlQuery, [commentId, userId]);    
    return await getCommentById(postId, commentId);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};


/**
 * Return all users who flag this comment
 * @param { number } commentId 
 */
export const flagUserList = async (commentId) => {
  const sqlQuery = `
  SELECT
  u.id as userId, u.name as userName
  FROM flag_comments as fc
  JOIN users as u ON fc.users_id = u.id
  WHERE fc.comments_id = ? AND approved = 0`;

  try {
    return await pool.query(sqlQuery, [commentId]);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};


/**
 * Check this comment is already approved from admin
 * @async
 * @param { number } commentId
 * @returns { Promise <boolean> }
 */
export const commentIsApproved = async (commentId) => {
  const sqlQuery = `SELECT * FROM flag_comments WHERE comments_id = ? AND approved = 1 LIMIT 1`;
  
  const checkComment = await pool.query(sqlQuery, [commentId]);
  
  return checkComment.length ? true: false;

};

/**
 * @async
 * @param { number } postId 
 * @param { number } commentId 
 * @param { number } userId 
 * @returns 
 */
export const flagComment = async (postId, commentId, userId) => {
  const sqlQuery = `INSERT INTO flag_comments(comments_id, users_id) VALUES (?, ?)`;
  
  try {
    await pool.query(sqlQuery, [commentId, userId]);
    return await getCommentById(postId, commentId);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};

/**
 * Remove flag from comment
 * @param { number } postId 
 * @param { number } commentId 
 * @param { number } userId 
 * @returns 
 */
export const unFlagComment = async (postId, commentId, userId) => {
  const sqlQuery = `DELETE FROM flag_comments WHERE comments_id =? AND users_id = ?`;
  
  try {
    const result = await getCommentById(postId, commentId);
    await pool.query(sqlQuery, [commentId, userId]);
    return result;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};

/**
 * Approve comment by ID and set checked for admin
 * @param { number } postId 
 * @param { number } commentId 
 */
export const approveComment = async (postId, commentId) => {
  const sqlQuery = `UPDATE flag_comments SET checked_admin = 1, approved = 1 WHERE comments_id = ?`;

  try {
    await pool.query(sqlQuery, [commentId]);
    return await getCommentById(postId, commentId);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};
