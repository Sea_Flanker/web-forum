import { pool } from './pool.js';
import { logServer } from '../common/help-functions.js';
import bcrypt from 'bcrypt';

/**
 * Checks if a username exists
 * @param {string} username 
 * @returns 
 */
export const hasThisUser = async (username) => 
await getUserByUsername(username) ? true : false;


/**
 * Check user exists by ID
 * @param { number } userId 
 * @returns 
 */
export const userExistsById = async (userId) => {
  const sqlQuery = `SELECT * FROM users WHERE id = ? AND is_deleted = 0`;

  try {
    const result =  await pool.query(sqlQuery, [userId]);
    return result.length ? true : false;
  } catch (err) {
  logServer.yellow(err.message);
  return null;
}
};


/**
 * Gets a user by username
 * @param {string} username 
 * @returns 
 */
export const getUserByUsername = async (username) => {
  const sqlQuery = `SELECT * FROM users WHERE name = ? AND is_deleted = 0`;
  const result = await pool.query(sqlQuery, [username]);
  if (result[0]) {
    return result[0];
  };

  return null;
};

/**
 * Add new user to database and return created user info
 * @param { object } user 
 * @param { number } group 
 * @returns 
 */
export const createUser = async (user, group) => {
  const hashPassword = await bcrypt.hash(user.password, 10);
  const sql = `INSERT INTO users(name, password, groups_id) VALUES (?, ?, ?)`;
  const newUser = await pool.query(sql, [user.username, hashPassword, group]);

  const sqlQuery = `
    SELECT users.id as id,users.name as username,
    users.registration_date as registrationDate, g.name as usersGroup
    FROM users
    JOIN groups as g ON g.id=users.groups_id
    WHERE users.id = ?`;

  const result = await pool.query(sqlQuery, [newUser.insertId]);
  return result;
};


/**
 * Check banned list
 * @param {string} user 
 * @returns 
 */
export const checkUserForBan = async(user) => {
  const sqlQuery = `
  SELECT u.name as username, su.name as baned_from, b.reason, b.date FROM users as u
  JOIN banned_users as b ON u.id = b.banned_user_id
  JOIN users as su ON su.id = b.banned_from_id
  WHERE u.name = ?`;

  const result = await pool.query(sqlQuery, [user]);

  return result;
};


/**
 * Check for e-mail duplicate
 * @param {number} id 
 * @param {string} email 
 * @returns 
 */
export const checkEmailDuplicate = async(id, email) => {
  const sqlQuery = `SELECT e_mail FROM users WHERE e_mail = ? AND id <> ?`;
  const result = await pool.query(sqlQuery, [email, id]);
  if (result.length) {
    return true;
  };
  return false;
};

/**
 * Update user information
 * @param {object} userInfo 
 * @param {object} newData 
 * @returns 
 */
export const updateUserInfo = async (userInfo, newData) => {
  const sqlQuery = `UPDATE users SET first_name = ?, last_name = ?, e_mail = ? WHERE id = ?`;
  try {
    const updatedUser = await pool
      .query(sqlQuery, [newData.firstName, newData.lastName, newData.email, userInfo.id]);
    const result = await getFullUserInfoById(userInfo.id);
    if (result[0]) {
      return result[0];
    }
    return null;
  } catch (err) {
    console.log (err.message);
    return false;
  };
};

/**
 * Gets a users entire info, by id
 * @param {number} id 
 * @returns 
 */
export const getFullUserInfoById = async (id) => {
  const sqlQuery = `
  SELECT u.id as userId, u.name as username, u.first_name as firstName, u.last_name as lastName,
  u.e_mail as email, u.registration_date as memberFrom, g.name as userGroup,
  (SELECT COUNT(*) FROM comments where users_id = u.id AND is_deleted = 0) as commentsCount,
  (SELECT COUNT(*) FROM posts where users_id = u.id AND is_deleted = 0) as postsCount,
  (SELECT COUNT(*) FROM post_likes where users_id = u.id) +
  (SELECT COUNT(*) FROM comments_likes where users_id = u.id) as totalLikes,
  (SELECT COUNT(*) FROM banned_users WHERE banned_user_id = u.id) as bannedStatus,
  (SELECT COUNT(*) FROM friends as f JOIN users as us ON f.receiver = us.id
    WHERE f.sender = u.id AND us.is_deleted = 0) as friendWith,
  (SELECT COUNT(*) FROM friends as f JOIN users as us ON f.sender = us.id
    WHERE f.receiver = u.id AND us.is_deleted = 0) as friendTo
  FROM users as u
  JOIN groups as g on g.id = u.groups_id
  WHERE u.id = ? AND is_deleted = 0`;
  return await pool.query(sqlQuery, [id]);
};

/**
 * Search in users
 * @param {string} keyWord 
 * @returns 
 */
export const searchInUsers = async (keyWord) => {
  const sqlQuery = `
  SELECT u.id as userId, u.name as username, u.first_name as firstName, u.last_name as lastName,
  u.e_mail as email, u.registration_date as memberFrom, g.name as userGroup,
  (SELECT COUNT(*) FROM comments where users_id = u.id) as commentsCount,
  (SELECT COUNT(*) FROM posts where users_id = u.id) as postsCount,
  (SELECT COUNT(*) FROM post_likes where users_id = u.id) +
  (SELECT COUNT(*) FROM comments_likes where users_id = u.id) as totalLikes
  FROM users as u
  JOIN groups as g on g.id = u.groups_id
  WHERE (u.name LIKE '%${keyWord}%'
  OR u.first_name LIKE '%${keyWord}%'
  OR u.last_name LIKE '%${keyWord}%')
  AND is_deleted = 0
  `;

  try {
    const result = await pool.query(sqlQuery);
    if (result.length) {
      return result;
    };
    return { message: `nothing found for '${keyWord}'` };
  } catch (err) {
    console.log (err.message);
    return false;
  };
};

/**
 * Get banned users list
 * @param {string} name 
 * @returns 
 */
export const getBanList = async (name) => {
  
  let sqlQuery = `
  SELECT u.id as userId, u.name as username, u.first_name as firstName,
  u.last_name as lastName, u.e_mail as email, b.date as bannedOn, us.name as bannedBy,
  b.reason as bannedReason
  FROM users as u
  JOIN banned_users as b ON u.id = b.banned_user_id
  JOIN users as us ON us.id = b.banned_from_id`;

  if (name) {
    sqlQuery = sqlQuery + ` WHERE u.name = ?`;
  };

  try {
    const result = await pool.query(sqlQuery, [name]);
    return result;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  };
};

/**
 * Create a ban
 * @param {number} user 
 * @param {string} reason 
 * @param {string} admin 
 * @returns 
 */
export const createBan = async (user, reason, admin) => {
  const sqlQuery = `
  INSERT INTO
  banned_users(reason, banned_user_id, banned_from_id)
  VALUES(?, ?, ?)`;

  try {
    const bannedUser = await pool.query(sqlQuery, [reason, user, admin]);
    return await getFullUserInfoById(user);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  };
};

/**
 * Remove a ban
 * @param {number} userId 
 * @returns 
 */
export const deleteBan =  async (userId) => {
  const sqlDelete = `DELETE FROM banned_users WHERE banned_user_id = ?`;

  try {
    const unBanUser = await pool.query(sqlDelete, [userId]);
    return await getFullUserInfoById(userId);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  };
};


/**
 * Delete user by id.
 * @param {number} userId 
 * @returns 
 */
export const deleteUser = async (userId) => {
  try {
    const result = await getFullUserInfoById(userId);
    await pool.query(`UPDATE users SET is_deleted = 1 WHERE id = ?`, [userId]);
    if (result[0]) {
      return result[0];
    }
    return null;
  } catch (err) {
    console.log (err.message);
    return false;
  }; 
};

/**
 * Logout user.
 * @param {object} userInfo 
 * @param {object} newData 
 * @returns 
 */
export const logoutUser = async (userInfo, newData) => {
  const sqlQuery = `UPDATE tokens SET token = 0, WHERE id = ?`;
  try {
    const updatedUser = await pool.query(sqlQuery, [newData.token]);
    const result = await getFullUserInfoById(userInfo.id);
    if (result[0]) {
      return result[0];
    };
    return null;
  } catch (err) {
    console.log (err.message);
    return false;
  };  
};


/**
 * Save used token
 * @param {string} token 
 * @returns 
 */
export const saveOldToken = async (token) => {
  const sqlQuery = `INSERT INTO tokens(token) VALUES (?)`;

  try {
    return await pool.query(sqlQuery, [token]);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  };
};

/**
 * Check if token has been blacklisted
 * @param {string} token 
 * @returns 
 */
export const checkOldTokens = async (token) => {
  // const sqlQuery = `SELECT tokens(token) VALUES (?)`;
  const sqlQuery = `SELECT token FROM tokens WHERE token = ?`;

  try {
    const validToken = await pool.query(sqlQuery, [token]);    
    return validToken.length ? true : false;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  };
};

/**
 * Add friend
 * @param {number} userId 
 * @param {number} friendId 
 * @returns 
 */
export const addNewFriend = async (userId, friendId) => {

  const sqlQuery = `
  INSERT INTO friends(sender, receiver) 
  VALUES(?, ?)`;

  try {
    const addedFriend = await pool.query(sqlQuery, [userId, friendId]);    
    return addedFriend;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  };
};

/**
 * Delete friend
 * @param {number} userId 
 * @param {number} friendId 
 * @returns 
 */
export const deleteSingleFriend = async (userId, friendId) => {

  const sqlQuery = `
  DELETE FROM friends 
  WHERE sender = ? AND receiver = ?`;

  try {
    const removedFriend = await pool.query(sqlQuery, [userId, friendId]);    
    return removedFriend;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  };
};

/**
 * Add activities.
 * @param {number} userId ID of the user who made an action.
 * @param {string} username name of the user
 * @param {string} activity Type of the performed activity.
 * @returns 
 */
export const addNewActivity = async (userId, activity) => {

  const sqlQuery = `
  INSERT INTO activities(userId, activity) 
  VALUES(?, ?)`;

  try {
    const addedActivity = await pool.query(sqlQuery, [userId, activity]);    
    return addedActivity;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  };
};

/**
 * Read activities
 * @param {number} id 
 * @returns 
 */

export const getActivitiesByID = async (id) => {
  // const sqlCategory = `SELECT id as categoryId, name as category FROM post_cathegory`;
  const sqlActivities = `
  SELECT * FROM forumdb.activities WHERE userId = ?`;

  try {
      const result = await pool.query(sqlActivities,[+id]);
      return result;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};

/**
 * Check if friendship exists
 * @param {number} userId 
 * @param {number} friendId 
 * @returns 
 */
export const checkFriendship = async (userId, friendId) => {
  
  const sqlActivities = `
  SELECT * FROM forumdb.friends WHERE user_id = ? AND friend_id = ?`;

  try {
      const result = await pool.query(sqlActivities,[userId, friendId]);
      return result;
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};


/**
 * Return friend for user with Id
 * @param {number} userId 
 */
export const friendsList = async (userId) => {
  const sqlQuery = `
  SELECT u.id as userId, u.name as userName, f.start_date as friendsFrom,
    u.first_name as firstName, u.last_name as lastName, u.e_mail as email
  FROM friends as f
  JOIN users as u ON f.receiver = u.id
  WHERE f.sender = ? AND u.is_deleted = 0
  UNION
  SELECT u.id as userId, u.name as userName, f.start_date as friendsFrom,
    u.first_name as firstName, u.last_name as lastName, u.e_mail as email
  FROM friends as f
  JOIN users as u ON f.sender = u.id
  WHERE f.receiver = ? AND u.is_deleted = 0`;
  
  try {
    return await pool.query(sqlQuery,[userId, userId]);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};

/**
 * Check friendship exists
 * @param { number } myId 
 * @param { number } userId
 * @returns { Promise <boolean> }
 */
export const checkFriendsPair = async (myId, userId) => {
  const sqlQuery = `
    SELECT * FROM friends WHERE sender = ? AND receiver = ?
    UNION
    SELECT * FROM friends WHERE receiver = ? AND sender = ?
    `;
  const result = await pool.query(sqlQuery, [myId, userId, myId, userId]);
  return result.length ? true : false;
};


/**
 * Create new friendship
 * @param { number } myId 
 * @param { number } userId
 * @returns { Promise <number> }
 */
export const createNewFriendship = async (myId, userId) => {
  const sqlQuery = `INSERT INTO friends(sender, receiver) VALUES (?, ?)`;

  try {
    await pool.query(sqlQuery,[myId, userId]);
    return getFullUserInfoById(userId);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};


/**
 * Remove friendship
 * @param { number } myId 
 * @param { number } userId
 * @returns { Promise <number> }
 */
export const removeFriendship = async (myId, userId) => {
  const sqlQuery = `DELETE FROM friends WHERE sender = ? AND receiver = ?`;
  const sqlQueryTwo = `DELETE FROM friends WHERE receiver = ? AND sender = ?`;

  try {
    await pool.query(sqlQuery,[myId, userId]);
    await pool.query(sqlQueryTwo,[myId, userId]);
    return getFullUserInfoById(userId);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};


export const getLastUsers = async () => {
  const sqlQuery = `
  SELECT u.id as userId, u.name as username, u.first_name as firstName, u.last_name as lastName,
  u.e_mail as email, u.registration_date as memberFrom, g.name as userGroup,
  (SELECT COUNT(*) FROM comments where users_id = u.id) as commentsCount,
  (SELECT COUNT(*) FROM posts where users_id = u.id) as postsCount,
  (SELECT COUNT(*) FROM post_likes where users_id = u.id) +
  (SELECT COUNT(*) FROM comments_likes where users_id = u.id) as totalLikes
  FROM users as u
  JOIN groups as g on g.id = u.groups_id
  WHERE is_deleted = 0
  ORDER BY u.registration_date DESC
  LIMIT 10`;

  try {
    return await pool.query(sqlQuery);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};


/**
 * Return all users starts with specific letter
 * @param { string } letter 
 */
export const nameStartWith = async (letter) => {
  const sqlQuery = `
  SELECT u.id as userId, u.name as username, u.first_name as firstName, u.last_name as lastName,
  u.e_mail as email, u.registration_date as memberFrom, g.name as userGroup,
  (SELECT COUNT(*) FROM comments where users_id = u.id) as commentsCount,
  (SELECT COUNT(*) FROM posts where users_id = u.id) as postsCount,
  (SELECT COUNT(*) FROM post_likes where users_id = u.id) +
  (SELECT COUNT(*) FROM comments_likes where users_id = u.id) as totalLikes
  FROM users as u
  JOIN groups as g on g.id = u.groups_id
  WHERE u.name LIKE '${letter}%' AND is_deleted = 0
  `;
  
  try {
    return await pool.query(sqlQuery);
  } catch (err) {
    logServer.yellow(err.message);
    return null;
  }
};
