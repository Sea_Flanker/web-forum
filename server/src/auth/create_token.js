import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

const SECRET_KEY = dotenv.config().parsed.SECRET_KEY;

/**
 * 
 * @param {*} payLoad 
 * @returns 
 */
const createToken = (payLoad) => {
    const token = jwt.sign(payLoad, SECRET_KEY, { expiresIn: '24h' } );

    return token;
};

export default createToken;