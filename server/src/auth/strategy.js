import passportJwt from 'passport-jwt';
import dotenv from 'dotenv';

const SECRET_KEY = dotenv.config().parsed.SECRET_KEY;

const options = {
    secretOrKey: SECRET_KEY,
    jwtFromRequest: passportJwt.ExtractJwt.fromAuthHeaderAsBearerToken(),
};

/**
 * Settings for the passport strategy
 */
const jwtStrategy = new passportJwt.Strategy(options, async (payload, done) => {
    const user = {
        id: payload.id,
        username: payload.username,
        group: payload.group,
        firstName: payload.firstName,
        lastName: payload.lastName,
    };

    done(null, user);
});

export default jwtStrategy;
