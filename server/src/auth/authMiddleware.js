import passport from 'passport';

/**
 * Authentication middleware
* @param {express.Request} req 
* @param {express.Response} res 
* @param {express.NextFunction} next 
 */
const authMiddleware = (req, res, next) => {
    passport.authenticate('jwt', {session: false})(req,res,next);
};

export { authMiddleware };