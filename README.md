### .env file
```text
PORT = 3000
HOST = localhost
DBPORT = 3306
USER = root
PASSWORD = 
DATABASE = forumdb
SECRET_KEY = Mladenov_Bochev
LOG_DIR = ./logs
```
<br/>

### Install database
```JSON
npm run seed
```
<br/>

### api/users/

#### register
###### METHOD: POST
Регистрация на нов потребител
Заявката приема два два аргумента от __body__ под формата на __JSON__
_username_ - потребителското име с дължина поне 4 символа състоящо се само от малки букви
_password_ - парола с дължина поне 6 символа
```JSON
{
    "username": "kalina",
    "password": "123456"
}
```
При успешна регистрация отговорът на сървъра е __JSON__ с данните на новосъздадения потребител.
```JSON
[
    {
        "id": 5,
        "username": "kalina",
        "registrationDate": "2021-07-07T16:54:22.000Z",
        "usersGroup": "user"
    }
]
```
<br/>

#### login
###### METHOD: POST
Аутентикация на потребителя в системата
Заявката приема два два аргумента от __body__ под формата на __JSON__
_username_ - потребителското име с дължина поне 4 символа състоящо се само от малки букви
_password_ - парола
```JSON
{
    "username": "fidana",
    "password": "123456"
}
```
При успешна аутентикация върнатия от сървъра отговор е __JSON WEB TOKEN__
<br/>

#### /search?name=_string_
###### METHOD: GET
Търсене на потребители по зададе шаблон. Търсенето става в потребителските имена, собствените и фамилните. Шаблонът за търсене се задава като Query parameter
Пример: __fidana.net:3000/api/users/search?name=ka__
При успешно обработена заявка върнатия от сървъра отговор е под формата на __JSON__
```JSON
[
    {
        "username": "penka",
        "firstName": "Пенка",
        "lastName": "Пенкова",
        "email": "penka@fidana.net",
        "memberFrom": "2021-07-02T08:00:57.000Z"
    },
    {
        "username": "veska",
        "firstName": "Веска",
        "lastName": "Ламерова",
        "email": "veska@fidana.net",
        "memberFrom": "2021-07-02T08:01:32.000Z"
    }
]
```




#### update/:usedID
###### METHOD: PUT
###### _only for authenticated users, user with right username and admin_
Обновява личните данни на потребителя /не се попълват при регистрация/
Заявката приема 3 аргумента:
_firstName_ - собственото име на потребителя
_lastName_ - фамилното име на потребителя
_email_ - адрес за електронна поща /валиден/

```JSON
{
    "firstName": "Златна",
    "lastName": "Пампорова",
    "email": "zlatna@fidana.net"
}
```
При успешно обработена заявка върнатия от сървъра отговор е __JSON__
```JSON
{
    "username": "zlatna",
    "firstName": "Златна",
    "lastName": "Пампорова",
    "email": "zlatna@fidana.net",
    "memberFrom": "2021-07-02T12:52:37.000Z",
    "userGroup": "user"
}
```