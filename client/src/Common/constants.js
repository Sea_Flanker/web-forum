export const API_URL = 'http://localhost:3000/api/';

export const HEADERS = {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${localStorage.getItem('token')}`,
}


export const getHeaders = () => {
    return {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${localStorage.getItem('token')}`,
    }
}


export const PATHS = {
    home: { link: '/', name: 'home'},
    category: { link: '/category', name: 'categories' },
}

export const USERS_GROUPS = {
    admin: 1,
    user: 2,
}