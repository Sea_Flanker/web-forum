import decodeToken from 'jwt-decode';


export const getUserDataFromToken = (token) => {
  try {
    const userData = decodeToken(token);
    return userData;
  } catch (err) {

  }
  return null;
};