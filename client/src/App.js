import './App.css';
import CategoryView from './Components/CategoryView/CategoryView';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Header from './Components/Header/Header';
import MainContainer from './Components/MainContainer/MainContainer';
import Footer from './Components/Footer/Footer';
import CategoryPosts from './Components/CategoryPosts/CategoryPosts';
import PostView from './Components/PostView/PostView';
import Home from './Components/Home/Home';
import AuthContext from './Context/AuthContext';
import { useState } from 'react';
import { getUserDataFromToken } from './Common/token_functions';
import PathButtonContext from './Context/PathButtonContext';
import NavigationBar from './Components/NavigationBar/NavigationBar';
import PathBar from './Components/PathBar/PathBar';
import { PathContext } from './Context/PathContext';
import UserDetails from './Components/UserDetails/UserDetails';
import BanList from './Components/BanList/BanList';
import Users from './Components/Users/Users';
import FriendsList from './Components/FriendsList/FriendsList';


function App() {
  const [user, setUser] = useState(getUserDataFromToken(localStorage.getItem('token')));
  const [pathButton, setPathButton] = useState({ enabled: false, value: 'none', actionFunc: () => null });
  const [currentPath, updateCurrentPath] = useState({ path: [], updatePath: () => null });
  
  return (
    
      <MainContainer>
        <BrowserRouter>
          <AuthContext.Provider value={{user: user, setUser: setUser}}>
            <PathButtonContext.Provider value={{ options: pathButton, updateFunc: setPathButton }}>
              <PathContext.Provider value={{path: currentPath, updatePath: updateCurrentPath}}>
                <Header />
                { user
                    ? <><NavigationBar /><PathBar /></>
                    : null
                }
                <Switch>
                  <Route exact path='/' component={ Home } />
                  <Route exact path='/category' component={ CategoryView } />
                  <Route exact path='/category/:id' component={ CategoryPosts } />
                  <Route exact path='/posts/:id' component={ PostView } />
                  <Route exact path='/users' component={ Users } />
                  <Route exact path='/users/:id' component={ UserDetails } />
                  <Route exact path='/banList' component={ BanList } />
                  <Route exact path='/friends' component={ FriendsList } />
                </Switch>
              </PathContext.Provider>
            </PathButtonContext.Provider>
          </AuthContext.Provider>
        </BrowserRouter>
        <Footer/>
      </MainContainer>
    
  );
}

export default App;
