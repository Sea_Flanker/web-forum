import { API_URL, getHeaders } from "../Common/constants.js";

export const loginToServer = (username, password) => {
  return fetch(`${API_URL}users/login`, {
    method: 'POST',
    headers: getHeaders(),
    body: JSON.stringify({username, password})
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
}


export const logOutFromServer = () => {
  return fetch(`${API_URL}users/logout`, {
    method: 'DELETE',
    headers: getHeaders(),
  })
  .then(x => x.json())
  .catch(() => ({ message: 'Server have problem' }));
}


export const registerToServer = (username, password) => {
  return fetch(`${API_URL}users/register`, {
    method: 'POST',
    headers: getHeaders(),
    body: JSON.stringify({username, password})
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
}


export const getUserInfoById = (userId) => {
  return fetch(`${API_URL}users/info/${userId}`, {
    headers: getHeaders(),
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
};


export const getUserFriends = (userId) => {
  return fetch(`${API_URL}users/friends`, {
    headers: getHeaders(),
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
};


export const removeFriendFromList = (id) => {
  return fetch(`${API_URL}users/friends/${id}`, {
    method: 'DELETE',
    headers: getHeaders(),
  })
  .then(x => x.json())
  .catch(() => ({ message: 'Server have problem' }));
}


export const addFriendToList = (id) => {
  return fetch(`${API_URL}users/friends/${id}`, {
    method: 'POST',
    headers: getHeaders(),
  })
  .then(x => x.json())
  .catch(() => ({ message: 'Server have problem' }));
}

export const removeFromBanList = (username) => {
  return fetch(`${API_URL}users/bans/remove`, {
    method: 'DELETE',
    headers: getHeaders(),
    body: JSON.stringify({ username })
  })
  .then(x => x.json())
  .catch(() => ({ message: 'Server have problem' }));
}


export const addToBanList = (username, reason) => {
  return fetch(`${API_URL}users/bans/add`, {
    method: 'POST',
    headers: getHeaders(),
    body: JSON.stringify({ username, reason })
  })
  .then(x => x.json())
  .catch(() => ({ message: 'Server have problem' }));
}


export const deleteUserById = (userId) => {
  return fetch(`${API_URL}users/${userId}`, {
    method: 'DELETE',
    headers: getHeaders(),
  })
  .then(x => x.json())
  .catch(() => ({ message: 'Server have problem' }));
}


export const updateUserInfoById = (userId, body) => {
  return fetch(`${API_URL}users/update/${userId}`, {
    method: 'PUT',
    headers: getHeaders(),
    body: JSON.stringify(body),
  })
  .then(x => x.json())
  .catch(() => ({ message: 'Server have problem' }));
}


export const getBanListFromServer = () => {
  return fetch(`${API_URL}users/bans/list`, {
    method: 'GET',
    headers: getHeaders(),
  })
  .then(x => x.json())
  .catch(() => ({ message: 'Server have problem' }));
}


export const getLastTen = () => {
  return fetch(`${API_URL}users/last`, {
    method: 'GET',
    headers: getHeaders(),
  })
  .then(x => x.json())
  .catch(() => ({ message: 'Server have problem' }));
}


export const searchInUsers = (string) => {
  return fetch(`${API_URL}users/search?name=${string}`, {
    method: 'GET',
    headers: getHeaders(),
  })
  .then(x => x.json())
  .catch(() => ({ message: 'Server have problem' }));
}


export const getUsersByFirstLetter = (letter) => {
  return fetch(`${API_URL}users/names/${letter}`, {
    method: 'GET',
    headers: getHeaders(),
  })
  .then(x => x.json())
  .catch(() => ({ message: 'Server have problem' }));
}