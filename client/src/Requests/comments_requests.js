import { API_URL, getHeaders } from "../Common/constants.js";

export const getPostComments = (postId, start = 0, count = 4) => {
  return fetch(`${API_URL}posts/${postId}/comments?start=${start}&count=${count}`, {
    headers: getHeaders(),
  })
  .then(x => x.json())
.catch(err => ({ message: 'Server have problem' }));
};

export const likeCommentById = (postId, commentId) => {
  return fetch(`${API_URL}posts/${postId}/comments/like/${commentId}`, {
    method: 'POST',
    headers: getHeaders(),
  })
  .then(x => x.json())
.catch(err => ({ message: 'Server have problem' }));
};

export const unLikeCommentById = (postId, commentId) => {
  return fetch(`${API_URL}posts/${postId}/comments/unlike/${commentId}`, {
    method: 'DELETE',
    headers: getHeaders(),
  })
  .then(x => x.json())
.catch(err => ({ message: 'Server have problem' }));
};

export const updateCommentById = (postId, commentId, bodyData) => {
  return fetch(`${API_URL}posts/${postId}/comments/update/${commentId}`, {
    method: 'PUT',
    headers: getHeaders(),
    body: JSON.stringify(bodyData),
  })
  .then(x => x.json())
.catch(err => ({ message: 'Server have problem' }));
};


export const deleteCommentById = (postId, commentId) => {
  return fetch(`${API_URL}posts/${postId}/comments/delete/${commentId}`, {
    method: 'DELETE',
    headers: getHeaders(),
  })
  .then(x => x.json())
.catch(err => ({ message: 'Server have problem' }));
};


export const createNewComment = (postId, bodyData) => {
  return fetch(`${API_URL}posts/${postId}/comments/create`, {
    method: 'POST',
    headers: getHeaders(),
    body: JSON.stringify(bodyData),
  })
  .then(x => x.json())
.catch(err => ({ message: 'Server have problem' }));
};