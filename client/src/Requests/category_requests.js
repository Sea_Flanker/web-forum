import { API_URL, getHeaders } from "../Common/constants.js";


export const showAllCategories = () => {
  return fetch(`${API_URL}posts/all?count=0`, {
    headers: getHeaders(),
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
};


export const getAllPostsFromCategory = (categoryId) => {
  return fetch(`${API_URL}posts/category/${categoryId}?start=0&count=100`, {
    headers: getHeaders(),
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
};

