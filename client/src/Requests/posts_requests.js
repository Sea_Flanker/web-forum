import { API_URL, getHeaders } from "../Common/constants.js";

export const getPostById = (postId) => {
  return fetch(`${API_URL}posts/${postId}`, {
    headers: getHeaders()
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
};


export const likePostById = (postId) => {
  return fetch(`${API_URL}posts/like/${postId}`, {
    method: 'POST',
    headers: getHeaders()
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
};


export const unLikePostById = (postId) => {
  return fetch(`${API_URL}posts/unlike/${postId}`, {
    method: 'DELETE',
    headers: getHeaders()
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
};


export const lockPostById = (postId, reason) => {
  return fetch(`${API_URL}posts/lock/${postId}`, {
    method: 'POST',
    headers: getHeaders(),
    body: JSON.stringify({ reason }),
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
};


export const unLockPostById = (postId) => {
  return fetch(`${API_URL}posts/unlock/${postId}`, {
    method: 'DELETE',
    headers: getHeaders(),
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
};


export const createPost = (postBody) => {
  return fetch(`${API_URL}posts/create/`, {
    method: 'POST',
    headers: getHeaders(),
    body: JSON.stringify(postBody),
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
};


/**
 * 
 * @param { number } postId 
 * @param { object } postBody
 * @returns 
 */
export const updatePostById = (postId, postBody) => {
  return fetch(`${API_URL}posts/${postId}`, {
    method: 'PUT',
    headers: getHeaders(),
    body: JSON.stringify(postBody),
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
};


export const deletePostById = (postId) => {
  return fetch(`${API_URL}posts/${postId}`, {
    method: 'DELETE',
    headers: getHeaders(),
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
};


export const updateGroupById = (groupId, category) => {
  return fetch(`${API_URL}posts/category/${groupId}`, {
    method: 'PUT',
    headers: getHeaders(),
    body: JSON.stringify({category}),
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
};

export const deleteGroupById = (groupId) => {
  return fetch(`${API_URL}posts/category/${groupId}`, {
    method: 'DELETE',
    headers: getHeaders(),
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
};


export const createNewPostCategory = (category) => {
  return fetch(`${API_URL}posts//category/create`, {
    method: 'POST',
    headers: getHeaders(),
    body: JSON.stringify({category}),
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
};


export const movePostToOtherGroup = (postId, categoryId) => {
  return fetch(`${API_URL}posts/move`, {
    method: 'PUT',
    headers: getHeaders(),
    body: JSON.stringify({postId, categoryId}),
  })
  .then(x => x.json())
  .catch(err => ({ message: 'Server have problem' }));
};
