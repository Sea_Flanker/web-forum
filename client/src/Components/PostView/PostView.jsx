import React from 'react';
import { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { getPostComments } from '../../Requests/comments_requests.js';
import { getPostById, likePostById, unLikePostById, unLockPostById, updatePostById, deletePostById } from '../../Requests/posts_requests.js';
import CommentView from '../CommentView/CommentView.jsx';
import ErrorBox from '../ErrorBox/ErrorBox.jsx';
import LockingDialog from '../LockingDialog/LockingDialog.jsx';
import Preloader from '../Preloader/Preloader.jsx';
import UserBusinessCard from '../UserBusinessCard/UserBusinessCard.jsx';
import ConfirmDialog from '../ConfirmDialog/ConfirmDialog.jsx';
import NewCommentDialog from '../NewCommentDialog/NewCommentDialog.jsx';
import { useContext } from 'react';
import PathButtonContext from '../../Context/PathButtonContext.js';
import { PathContext } from '../../Context/PathContext.js';
import { PATHS, USERS_GROUPS } from '../../Common/constants.js';
import AuthContext from '../../Context/AuthContext.js';
import MovePostDialog from '../MovePostDialog/MovePostDialog.jsx';
import './PostView.css'

const PostView = (props) => {
  const postId = props.match.params.id;
  let history = useHistory();

  const [callNext, updateCallNext] = useState(false);
  const [start, updateStart] = useState(4);
  const [post, updatePost] = useState(null);
  const [comments, updateComments] = useState([]);
  const [lockDialog, updateLockDialog] = useState(false);
  const [editMode, setEditMode] = useState(false);
  const [newText, updateNewText] = useState(null);
  const [conDialog, updateConDialog] = useState({visible: 'none', message: '', okFunc: ()=> 1});
  const [newCommentShow, updateNewCommentShow] = useState(false ? 'block' : 'none');
  const [movePost, updateMovePost] = useState(false);

  

  const likePost = () => likePostById(postId).then(result => updatePost(result));
  const unLikePost = () => unLikePostById(postId).then(result => updatePost(result));

  const { user } = useContext(AuthContext);
  const { updateFunc } = useContext(PathButtonContext);
  const { updatePath } = useContext(PathContext);

  useEffect(() => {
    if (post) {
      updatePath(
        [ PATHS.home,
          PATHS.category,
          { link: `/category/${post.categoryId}`, name: `${post.categoryName}` },
          { link: `/posts/${post.postId}`, name: `${post.postTitle}`}
      ]);
    }
  }, [updatePath, post]);

  useEffect(() => {
    const checkLock = post ? !post.locked : true
    updateFunc({ enabled: checkLock, value: 'ADD COMMENT', actionFunc: () => updateNewCommentShow(!newCommentShow) });
  }, [updateFunc, updateNewCommentShow, newCommentShow, post]);

  useEffect(() => {
    getPostById(postId).then(result => updatePost(result));
  }, [postId]);


  useEffect(() => {    
      getPostComments(postId).then(result => updateComments(result));
  }, [postId]);


  useEffect(() => {

    const onScroll = () => {
      if ((window.innerHeight + window.scrollY) >= document.body.offsetHeight && !callNext) {
        updateCallNext(true);
        if (start < post.comments) {
          updateStart(comments.length + 4);
          getPostComments(post.postId, start).then(result => {
            updateComments([...comments, ...result]);
            updateCallNext(false);
          });
        }
      }
    }

    window.addEventListener('scroll', onScroll);
    
    return () => window.removeEventListener('scroll', onScroll);
  }, [post, callNext, comments, start]);
  

  const changeComment = (comment, action = 'update') => {
    if (action === 'update') {
      const newArray = comments.map(x => {
        if (x.commentId === comment.commentId) {
          x = comment;
        }
          return x;
      });
      updateComments(newArray);
    } else if (action === 'remove') {
        getPostComments(postId).then(result => updateComments(result));
        getPostById(postId).then(result => updatePost(result));
        updateStart(4);
        updateCallNext(false);
    } else if (action === 'add') {
      getPostComments(postId).then(result => updateComments(result));
        getPostById(postId).then(result => updatePost(result));
        updateStart(4);
        updateCallNext(false);
    }

  }

  const changePost = (post) => updatePost(post);
  const unLockPost = (postId) => unLockPostById(postId).then(result => updatePost(result));


  const enterEditMode = () => {
    updateNewText(post.postText);
    setEditMode(true);
  }

  const updatePostText = () => {
      setEditMode(false);
      const bodyObject = {
        title: post.postTitle,
        text: newText,
      }
      updatePostById(postId, bodyObject).then(result => updatePost(result));
  }

  const cancelPostEdit = () => updateConDialog({visible: 'block', message: 'ARE YOU SURE? ALL CHANGES WILL BE LOST!', okFunc: confirmFunction});

  const cancelFunction = () => updateConDialog({visible: 'none', message: 'ARE YOU SURE? ALL CHANGES WILL BE LOST!'});

  const confirmFunction = () => {
    updateConDialog({visible: 'none'});
    setEditMode(false);
  }

  const deletePostAction = () => updateConDialog({visible: 'block', message: 'ARE YOU SURE? POST AND ALL COMMENTS WILL BE LOST!', okFunc: confirmDeletePost});

  const confirmDeletePost = () => {
    updateConDialog({visible: 'none'});
    deletePostById(postId)
    .then(result => {
      if (result.message) {
        console.log (result.message);
      } else {
        history.push(`/category/${result.categoryId}?name=${result.categoryName}`);
      }

    });
  }  

  if (post === null) {
    return <Preloader />
  }
  
  if (post.message) {
    return <ErrorBox message={ post.message } nextLocation='/' />
  }

  return (
    <>
    
      { movePost ? <MovePostDialog postId={postId} closeFunc={updateMovePost} categoryId={post.categoryId} updateFunc={updatePost} /> : null}

      <ConfirmDialog visible={conDialog.visible} message={conDialog.message} okFunc={conDialog.okFunc} cancelFunc={cancelFunction}/>

      <NewCommentDialog visible={newCommentShow} postId={postId} addFunc={changeComment}/>

      <LockingDialog enable={lockDialog} showFunc={updateLockDialog} postFunc={changePost} postId={postId} />
      
      <div className='postDivHeader'>
        { 
          post.locked 
            ? post.lockedReason
              ?`${post.postTitle} (locked) - ${post.lockedReason}`
              : `${post.postTitle} (locked)`
            : post.postTitle 
        }
      </div>

      <div className='postDiv'>
        <div>
          <UserBusinessCard userId={post.authorId} />
          <div className='postAdditionalInfo'>
            <div className='postLikesDiv'>Likes: {post.likes}              
              {
                post.whoLike.length
                  ? <div className='whoLike'> {post.whoLike.map(user => <Link key={user.userId} to={`/users/${user.userId}`}>{user.userName}<br/></Link>)} </div>
                  : null
              }
            </div>
            <br/>
            posted on: {new Date(post.createdOn).toLocaleDateString('fr-CA')}
            <br/>
            last update: {new Date(post.lastUpdateOn).toLocaleDateString('fr-CA')}
            <br/><br/>
            <div className='buttonsDiv'>
              {
                !post.locked && !post.postIsMine && !editMode
                  ?  post.likedByMe
                    ? <input className='smallBlueButton' style={{width: 'auto'}} type='button' value='UNLIKE' onClick={() => unLikePost()}/>                    
                    : <input className='smallBlueButton' type='button' value='LIKE' onClick={() => likePost()}/>
                : null
              }
              {
                user.group === USERS_GROUPS.admin && !post.locked && !editMode
                  ? <> {post.postIsMine ? null : <div style={{width:'5px'}}></div>}  <input className='smallYellowButton' type='button' value='MOVE' onClick={() => updateMovePost(true)} /> </>
                  : null
              }
            </div>
            
            {
              post.postIsMine || user.group === USERS_GROUPS.admin
                ? <>
                    {
                      post.locked
                      ? <div className='buttonsDiv'><input className='smallYellowButton' style={{width: 'auto'}} type='button' value='UNLOCK' onClick={()=> unLockPost(postId)}/></div>
                      : editMode
                        ? null
                        : <div className='buttonsDiv'>
                            <input className='smallBlueButton' type='button' value='EDIT' onClick={() => enterEditMode()} />
                            <input className='smallYellowButton' type='button' value='LOCK' onClick={() => updateLockDialog(!lockDialog)} style={{marginLeft: '7.5px', marginRight: '7.5px'}} />
                            <input className='smallRedButton' type='button' value='DEL' onClick={() => deletePostAction()} />
                          </div>
                    }
                  </>
                : null
            }
            
            
          </div>
        </div>
        <div className='postArea'>
          {
          editMode
            ? <>
                <div>
                  <textarea rows='14' id='editPostText' type='textarea' placeholder='Your text' value={newText} onChange={(event) => updateNewText(event.target.value)}></textarea>
                </div>
                <div>
                  <br/>
                <input className='editPostButton' type='button' value='UPDATE' onClick={() => updatePostText()}  />
                <input className='editPostButton' id='cancelUpdatePostButton' type='button' value='CANCEL' onClick={() => cancelPostEdit()} style={{marginLeft: '5px'}} />
                </div>
              </>
            : post.postText
          } 
        </div>
      </div>
      
      {
      !comments.length
        ? null
        : comments.map(comment => <CommentView key={comment.commentId} props={comment} updateFunc={changeComment} postStatus={post.locked}/>)
      }
      
    </>
  )
}

export default PostView;