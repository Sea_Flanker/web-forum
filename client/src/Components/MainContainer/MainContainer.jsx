import React from 'react';
import './MainContainer.css';


const MainContainer = ({ children }) => {
    return (
        <div className='mainContainerDiv'> {children} </div>
    )
};

export default MainContainer;