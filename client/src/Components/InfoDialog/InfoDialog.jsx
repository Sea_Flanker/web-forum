import React from 'react';
import './InfoDialog.css';

const InfoDialog = ({props}) => {
  
  const {text, buttonText, buttonFunc} = props;



  return (
    <div className='infoDialog'>
      <div className="infoDialogMain">
        <div className='lockDialogTop' style={{fontFamily: 'Simo'}}>DEVELOPERS FORUM<div className='close' onClick={()=>buttonFunc()}>&times;</div></div>
        <div className='infoDialogBody'>
          <div className='infoDialogMessage'>{text}</div>
          <div className='infoDialogMessage'>
            <input className='smallBlueButton' style={{width: 'auto'}} type='button' value={buttonText} onClick={() => buttonFunc()} />
          </div>
        </div>
      </div>
    </div>
  )
};

export default InfoDialog;