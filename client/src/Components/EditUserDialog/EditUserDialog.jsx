import React, { useState, useEffect } from 'react';
import { updateUserInfoById, getUserInfoById } from '../../Requests/user_requests.js';


const EditUserDialog = ({ firstStart, closeFunc, userId, okFunc }) => {

  const errorsMessage = {
    fName: 'First name must be string in range 1-20 symbols',
    lName: 'Last name must be string in range 1-20 symbols',
    email: 'Invalid email',
  }

  const [haveError, updateHaveError] = useState([]);
  const [personalData, updatePersonalData] = useState(
    {
      firstName: '',
      lastName: '',
      email: '',
    }
  );
  
  useEffect(() => {
    if (!firstStart) {
      getUserInfoById(userId).then(result => {
        if (result.message) {

        } else {
          result = result[0];
          updatePersonalData({
            firstName: result.firstName || '',
            lastName: result.lastName || '',
            email: result.email || '',
          })
        }
      })
    }
  }, [firstStart, userId]);

  const changePersonalData = (field, value) => updatePersonalData({...personalData, [field]: value});

  const closeWindow = () => closeFunc(false);

  
  const updateInfo = () => {
    let errorCheck = false;
    if (!RegExp(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/).test(personalData.email)) {
      if (!haveError.includes(errorsMessage.email)) {
        updateHaveError([...haveError, errorsMessage.email]);
      }
      errorCheck = true;
    }

    if (!personalData.firstName.trim().length) {
      if (!haveError.includes(errorsMessage.fName)) {
        updateHaveError([...haveError, errorsMessage.fName]);
      }
      errorCheck = true;
    }

    if (!personalData.lastName.trim().length) {
      if (!haveError.includes(errorsMessage.lName)) {
        updateHaveError([...haveError, errorsMessage.lName]);
      }
      errorCheck = true;
    }

    if (errorCheck) {
      return;
    } else {
      updateHaveError([]);
    }

    updateUserInfoById(userId, personalData)
    .then(result => {
      if (result.message) {
        updateHaveError(result.message.split(';'));
      } else {
        okFunc(result);
        closeWindow();
      }
    });
  }


  return (
    <div className='loginDialog'>
        <div className="registerDialogMain">
          <div className='lockDialogTop' style={{fontFamily: 'Simo'}}>DEVELOPERS FORUM<div className='close' onClick={closeWindow}>&times;</div></div>
          <div className='lockDialogBody'>
            { haveError.length
              ? haveError.map((el, index) => <div key={index} className='errorDiv'>{el}</div>)
              : null
            }

            <div className="row">
              <div className="col-40">
                <label className='newPostLabels' htmlFor='userFirstName'>FIRST NAME</label>
              </div>
              <div className="col-60">
                <input className='loginInputs' id='userFirstName' type='text' placeholder='Your first name'
                  value={personalData.firstName} onChange={(e) => changePersonalData('firstName', e.target.value)}>
                </input>
              </div>
            </div>

            <div className="row">
              <div className="col-40">
                <label className='newPostLabels' htmlFor='userLastName'>LAST NAME</label>
              </div>
              <div className="col-60">
                <input className='loginInputs' id='userLastName' type='text' placeholder='Your second name'
                  value={personalData.lastName} onChange={(e) => changePersonalData('lastName', e.target.value)}>
                </input>
              </div>
            </div>

            <div className="row">
              <div className="col-40">
                <label className='newPostLabels' htmlFor='userEmail'>E-MAIL</label>
              </div>
              <div className="col-60">
                <input className='loginInputs' id='userEmail' type='text' placeholder='Your e-mail address'
                  value={personalData.email} onChange={(e) => changePersonalData('email', e.target.value)}>
                </input>
              </div>
            </div>

          </div>
          <div style={{padding: '10px'}}>
          <div className="col-40"></div>
            <input className='smallBlueButton' type='button' value='OK' onClick={updateInfo} />
            <input className='smallRedButton' type='button' value='CANCEL' onClick={closeWindow} style={{marginLeft: '5px', width: 'auto'}}/>
          </div>
        </div>    
      </div>
  )
}

export default EditUserDialog;