import React from 'react';
import './ErrorBox.css'
import { useHistory } from 'react-router-dom';
import propTypes from 'prop-types';


const ErrorBox = ({message, nextLocation}) => {
  let history = useHistory();
  
  return (
    <div className='dialogDiv'>
      <div className='dialogTop'>ERROR</div>
      <div className='errorBoxMessage'> { message } </div>
      <div className='dialogBottom'>
        <input className='smallBlueButton' type='button' value='OK' onClick={() => history.push(`${nextLocation}`)} />
      </div>
    </div>
  );
};

ErrorBox.propTypes = {
  message: propTypes.string.isRequired,
  nextLocation: propTypes.string.isRequired,
}

export default ErrorBox;