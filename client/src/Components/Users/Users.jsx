import React, {useState, useEffect, useContext} from 'react';
import { Link } from 'react-router-dom';
import { PATHS } from '../../Common/constants';
import { PathContext } from '../../Context/PathContext';
import PathButtonContext from '../../Context/PathButtonContext.js';
import './Users.css';
import { getLastTen, searchInUsers, getUsersByFirstLetter } from '../../Requests/user_requests';
import Preloader from '../Preloader/Preloader';

const Users = () => {
  const { updatePath } = useContext(PathContext);
  const { updateFunc } = useContext(PathButtonContext);
  const alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');

  const [usersList, updateUsersList] = useState(null);
  const [tableCaption, updateTableCaption] = useState('LAST 10 REGISTERED USERS');
  const [searchString, updateSearchString] = useState('');

  useEffect(() => {
    updatePath([PATHS.home, { link: `/users`, name: 'users' }]);
    updateFunc({ enabled: false, value: 'CREATE CATEGORY', actionFunc: () => null });
  }, [updatePath, updateFunc]);

  useEffect(() => {
    getLastTen().then(result => {
      if (result.message) {

      } else {
        updateUsersList(result);
      }
    })
  }, []);


  const searchAction = () => {
    searchInUsers(searchString).then(result => {
      if (result.message) {
        if (result.message.startsWith('nothing found for')) {
          updateTableCaption(`NOTHING FOUND`);
          updateUsersList([]);
        }
      } else {
        updateUsersList(result);
        updateTableCaption(`SEARCH RESULTS FOR '${searchString}'`.toUpperCase());
      }
    })
  }

  const getByLetter = (letter) => {
    updateSearchString('');
    getUsersByFirstLetter(letter).then(result => {
      if (result.message) {

      } else {
        updateUsersList(result);
        updateTableCaption(`RESULT FOR USERS STARTED WITH '${letter}'`.toUpperCase());
      }
    })
  };



  if (!usersList) {
    return <Preloader/>
  }


  return (
    <>
      <div className='usersTopDiv'>
        <div>
          <table style={{width:"100%", fontSize:'18px'}}>
            <tbody>
              <tr>
                <td width='28%'>BROWSE BY FIRST LETTER</td>
                {
                  alphabet.map(x => <td className='letterButton' key={x} onClick={()=> getByLetter(x)}>{x.toUpperCase()}</td>)
                }
              </tr>
            </tbody>
          </table>
        </div>
        <div style={{marginTop:'20px', marginBottom:'0px'}}>
          <table style={{width:"100%", fontSize:'18px'}}>
            <tbody>
              <tr>
                <td width='27%'>SEARCH IN ALL USER NAMES</td>
                <td className='searchTd'>
                  <input className='searchInput' id='userFirstName' type='text' placeholder='search ...'
                    value={searchString} onChange={e => updateSearchString(e.target.value)} />
                <input className='searchButton' type='button' value='SEARCH' onClick={searchAction} />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
      <div className='usersBottomDiv'>
        <table className='banListTable'>
          <caption className='tableCaption'>{tableCaption}</caption>
          <tbody>
            <tr>
              <th>Username</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>E-mail</th>
              <th style={{textAlign:'center'}}>Posts</th>
              <th style={{textAlign:'center'}}>Comments</th>
              <th>Member from</th>
            </tr>
            {
              usersList.map(x => 
                <tr key={x.userId}>
                  <td><Link style={{textDecoration:'none', color:'black'}} to={`/users/${x.userId}`}>{x.username}</Link></td>
                  <td>{x.firstName || 'none'}</td>
                  <td>{x.lastName || 'none'}</td>
                  <td>{x.email || 'none'}</td>
                  <td style={{textAlign:'center'}}>{x.postsCount}</td>
                  <td style={{textAlign:'center'}}>{x.commentsCount}</td>
                  <td>{new Date(x.memberFrom).toLocaleString('fr-CA')}</td>
                </tr>)
            }
          </tbody>
        </table>
      </div>
    </>
  )
}

export default Users;