import React, { useState } from 'react';
import UserBusinessCard from '../UserBusinessCard/UserBusinessCard';
import { Link } from 'react-router-dom';
import { likeCommentById, unLikeCommentById, updateCommentById, deleteCommentById } from '../../Requests/comments_requests';
import ConfirmDialog from '../ConfirmDialog/ConfirmDialog.jsx';
import { useContext } from 'react';
import AuthContext from '../../Context/AuthContext.js';
import { USERS_GROUPS } from '../../Common/constants.js';

const CommentView = ({props, updateFunc, postStatus}) => {
  const likeComment = (commentId) => likeCommentById(props.postId, commentId).then(result => updateFunc(result));
  const unLikeComment = (commentId) => unLikeCommentById(props.postId, commentId).then(result => updateFunc(result));
  
  const [editMode, setEditMode] = useState(false);
  const [newText, updateNewText] = useState(null);
  const [conDialog, updateConDialog] = useState({visible: 'none', message: '', okFunc: ()=> 1, cancelFunc: () => 1});

  const { user } = useContext(AuthContext);

  const enterEditMode = () => {
    updateNewText(props.commentText);
    setEditMode(true);
  }

  const cancelCommentEdit = () => {    
      setEditMode(false);
      updateConDialog({visible: 'none'});
  }

  const updateCommentText = (commentId) => {
      setEditMode(false);
      updateCommentById(props.postId, commentId, { text: newText }).then(result => updateFunc(result));
  }

  const cancelFunction = () => updateConDialog({visible: 'none', message: 'ARE YOU SURE? ALL CHANGES WILL BE LOST!'});

  const confirmDeleteComment = () => {
    updateConDialog({visible: 'none'});
    deleteCommentById(props.postId, props.commentId).then(result => updateFunc(result, 'remove'));
  }

  const cancelUpdateAction = () => updateConDialog({visible: 'block', message: 'ARE YOU SURE ABOUT THIS? ALL CHANGES WILL BE LOST!', okFunc: cancelCommentEdit, cancelFunc: cancelFunction});

  const deletePostAction = () => updateConDialog({visible: 'block', message: 'ARE YOU SURE ABOUT THIS?', okFunc: confirmDeleteComment, cancelFunc: cancelFunction});


  const controlPanel = () => {
    return (
      <>
        <input className='smallBlueButton' type='button' value='EDIT' onClick={() => enterEditMode()}/>
        <span style={{width: '5px'}}></span>
        <input className='smallRedButton' type='button' value='DEL' onClick={() => deletePostAction()}/>
      </>
    )
  }


  return (
    <div className='postDiv'>
      <ConfirmDialog visible={conDialog.visible} message={conDialog.message} okFunc={conDialog.okFunc} cancelFunc={conDialog.cancelFunc}/>
        <div>
          <UserBusinessCard userId={props.authorId} />
          <div className='postAdditionalInfo'>
            <div className='postLikesDiv'>Likes: {props.likes}              
              {
                props.likes
                  ? <div className='whoLike'>{props.likedBy.map(user => <Link key={user.userId} to={`/users/${user.userId}`}>{user.username}<br/></Link>)}</div>
                  : null
              }
            </div>
            <br/>
            posted on: {new Date(props.createdOn).toLocaleDateString('fr-CA')}
            <br/>
            last update: {new Date(props.lastUpdatedOn).toLocaleDateString('fr-CA')}
            <br/><br/>
              {
                !postStatus && !editMode
                  ? <>
                      <div className='buttonsDiv'>
                        {
                          props.commentIsMine
                            ? controlPanel()
                            : props.alreadyLikeThisComment
                              ? <input className='smallBlueButton' style={{width: 'auto'}} type='button' value='UNLIKE' onClick={()=> unLikeComment(props.commentId)}/>
                              : <input className='smallBlueButton' type='button' value='LIKE' onClick={()=> likeComment(props.commentId)} />
                        }
                      </div>
                        {
                          user.group === USERS_GROUPS.admin && !props.commentIsMine
                          ? <div className='buttonsDiv'>{controlPanel()}</div>
                          : null
                        }
                    </>
                  : null
              }
          </div>
        </div>
        <div className='postArea'>
          {
            editMode
            ? <>
                <div>
                  <textarea rows='14' id='editPostText' type='textarea' placeholder='Your text' value={newText} onChange={(event) => updateNewText(event.target.value)}></textarea>
                </div>
                <div>
                  <br/>
                <input className='editPostButton' type='button' value='UPDATE' onClick={() => updateCommentText(props.commentId)}  />
                <input className='editPostButton' id='cancelUpdatePostButton' type='button' value='CANCEL' onClick={() => cancelUpdateAction()} style={{marginLeft: '5px'}} />
                </div>
              </>
            : props.commentText
          }
        </div>
      </div>
  )
}

export default CommentView;