import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
import PathButtonContext from '../../Context/PathButtonContext';
import { PathContext } from '../../Context/PathContext';

import './PathBar.css';


const PathBar = () => {

  const { options } = useContext(PathButtonContext);
  const { path } = useContext(PathContext);
  
  return (
    <div className='pathDiv'>
      <div>
        {
          path.length
          ? path.map((el, index) => index < path.length - 1
            ? <Link key={index} className='pathDivLink' to={el.link}>{el.name.toLowerCase()}&nbsp;</Link>
            : `/ ${el.name.toLowerCase()}`)
          : null
        }
        </div>
        
        { options.enabled
          ? <input className='smallBlueButton' style={{width: 'auto', height: '22px'}} type='button' value={options.value} onClick={options.actionFunc} />
          : null
        }
    </div>
  )
}

export default PathBar;