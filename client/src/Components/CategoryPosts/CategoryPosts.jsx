import React from 'react';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { getAllPostsFromCategory } from '../../Requests/category_requests.js';
import Preloader from '../Preloader/Preloader.jsx';
import './CategoryPosts.css';
import ErrorBox from '../ErrorBox/ErrorBox.jsx';

import userIcon from './user.png'
import lockIcon from './lock.png'
import NewPostDialog from '../NewPostDialog/NewPostDialog.jsx';
import { useContext } from 'react';
import PathButtonContext from '../../Context/PathButtonContext.js';
import { PathContext } from '../../Context/PathContext.js';
import { PATHS } from '../../Common/constants.js';
import AuthContext from '../../Context/AuthContext.js';


const CategoryPosts = (props) => {
  const categoryId = props.match.params.id;
  const categoryName = (props.location.search.split('='))[1];

  const [posts, updatePosts] = useState(null);
  const [newPostShow, updateNewPostShow] = useState(false ? 'block' : 'none');

  const { updateFunc } = useContext(PathButtonContext);
  const { updatePath } = useContext(PathContext);

  const { user } = useContext(AuthContext);

  useEffect(() => {  
    getAllPostsFromCategory(categoryId).then(r => updatePosts(r));
  }, [categoryId]);

  useEffect(() => {
    if (posts && !posts.message) {
      updatePath([PATHS.home, PATHS.category, { link: `/category/${categoryId}`, name: posts[0] ? posts[0].categoryName : categoryName.replace(/%20/g, ' ') }]);
    }
  }, [updatePath, posts, categoryId, categoryName]);
  

  useEffect(() => {
      updateFunc({ enabled: true, value: 'NEW POST', actionFunc: () => updateNewPostShow(!newPostShow) });
  }, [updateFunc, newPostShow]);

  const calculateSpace = (array = []) => array.some(x => x.isLocked && x.postIsMine) ? 24 : 0

  if (!user) {
    return null;
  }
  
  if (posts === null) {
    return <Preloader />
  }

  if (posts.message) {
    return <ErrorBox message={ posts.message } nextLocation='/category' />
  }

  if (!posts.length) {
    return (
      <>
        <div style={{margin:'30px', textAlign:'center'}}> NO POSTS IN THIS CATEGORY </div>
        <NewPostDialog visible={newPostShow} categoryId={categoryId} categoryName={categoryName}/>
      </>
    )
  }  

  return (
    <>
      <NewPostDialog visible={newPostShow} categoryId={categoryId} categoryName={posts[0].categoryName}/>
      <div className="tableDiv">
        <table className="categoryTable">
          <tbody>
            <tr>
              <th> </th>
              <th> Title </th>
              <th> Author </th>
              <th className='centerTh'> Comments </th>
              <th className='centerTh'> Likes </th>
              <th> Last updated </th>
            </tr>
            { posts.map(x =>
              <tr key={x.postId.toString()}>
                <td style={{padding:'2px 10px', width:'48px'}}>
                  { x.postIsMine
                    ? <img className='tableIconImage' title='post is mine' src={userIcon} alt='mine post'/>
                    : <div style={{marginRight:`${calculateSpace(posts)}px`, display:'inline'}}></div>}
                  { x.isLocked ? <img className='tableIconImage' title='post is locked' src={lockIcon} alt='locked post'/> : null}
                </td>
                <td><Link className='linkStyle' to={`/posts/${x.postId}`}>{x.postTitle}</Link></td>
                <td><Link className='linkStyle' to={`/users/${x.authorId}`}>{x.authorName} </Link></td>
                <td className='centerTd'>{x.comments}</td>
                <td className='centerTd'>{x.likes}</td>
                <td>{new Date(x.lastUpdated).toLocaleString('fr-CA')}</td>
              </tr>)
            }
          </tbody>
        </table>
      </div>
    </>
  )
}

export default CategoryPosts;