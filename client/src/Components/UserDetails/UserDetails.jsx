import React, { useContext, useEffect, useState } from 'react';
import { PATHS, USERS_GROUPS } from '../../Common/constants';
import AuthContext from '../../Context/AuthContext';
import PathButtonContext from '../../Context/PathButtonContext';
import { PathContext } from '../../Context/PathContext';
import { getUserInfoById, getUserFriends, removeFriendFromList, addFriendToList, removeFromBanList, deleteUserById } from '../../Requests/user_requests';
import BanUserDialog from '../BanUserDialog/BanUserDialog';
import EditUserDialog from '../EditUserDialog/EditUserDialog';
import ErrorBox from '../ErrorBox/ErrorBox';
import NewConfirmDialog from '../NewConfirmDialog/NewConfirmDialog';
import Preloader from '../Preloader/Preloader';

import './UserDetails.css';

const UserDetails = (props) => {
  const userId = props.match.params.id;
  
  const { updateFunc } = useContext(PathButtonContext);
  const { updatePath } = useContext(PathContext);
  const { user } = useContext(AuthContext);
  
  const [userInfo, updateUserInfo] = useState(null);
  const [friendStatus, updateFriendStatus] = useState(false);
  const [showBanDialog, updateShowBanDialog] = useState(false);
  const [showConfirmDialog, updateShowConfirmDialog] = useState(false);
  const [showEditDialog, updateShowEditDialog] = useState(false);


  const profileIsMine = () => +userId === +user.id ? true : false;  
  
  useEffect(() => {
    if (!userInfo || !userInfo.length) {
      updatePath([PATHS.home, { link: `/users`, name: 'users' }]);
    } else {
      updatePath([PATHS.home, { link: `/users`, name: 'users' }, { link: `/${userInfo.userId}`, name: userInfo.username }]);
    }
  }, [updatePath, userInfo]);
  
  useEffect(() => {
      updateFunc({ enabled: false, value: 'NEW POST', actionFunc: () => 1 });
  }, [updateFunc]);

  useEffect(() => {
    getUserInfoById(userId)
      .then(result => {
        if (result.message) {
          updateUserInfo(result);
        } else {
          updateUserInfo(result[0]);
        }
      })
  }, [userId]);

  useEffect(() => {
    if (userInfo) {
      getUserFriends().then(result => {
        const checkResult = result.filter(x => +x.userId === +userId);
        updateFriendStatus(checkResult.length ? true : false);
      });
    }
  }, [userId, userInfo]);


  const removeFriend = () => {
    removeFriendFromList(userId)
    .then(result => {
      if (result.message) {

      } else {
        updateFriendStatus(false);
        updateUserInfo(result);
      }
    })
  }

  const addFriend = () => {
    addFriendToList(userId)
    .then(result => {
      if (result.message) {

      } else {
        updateFriendStatus(true);
        updateUserInfo(result);
      }
    })
  }

  const badUser = (username) => updateShowBanDialog(true);


  const unBadUser = (username) => {
    removeFromBanList(username)
    .then(result => {
      if (result.message) {

      } else {
        updateUserInfo(result[0]);
      }
    })
  }


  const deleteUser = (userId) => {
    updateShowConfirmDialog(false);
    
    deleteUserById(userId).then(result => {
      if (result.message) {

      } else {
        props.history.push('/users');
      }
    })
  }

  if (userInfo === null) {
    return <Preloader />
  }

  if (userInfo.message) {
    return <ErrorBox message={`${userInfo.message}. Maybe user is deleted`} nextLocation={'/users'}/>
  }


  const bannedTitle = () => {
    return (
      <div className='userTitleBanned'>
        {userInfo.username} (banned) <div style={{width:'30px'}}></div>
          {
            user.group === USERS_GROUPS.admin
              ? <>
                  <input className='smallRedButton' style={{width: 'auto', marginRight:'5px'}} type='button' value='DEL' onClick={() => updateShowConfirmDialog(true)}/>
                  <input className='smallYellowButton' style={{width: 'auto'}} type='button' value='REMOVE BAN' onClick={() => unBadUser(userInfo.username)}/>
                </>
              : null
          }
      </div>
    );
  }

  const friendButtons = () => {
    return (
      <div>
        {
          user.group === USERS_GROUPS.admin
            ? <>
                <input className='smallBlueButton' style={{width: 'auto', marginRight:'5px'}} type='button' value='EDIT' onClick={() => updateShowEditDialog(true)}/>
                <input className='smallRedButton' style={{width: 'auto', marginRight:'5px'}} type='button' value='DEL' onClick={() => updateShowConfirmDialog(true)}/>
                <input className='smallYellowButton' style={{width: 'auto', marginRight:'5px'}} type='button' value='BAN' onClick={badUser}/>
              </>
            : null
        }
        {
          friendStatus
            ? <input className='smallBlueButton' style={{width: 'auto'}} type='button' value='REMOVE FRIEND' onClick={removeFriend}/>
            : <input className='smallBlueButton' style={{width: 'auto'}} type='button' value='ADD FRIEND' onClick={addFriend}/>
        }
      </div>
    )
  }

  return (
    <div className='userDetailsDiv'>
      <div className='userDetMain'>
        {
          userInfo.bannedStatus
            ? bannedTitle()
            : <div className='userTitle'>{userInfo.username} <div style={{width:'30px'}}></div>
              {
                profileIsMine()
                  ? <input className='smallBlueButton' style={{width: 'auto'}} type='button' value='EDIT' onClick={() => updateShowEditDialog(true)}/>
                  : friendButtons()
              }
            
            </div>
        }

        <table>
          <tbody  className='userInfoTable'>
            <tr>
              <td>First Name:</td><td className='secondCol'>{userInfo.firstName || 'none'}</td>
            </tr>
            <tr>
              <td>Last Name:</td><td className='secondCol'>{userInfo.lastName || 'none'}</td>
            </tr>
            <tr>
              <td>E-mail:</td><td className='secondCol'>{userInfo.email || 'none'}</td>
            </tr>
            <tr>
              <td>Member from:</td><td className='secondCol'>{userInfo.memberFrom}</td>
            </tr>
            <tr>
              <td>Posts:</td><td className='secondCol'>{userInfo.postsCount}</td>
            </tr>
            <tr>
              <td>Comments:</td><td className='secondCol'>{userInfo.commentsCount}</td>
            </tr>
            <tr>
              <td>Friends:</td><td className='secondCol'>{userInfo.friendWith + userInfo.friendTo}</td>
            </tr>
            <tr>
              <td>Group:</td><td className='secondCol'>{userInfo.userGroup}</td>
            </tr>
          </tbody>
        </table>
        { showBanDialog && <BanUserDialog username={userInfo.username} okFunc={updateUserInfo} closeFunc={updateShowBanDialog}/> }
        { showConfirmDialog && <NewConfirmDialog message={'YOU REALLY WANT DELETE THIS USER?'} okFunc={() => deleteUser(userInfo.userId)} cancelFunc={() => updateShowConfirmDialog(false)}/>}
        { showEditDialog && <EditUserDialog firstStart={false} closeFunc={updateShowEditDialog} userId={userId} okFunc={updateUserInfo} /> }
      </div>
    </div>
  )
}

export default UserDetails;