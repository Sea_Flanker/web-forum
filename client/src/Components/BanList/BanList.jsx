import React, {useState, useEffect, useContext} from 'react';
import PathButtonContext from '../../Context/PathButtonContext';
import { PathContext } from '../../Context/PathContext';
import { PATHS } from '../../Common/constants';
import { getBanListFromServer, removeFromBanList } from '../../Requests/user_requests.js';
import Preloader from '../Preloader/Preloader.jsx';
import ErrorBox from '../ErrorBox/ErrorBox.jsx';
import { Link } from 'react-router-dom';
import './BanList.css';

const BanList = () => {
  const [banList, updateBanList] = useState(null);

  const [usersList, setUsersList] = useState([]);
  
  const { updateFunc } = useContext(PathButtonContext);
  const { updatePath } = useContext(PathContext);

  useEffect(() => {    
    updatePath([PATHS.home, { link: `/users`, name: 'users' }, { link: ``, name: 'ban list' }]);
  }, [updatePath]);


  useEffect(() => {
      updateFunc({ enabled: usersList.length ? true : false, value: 'REMOVE BAN', actionFunc: () => removeBans()});
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [updateFunc, usersList]);


  useEffect(() => {
    getBanListFromServer().then(result => updateBanList(result));
  }, [])

  if (banList === null) {
    return <Preloader />
  }
  
  if (!banList.length) {
    return <div style={{margin:'20px', textAlign:'center'}}>BAN LIST IS EMPTY <br/><br/>(and you are called admin)</div>
  }

  if (banList.message) {
    return <ErrorBox message={banList.message} nextLocation='/' />
  }


  const selectAll = () => {
    if (banList.length === usersList.length) {
      setUsersList([])
    } else {
      setUsersList(banList.map(x => x.username));
    }
  }

  const checkUncheck = (username) => {
    if (usersList.includes(username)) {      
      setUsersList(usersList.filter(x => x !== username));
    } else {
      setUsersList([...usersList, username]);
    }
  }


  const removeBans = () => {
    Promise.all(usersList.map(x => removeFromBanList(x)))
      .then(result => {
        const unBanned = result.flat(1).map(x => x.userId);
        updateBanList(banList.filter(x => !unBanned.includes(x.userId)));
        setUsersList([]);
      });
  }

  return (
    <div className='banListMain'>
      <table className='banListTable'>
        <tbody>
          <tr>
            <th><input type='checkbox' checked={banList.length === usersList.length} onChange={selectAll}/></th>
            <th>username</th>
            <th>First name</th>
            <th>Second name</th>
            <th>Banned date</th>
            <th>Banned reason</th>
            <th>Banned by</th>
          </tr>
          {
            banList.map(x => 
          
            <tr key={x.userId}>
              <td><input type='checkbox' checked={usersList.includes(x.username)} onChange={() => checkUncheck(x.username)}/> </td>
              <td><Link className='linkStyle' to={`/users/${x.userId}`}>{x.username}</Link></td>
              <td>{x.firstName || 'none'}</td>
              <td>{x.lastName || 'none'}</td>
              <td>{new Date(x.bannedOn).toLocaleString('fr-CA')}</td>
              <td>{x.bannedReason}</td>
              <td>{x.bannedBy}</td>

            </tr>)
          }
        </tbody>
      </table>
    </div>
  )
}

export default BanList;