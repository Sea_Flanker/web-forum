import React, { useEffect, useState } from 'react';
import { showAllCategories } from '../../Requests/category_requests.js';
import { movePostToOtherGroup } from '../../Requests/posts_requests.js';
import './MovePostDialog.css';


const MovePostDialog = ({postId, categoryId, closeFunc, updateFunc}) => {
  const [categoryList, updateCategoryList] = useState([]);
  const [targetCategory, updateTargetCategory] = useState('');

  useEffect(() => {
    showAllCategories().then(result => {
      updateCategoryList(result);
      updateTargetCategory((result.filter(x => x.categoryId !== categoryId))[0].category);
    });
  }, [categoryId]);

  const closeWindow = () => {
    closeFunc(false);
  }

  const movePost = () => {    
    const targetId = (categoryList.filter(x => x.category === targetCategory))[0].categoryId;
    movePostToOtherGroup(+postId, +targetId)
    .then(result => {
      updateFunc(result);
    });
    closeFunc(false);
  }

  if (!categoryList.length) {
    return null;
  }

  const getCurrentCategoryName = () => (categoryList.filter(x => x.categoryId === categoryId))[0].category;

  return (
    <div className='loginDialog'>
        <div className="registerDialogMain">
          <div className='lockDialogTop' style={{fontFamily: 'Simo'}}>DEVELOPERS FORUM<div className='close' onClick={closeWindow}>&times;</div></div>
          <div className='lockDialogBody'>
            
          <div className="row" style={{display:'flex'}}>
              <div className="col-repeat">
                <label className='newPostLabels' htmlFor='registerUsername'>CURRENT CATEGORY</label>
              </div>
              <div className="col-input" style={{display:'flex', alignItems:'center'}}>
                {getCurrentCategoryName()}
              </div>
            </div>

            <div className="row">
              <div className="col-repeat">
                <label className='newPostLabels' htmlFor='categoriesList'>MOVE TO</label>
              </div>
              <div className="col-input">
                <select className='selectItem' id='categoriesList' value={targetCategory} onChange={(e) => updateTargetCategory(e.target.value)}>
                  {categoryList.map(x => x.categoryId !== categoryId ? <option key={x.categoryId}>{x.category}</option> : null)}
                </select>
              </div>
            </div>
            
          </div>   

          <div style={{padding: '20px'}}>
          <div className="col-repeat"></div>
            <input className='smallBlueButton' type='button' value='OK' onClick={movePost} />
            <input className='smallRedButton' type='button' value='CANCEL' onClick={closeWindow} style={{marginLeft: '5px', width: 'auto'}}/>
          </div>
        </div>    
      </div>
  )
}

export default MovePostDialog;