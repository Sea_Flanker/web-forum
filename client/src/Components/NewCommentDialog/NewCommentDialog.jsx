import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { createNewComment } from '../../Requests/comments_requests.js';

const NewCommentDialog = ({ visible, postId, addFunc }) => {
  const [showDialog, updateShowDialog] = useState(visible);
  const [commentText, updateCommentText] = useState('');
  const [haveError, updateHaveError] = useState([]);

  useEffect(() => {
    updateShowDialog(visible)
  }, [visible]);

  const closeDialog = () => updateShowDialog('none');

  const addComment = () => {
    createNewComment(postId, { text: commentText }).then(result => {
      if (result.message) {
        updateHaveError(result.message.split(';'));
        return;
      }
      addFunc(result, 'add');
      updateShowDialog('none');
    });
  }

  return (
    <div id='myModal' className='modal' style={{display: showDialog}}>      
        <div className="postDialogMain">
          <div className='lockDialogTop'>
            ADD NEW COMMENT
            <div className='close' onClick={() => closeDialog()}>&times;</div>
          </div>
          {
            haveError.length
            ? haveError.map((el, index) => <div key={index} className='errorDiv'>{el}</div>)
            : null
          }
          <div className='lockDialogBody'>
            <div className="row">
              <div className="col-25">
                <label className='newPostLabels' htmlFor='newPostText'>Comment text *</label>
              </div>
              <div className="col-75">
                <textarea id='newPostText' type='textarea' placeholder='Your text' style={{height: '200px'}} value={commentText} onChange={(e)=> updateCommentText(e.target.value)}></textarea>
              </div>
            </div>
            <br />
            <div style={{display:'flex', justifyContent:'center'}}>
              <input className='smallBlueButton' style={{width:'auto'}} type='button' value='CREATE' onClick={() => addComment()}/>
              <span style={{width:'5px'}}></span>
              <input className='smallRedButton' style={{width:'auto'}} type='button' value='CANCEL' onClick={() => closeDialog()} />
            </div>
          </div>
        </div>    
      </div>
  )
}

export default NewCommentDialog;