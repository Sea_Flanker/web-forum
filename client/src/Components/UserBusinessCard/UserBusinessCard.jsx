import React from 'react';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { getUserInfoById } from '../../Requests/user_requests';
import './UserBusinessCard.css';


const UserBusinessCard = ({ userId }) => {

  const [userInfo, updateUserInfo] = useState({});

  useEffect(() => {
    getUserInfoById(userId).then(result => result.message ? updateUserInfo(result) : updateUserInfo(result[0]));    
  },[userId]);


  if (userInfo.message) {
    return <div className='deletedUserDiv'>DELETED USER</div>
  }

  if (userInfo) {
    return (
    <div className='divUserBusinessCard'>
      author: <Link className='linkStyleUser' to={`/users/${userInfo.userId}`}> {userInfo.username} </Link>
      <br/>
      name: {`${userInfo.firstName} ${userInfo.lastName}`}
      <br/>
      e-mail: {userInfo.email}
      <br/>
      member from: {new Date(userInfo.memberFrom).toLocaleDateString('en-GB')}
      <br/>
      total publications: { userInfo.postsCount + userInfo.commentsCount }
      <br/>
      <br/>
      <span className='userInfoGroup'>
      {userInfo.userGroup === 'admin' ? `${userInfo.username} is a smurf target (ADMIN)`  : ''}
      </span>
    </div>
    )
  }

  return <div>wait</div>

}

export default UserBusinessCard;