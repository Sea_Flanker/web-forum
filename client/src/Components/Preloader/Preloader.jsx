import React from 'react';
import './Preloader.css';

import preloadImage from './preload.gif'

const Preloader = () => {
  return (
    <div className='preLoadImage'>
      <img src={preloadImage} alt='preload_image'/>
    </div>
  )
}

export default Preloader;