import React, { useState } from 'react';
import { addToBanList } from '../../Requests/user_requests.js';


const BanUserDialog = ({username, okFunc, closeFunc}) => {
  const [banReason, setBanReason] = useState('');
  const [haveError, updateHaveError] = useState(false);


  const closeWindow = () => {
    setBanReason('');
    closeFunc(false);
  }

  const createBan = () => {
    if (banReason.trim().length < 4) {
      updateHaveError(['Reason must be string with minimum 4 symbols!']);
      return;
    }
    addToBanList(username, banReason)
    .then(result => {
      if (result.message) {
        updateHaveError(result.message.split(';'));
      } else {
        okFunc(result[0]);
        closeWindow();
      }
    });
  }
  
  return (
    <div className='loginDialog'>
        <div className="loginDialogMain">
          <div className='lockDialogTop' style={{fontFamily: 'Simo'}}>DEVELOPERS FORUM<div className='close' onClick={closeWindow}>&times;</div></div>
          <div className='lockDialogBody'>
            { haveError
              ? haveError.map((el, index) => <div key={index} className='errorDiv'>{el}</div>)
              : null
            }
            <div className="row">
              <div className="col-40">
                <label className='newPostLabels' htmlFor='banUserReason'>REASON</label>
              </div>
              <div className="col-60">
                <input className='loginInputs' id='banUserReason' type='text' placeholder='reason for ban' value={banReason} onChange={(e)=> setBanReason(e.target.value)}></input>
              </div>
            </div>

            

          </div>   

          <div style={{padding: '10px'}}>
          <div className="col-40"></div>
            <input className='smallBlueButton' type='button' value='OK' onClick={createBan} />
            <input className='smallRedButton' style={{width:'auto', marginLeft:'5px'}} type='button' value='CANCEL' onClick={closeWindow} />
          </div>
        </div>    
      </div>
  )
}

export default BanUserDialog;