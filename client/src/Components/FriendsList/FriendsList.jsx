import React, {useState, useEffect, useContext} from 'react';
import PathButtonContext from '../../Context/PathButtonContext';
import { PathContext } from '../../Context/PathContext';
import { PATHS } from '../../Common/constants';
import { Link } from 'react-router-dom';
import Preloader from '../Preloader/Preloader.jsx';
import ErrorBox from '../ErrorBox/ErrorBox.jsx';
import { getUserFriends, removeFriendFromList } from '../../Requests/user_requests';


const FriendsList = () => {
  const { updateFunc } = useContext(PathButtonContext);
  const { updatePath } = useContext(PathContext);

  const [myFriends, updateMyFriends] = useState(null);
  const [usersList, setUsersList] = useState([]);

  useEffect(() => {    
    updatePath([PATHS.home, { link: `/users`, name: 'users' }, { link: ``, name: 'friends' }]);
  }, [updatePath]);


  useEffect(() => {
    getUserFriends().then(result => updateMyFriends(result));
  }, []);


  useEffect(() => {
    updateFunc({ enabled: usersList.length ? true : false, value: 'REMOVE FRIEND', actionFunc: () => removeFriends()});
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [updateFunc, usersList]);


  const selectAll = () => {
    if (myFriends.length === usersList.length) {
      setUsersList([])
    } else {
      setUsersList(myFriends.map(x => x.userId));
    }
  }


  const checkUncheck = (userId) => {
    if (usersList.includes(userId)) {      
      setUsersList(usersList.filter(x => x !== userId));
    } else {
      setUsersList([...usersList, userId]);
    }
  }


  const removeFriends = () => {
    Promise.all(usersList.map(x => removeFriendFromList(x)))
      .then(result => {
        const newFriendList = result.map(x => x.userId);
        updateMyFriends(myFriends.filter(x => !newFriendList.includes(x.userId)));
        setUsersList([]);
      });
  }


  if (!myFriends) {
    return <Preloader />
  }

  if (!myFriends.length) {
    return <div style={{margin:'20px', textAlign:'center'}}>FRIENDS LIST IS EMPTY</div>
  }

  if (myFriends.message) {
    return <ErrorBox message={myFriends.message} nextLocation='/' />
  }

  return (
    <div className='banListMain'>
      <table className='banListTable'>
        <tbody>
          <tr>
            <th><input type='checkbox'  checked={myFriends.length === usersList.length} onChange={selectAll}/></th>
            <th>Username</th>
            <th>First name</th>
            <th>Second name</th>
            <th>E-mail</th>
            <th>Friends from</th>
          </tr>
          {
            myFriends.map(x => 
              <tr key={x.userId}>
                <td><input type='checkbox' checked={usersList.includes(x.userId)} onChange={() => checkUncheck(x.userId)}/></td>
                <td><Link className='linkStyle' to={`/users/${x.userId}`}>{x.userName}</Link></td>
                <td>{x.firstName || 'none'}</td>
                <td>{x.lastName || 'none'}</td>
                <td>{x.email || 'none'}</td>
                <td>{new Date(x.friendsFrom).toLocaleString('fr-CA')}</td>
              </tr>)
          }
        </tbody>
      </table>
    </div>
  )
}

export default FriendsList;