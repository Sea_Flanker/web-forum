import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { lockPostById } from '../../Requests/posts_requests.js';
import './LockingDialog.css';

const LockingDialog = ({enable, showFunc, postFunc, postId}) => {
  const [visible, setVisible] = useState(enable ? 'block' : 'none');
  const [reason, setReason] = useState('');
  
  useEffect(()=> {
    setVisible(enable);
  },[enable])

  
  const lockPost = (postId) => {
    lockPostById(postId, reason).then(result => postFunc(result));    
    setVisible(!visible);
    showFunc(false);
    setReason('');
  }

  const closeDialog = () => {
    setVisible(!visible);
    showFunc(false);
  }

  if (enable) {
    return (
      <div id='myModal' className='modal' style={{display: visible}}>
      
        <div className="modal-content">
          <div className='lockDialogTop'>
            LOCK POST
            <div className='close' onClick={() => closeDialog()}>&times;</div>
          </div>
          
          <div className='lockDialogBody'>
            <label htmlFor='lockReason'>Enter a reason /optional/</label>
            <input id='lockReason' type='text' value={reason} onChange={(e)=> setReason(e.target.value)}/>
            <br/>
            <br/>
            <input className='smallBlueButton' type='button' value='OK' onClick={() => lockPost(postId)}/>
            <input className='smallRedButton' type='button' value='CANCEL' onClick={() => closeDialog()} style={{marginLeft: '5px', width: 'auto'}}/>
          </div>
        </div>
    
      </div>
    )
  }
  return null;
};

export default LockingDialog;