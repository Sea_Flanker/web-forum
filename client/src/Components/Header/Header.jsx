import React from 'react';
import { useContext, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { getUserDataFromToken } from '../../Common/token_functions';
import AuthContext from '../../Context/AuthContext';
import { loginToServer, logOutFromServer, registerToServer } from '../../Requests/user_requests';
import EditUserDialog from '../EditUserDialog/EditUserDialog';
import InfoDialog from '../InfoDialog/InfoDialog';
import './Header.css';

const Header = () => {
  const [newUserId, updateNewUserId] = useState(0);
  const [showLogin, setShowLogin] = useState('none');
  const [showRegister, setShowRegister] = useState('none');
  const [userName, setUserName] = useState('');
  const [userPassword, setUserPassword] = useState('');
  const [userPasswordConfirm, setUserPasswordConfirm] = useState('');
  const [haveError, updateHaveError] = useState(false);
  const [showSuccessDialog, setShowSuccessDialog] = useState(false);
  const [editDialog, setEditDialog] = useState(false);

  const { user, setUser } = useContext(AuthContext);

  const history = useHistory();

  const infoDialogCommand = () => setShowSuccessDialog(!showSuccessDialog);

  const InfoDialogProps = {
    text: 'successful registration',
    buttonText: 'CONTINUE',
    buttonFunc: infoDialogCommand,
  }


  const loginFunc = () => {
    loginToServer(userName, userPassword)
    .then(result => {
      if (result.message) {
        updateHaveError(result.message.split(';'));
        return;
      }

      const checkToken = getUserDataFromToken(result.token);

      if (checkToken) {
        updateHaveError(false);
        setUser(checkToken);
        localStorage.setItem('token', result.token);
        setShowLogin('none');
        history.push('/');
      }
    });
  }

  const logOutFunc = () => {
    logOutFromServer().then(localStorage.removeItem('token'));
    setUser(null);
    history.push('/');
  }


  const registerUser = () => {
    if (userPassword !== userPasswordConfirm) {
      updateHaveError(['confirm password don\'t match'])
      return;
    }

    registerToServer(userName, userPassword).then(result => {
      if (result.message) {
        updateHaveError(result.message.split(';'));
        return;
      }
      updateNewUserId(result[0].id);
      loginFunc();
      setShowRegister('none');
      setEditDialog(true);
    });
  }

  const cancelFunction = (target = 'login') => {
    if (target === 'register') {
      setShowRegister('none');
      setUserPasswordConfirm('');
    } else {
      setShowLogin('none');
    }
    setUserName('');
    setUserPassword('');
    updateHaveError(false);
  }

  const finishRegistration = () => {
    setEditDialog(false);
    setShowSuccessDialog(!showSuccessDialog);
  }

  return (       
    <> 
      <div className='headerDiv'>
        <div>
          <div className='titleDiv'><Link className='linkStyled' to='/'>DEVELOPERS FORUM</Link></div>
          <div className='sloganDiv'>the perfect algorithm is like the perfect person - we just think it exists</div>
        </div>
        <div></div>
        <div className='loginDiv'>
          
          {
            user
            ? <>
              <div className='userNameBorder'>{user.username}</div>
              <input className='smallYellowButton' type='button' value='LOGOUT' onClick={logOutFunc} style={{marginLeft: '10px', width: 'auto'}}/>
              </>
            :<>
              <input className='smallYellowButton' type='button' value='LOGIN' onClick={() => setShowLogin('block')} style={{width: 'auto'}}/>
              <input className='smallYellowButton' type='button' value='REGISTER' onClick={() => setShowRegister('block')}  style={{marginLeft: '10px', width: 'auto'}}/>
            </>
            
          }

        </div>
      </div>
      
      {/* login dialog */}
      <div className='loginDialog' style={{display: showLogin}}>
        <div className="loginDialogMain">
          <div className='lockDialogTop' style={{fontFamily: 'Simo'}}>DEVELOPERS FORUM<div className='close' onClick={() => cancelFunction('login')}>&times;</div></div>
          <div className='lockDialogBody'>
            { haveError
              ? haveError.map((el, index) => <div key={index} className='errorDiv'>{el}</div>)
              : null
            }
            <div className="row">
              <div className="col-40">
                <label className='newPostLabels' htmlFor='loginUsername'>USERNAME</label>
              </div>
              <div className="col-60">
                <input className='loginInputs' id='loginUsername' type='text' placeholder='Your username' value={userName} onChange={(e)=> setUserName(e.target.value)}></input>
              </div>
            </div>

            <div className="row">
              <div className="col-40">
                <label className='newPostLabels' htmlFor='loginPassword'>PASSWORD</label>
              </div>
            
              <div className="col-60">
                <input className='loginInputs' id='loginPassword' type='password' placeholder='Your password' value={userPassword} onChange={(e)=> setUserPassword(e.target.value)}></input>
              </div>
            </div>

          </div>   

          <div style={{padding: '10px'}}>
          <div className="col-40"></div>
            <input className='smallBlueButton' type='button' value='OK' onClick={() => loginFunc()} />
            <input className='smallRedButton' type='button' value='CANCEL' onClick={() => cancelFunction('login')} style={{marginLeft: '5px', width: 'auto'}}/>
          </div>
        </div>    
      </div>




      {/* register dialog */}
      <div className='loginDialog' style={{display: showRegister}}>
        <div className="registerDialogMain">
          <div className='lockDialogTop' style={{fontFamily: 'Simo'}}>DEVELOPERS FORUM<div className='close' onClick={() => cancelFunction('register')}>&times;</div></div>
          <div className='lockDialogBody'>
            { haveError
              ? haveError.map((el, index) => <div key={index} className='errorDiv'>{el}</div>)
              : null
            }
            <div className="row">
              <div className="col-repeat">
                <label className='newPostLabels' htmlFor='registerUsername'>USERNAME</label>
              </div>
              <div className="col-input">
                <input className='loginInputs' id='registerUsername' type='text' placeholder='Your username' value={userName} onChange={(e)=> setUserName(e.target.value)}></input>
              </div>
            </div>

            <div className="row">
              <div className="col-repeat">
                <label className='newPostLabels' htmlFor='registerPassword'>PASSWORD</label>
              </div>
            
              <div className="col-input">
                <input className='loginInputs' id='registerPassword' type='password' placeholder='Your password' value={userPassword} onChange={(e)=> setUserPassword(e.target.value)}></input>
              </div>
            </div>


            <div className="row">
              <div className="col-repeat">
                <label className='newPostLabels' htmlFor='registerPasswordRepeat'>REPEAT PASSWORD</label>
              </div>
            
              <div className="col-input">
                <input className='loginInputs' id='registerPasswordRepeat' type='password'
                  placeholder='Repeat your password' value={userPasswordConfirm} onChange={(e)=> setUserPasswordConfirm(e.target.value)}></input>
              </div>
            </div>


          </div>   

          <div style={{padding: '10px'}}>
          <div className="col-repeat"></div>
            <input className='smallBlueButton' type='button' value='OK' onClick={() => registerUser()} />
            <input className='smallRedButton' type='button' value='CANCEL' onClick={() => cancelFunction('register')} style={{marginLeft: '5px', width: 'auto'}}/>
          </div>
        </div>    
      </div>
      
      {
        showSuccessDialog
          ? <InfoDialog props={InfoDialogProps}/>
          : null
      }
      { editDialog && <EditUserDialog userId={newUserId} firstStart={true} okFunc={finishRegistration} closeFunc={finishRegistration} /> }
    </>
  )
};

export default Header;