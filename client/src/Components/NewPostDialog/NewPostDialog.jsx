import React from 'react';
import { useEffect } from 'react';
import { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { createPost } from '../../Requests/posts_requests';
import './NewPostDialog.css';

const NewPostDialog = ({visible, categoryId, categoryName}) => {
  let history = useHistory();

  const [showDialog, updateShowDialog] = useState(visible);
  const [postTitle, updatePostTitle] = useState('');
  const [postText, updatePostText] = useState('');

  const [haveError, updateHaveError] = useState([]);

  useEffect(() => {
      updateShowDialog(visible)
    }, [visible]
  )

  const closeDialog = () => updateShowDialog('none');

  const addNewPost = () => {
    const bodyObject = {
      category: categoryId,
      title: postTitle,
      text: postText,
    }
    createPost(bodyObject).then(result => {
      if(result.message) {
        updateHaveError(result.message.split(';'));
        return;
      }
      history.push(`/posts/${result.id}`);
    });
    
  }
  

  return (
    <div id='myModal' className='modal' style={{display: showDialog}}>      
        <div className="postDialogMain">
          <div className='lockDialogTop'>
            CREATE NEW POST IN CATEGORY {categoryName}
            <div className='close' onClick={() => closeDialog()}>&times;</div>
          </div>
          {
            haveError.length
            ? haveError.map((el, index) => <div key={index} className='errorDiv'>{el}</div>)
            : null
          }
          <div className='lockDialogBody'>
            <div className="row">
              <div className="col-25">
                <label className='newPostLabels' htmlFor='newPostTitle'>Post title *</label>
              </div>
              <div className="col-75">
                <input id='newPostTitle' type='text' placeholder='Your title' value={postTitle} onChange={(e)=> updatePostTitle(e.target.value)}></input>
              </div>
            </div>
            <div className="row">
              <div className="col-25">
                <label className='newPostLabels' htmlFor='newPostText'>Post text *</label>
              </div>
              <div className="col-75">
                <textarea id='newPostText' type='textarea' placeholder='Your text' style={{height: '200px'}} value={postText} onChange={(e)=> updatePostText(e.target.value)}></textarea>
              </div>
            </div>
            <br />
            <div style={{display:'flex', justifyContent:'center'}}>
              <input className='smallBlueButton' style={{width:'auto'}} type='button' value='CREATE' onClick={() => addNewPost()}/>
              <span style={{width:'5px'}}></span>
              <input className='smallRedButton' style={{width:'auto'}} type='button' value='CANCEL' onClick={() => closeDialog()} />
            </div>
          </div>
        </div>    
      </div>
  )
  
}

export default NewPostDialog;