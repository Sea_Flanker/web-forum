import React from 'react';
import PropTypes from 'prop-types';

const NewConfirmDialog = ({message, okFunc, cancelFunc}) => {

  const closeConfirmDialog = (answer) => {
    answer ? okFunc() : cancelFunc();
  }

  return (
    <div id='myModal' className='modal'>      
      <div className="postDialogMain">
        <div className='lockDialogTop' style={{fontFamily: 'Simo'}}>DEVELOPERS FORUM<div className='close' onClick={() => closeConfirmDialog(false)}>&times;</div></div>
        <br/>
        <div className='lockDialogBody'>{message}</div>
        <div style={{padding: '10px'}}>
          <input className='smallBlueButton' type='button' value='OK' onClick={() => closeConfirmDialog(true)} />
          <input className='smallRedButton' type='button' value='CANCEL' onClick={() => closeConfirmDialog(false)} style={{marginLeft: '5px', width: 'auto'}}/>
        </div>
      </div>    
    </div>
  )
}

NewConfirmDialog.propTypes = {
  message: PropTypes.string,
  okFunc: PropTypes.func,
  cancelFunc: PropTypes.func,
}


export default NewConfirmDialog;