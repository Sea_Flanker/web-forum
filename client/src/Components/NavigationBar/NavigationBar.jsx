import React from 'react';
import { useContext } from 'react';
import {useHistory } from 'react-router';
import { USERS_GROUPS } from '../../Common/constants';
import AuthContext from '../../Context/AuthContext';
import './NavigationBar.css';



const NavigationBar = () => {
  const history = useHistory();
  const { user } = useContext(AuthContext);
  return (
    <div className='navigationDiv'>
      <div className='menuButtonDiv' onClick={() => history.push('/')}>HOME</div>
      <span className='borderSpan'></span>
      <div className='menuButtonDiv' onClick={() => history.push('/category')}>FORUM</div>
      <span className='borderSpan'></span>
      <div className='menuButtonDiv' onClick={() => history.push('/users')}>USERS</div>
      <span className='borderSpan'></span>
      <div className='menuButtonDiv' onClick={() => history.push('/friends')}>FRIENDS</div>
      <span className='borderSpan'></span>
      {
        user.group === USERS_GROUPS.admin
          ? <>
              <div className='menuButtonDiv' onClick={() => history.push(`/banList`)}>BAN LIST</div>
              <span className='borderSpan'></span>
            </>
          : null
      }
      <div className='menuButtonDiv' onClick={() => history.push(`/users/${user.id}`)}>PROFILE</div>
    </div>
  )
}

export default NavigationBar;