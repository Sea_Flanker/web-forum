import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { showAllCategories } from '../../Requests/category_requests.js';
import ErrorBox from '../ErrorBox/ErrorBox.jsx';
import Preloader from '../Preloader/Preloader';
import { useContext } from 'react';
import PathButtonContext from '../../Context/PathButtonContext.js';
import AuthContext from '../../Context/AuthContext.js';
import { PATHS, USERS_GROUPS } from '../../Common/constants.js';
import { PathContext } from '../../Context/PathContext.js';
import { createNewPostCategory, deleteGroupById, updateGroupById } from '../../Requests/posts_requests.js';
import './CategoryView.css';
import ConfirmDialog from '../ConfirmDialog/ConfirmDialog.jsx';
import InfoDialog from '../InfoDialog/InfoDialog.jsx';

const CategoryView = (history) => {
  const [categories, setCategories] = useState(null);
  const [haveError, setHaveError] = useState(false);
  const [conDialog, updateConDialog] = useState({visible: 'none', message: '', okFunc: ()=> 1});
  const [infoDialog, updateInfoDialog] = useState(false);

  const [addNewCategory, updateAddNewCategory] = useState(false);
  const [newCategoryName, updateNewCategoryName] = useState('');

  const [editMode, setEditMode] = useState(null);
  const [changeName, setChangeName] = useState(null);
  const [InfoDialogProps, changeInfoDialogProps] = useState({
    text: `name ${changeName} already exists!`,
    buttonText: 'OK',
    buttonFunc: () => null,
  });

  const { updateFunc } = useContext(PathButtonContext);
  const { updatePath } = useContext(PathContext);
  const { user: currentUser } = useContext(AuthContext);

  

  useEffect(() => {
    updatePath([PATHS.home, PATHS.category])
  }, [updatePath]);
  
  useEffect(() => {
    if (currentUser && currentUser.group === USERS_GROUPS.admin) {
      updateFunc({ enabled: true, value: 'CREATE CATEGORY', actionFunc: () => updateAddNewCategory(true) });
    } else {
      updateFunc({ enabled: false, value: 'CREATE CATEGORY', actionFunc: () => null });
    }
  }, [updateFunc, currentUser]);

  useEffect(() => {
    showAllCategories()
    .then(result => {
      if (result.message) {
        setHaveError(result.message);
      } else {
        setCategories(result);
        setHaveError(false);
      }
    })
  }, []);


  const enterEditMode = (id, name) => {
    setEditMode(id);
    setChangeName(name);
  }

  const infoDialogCommand = () => updateInfoDialog(false);

  const updateGroup = () => {
    const checkOthers = categories.filter(x => x.category.toLowerCase() === changeName.toLowerCase() && x.categoryId !== editMode);
    if (!checkOthers.length) {
      updateGroupById(editMode, changeName).then(result => {
        const newArray = categories.map(x => x.categoryId === result[0].id ? {...x, category: result[0].groupName} : x);
        setCategories(newArray);
      });
      setEditMode(null);
    } else {
      changeInfoDialogProps({
        text: `category ${changeName} already exists!`,
        buttonText: 'OK',
        buttonFunc: infoDialogCommand,
      })
      updateInfoDialog(true);
    }
  }

  const cancelFunction = () => updateConDialog({visible: 'none'});
  const deleteGroupAction = (id) => updateConDialog({visible: 'block', message: 'ARE YOU SURE? ALL POSTS IN THIS GROUP WILL BE LOST!', okFunc: () => confirmDeleteGroup(id)});
  
  const confirmDeleteGroup = (id) => {
    deleteGroupById(id).then(result => {
      const newArray = categories.filter(x => x.categoryId !== result[0].id);
      setCategories(newArray);
    });

    updateConDialog({visible: 'none'});
  }

  

  const cancelAddNewCategory = () => {
    updateAddNewCategory(false);
    updateNewCategoryName('');
  }

  const createNewCategory = () => {
    if (!newCategoryName.trim()) {
      changeInfoDialogProps({
        text: 'INVALID NAME',
        buttonText: 'OK',
        buttonFunc: infoDialogCommand,
      })
      updateInfoDialog(true);
      return;
    }

    createNewPostCategory(newCategoryName).then(result => {
      if (!result.message) {
        setCategories([...categories, result[0]]);
      } else {
        changeInfoDialogProps({
          text: result.message,
          buttonText: 'OK',
          buttonFunc: infoDialogCommand,
        })
        updateInfoDialog(true);
      }
    });
    changeInfoDialogProps({
      text: 'CATEGORY SUCCESSFUL CREATED',
      buttonText: 'OK',
      buttonFunc: infoDialogCommand,
    })
    updateInfoDialog(true);
    cancelAddNewCategory();
  }

  if (haveError) {
    return <ErrorBox message={ haveError } nextLocation='/' />
  }
  
  if (categories === null) {
    return <Preloader />
  }

  
  return (
    <div className="tableDiv">
      {
        infoDialog
        ? <InfoDialog props={InfoDialogProps}/>
        : null
      }
      <ConfirmDialog visible={conDialog.visible} message={conDialog.message} okFunc={conDialog.okFunc} cancelFunc={cancelFunction}/>
      <table className="categoryTable">
        <tbody>
          <tr>
            <th> Category </th>
            <th className='centerTh'> Posts </th>
            <th className='centerTh'> Comments </th>
            <th> Last activity </th>
            {
              currentUser.group === USERS_GROUPS.admin
                ? <th> Settings </th>
                : null
            }
          </tr>
          {
            addNewCategory
            ? <tr style={{height:'45px'}}> 
                <td colSpan={5} style={{padding:0}}>
                  <div className='editGroup'>
                        <input className='inputsForChange' placeholder='New category name' style={{width:'20%'}} type='text' value={newCategoryName} onChange={(e) => updateNewCategoryName(e.target.value)} />
                        <div className='blueOkIcon' style={{marginRight:'5px'}} onClick={createNewCategory}>&or;</div>
                        <div className='redCancelIcon' onClick={cancelAddNewCategory}>&times;</div>
                  </div>
                </td>
              </tr>

            : null
          }
          {categories.map(x =>
            <tr key={x.categoryId} style={{height:'45px'}}>
              {
                editMode === x.categoryId
                  ? <td style={{padding:0}}>
                      <div className='editGroup'>
                        <input className='inputsForChange' type='text' value={changeName} onChange={(e) => setChangeName(e.target.value)} />
                        <div className='blueOkIcon' style={{marginRight:'5px'}} onClick={updateGroup}>&or;</div>
                        <div className='redCancelIcon' onClick={() => setEditMode(null)}>&times;</div>
                      </div>
                    </td>
                  : <td style={{width:'25%'}}><Link className='linkStyle' to={`/category/${x.categoryId}?name=${x.category}`}>{x.category}</Link></td>
              }

              <td className='centerTd'>{x.postsCount}</td>
              <td className='centerTd'>{x.commentsCount}</td>
              <td>{x.lastActivity? new Date(x.lastActivity).toLocaleString('fr-CA') : 'none'}</td>
              {
              currentUser.group === USERS_GROUPS.admin
                ? <td style={{width:'15%'}}>
                    {
                      !editMode
                        ? <>
                            <input type='button' className='smallBlueButton' value='EDIT' onClick={() => enterEditMode(x.categoryId, x.category)} />
                            <input type='button' className='smallRedButton' value='DEL' style={{marginLeft:'5px'}} onClick={() => deleteGroupAction(x.categoryId)} />
                          </>
                        : null
                    }
                  </td>
                : null
              }
            </tr>)}
        </tbody>
      </table>
    </div>
  )
}

export default CategoryView;