import React from 'react';
import { useContext } from 'react';
import AuthContext from '../../Context/AuthContext';
import { useEffect } from 'react';
import { useHistory } from 'react-router';
import './Home.css';

import jsBook from './JS.jpg';
import nodeBook from './node.jpg';
import hopeBook from './hope.jpg';
import gitBook from './gitBook.jpg';
import delBook from './delCode.jpg';
import googleBook from './google.jpg';
import unitTestBook from './unitTest.jpg';
import writeCodeBook from './writeCode.jpeg';
import PathButtonContext from '../../Context/PathButtonContext';
import { PathContext } from '../../Context/PathContext';
import { PATHS } from '../../Common/constants';



const Home = () => {
  const { user } = useContext(AuthContext);
  const { updateFunc } = useContext(PathButtonContext);
  const history = useHistory();
  
  const { updatePath } = useContext(PathContext);


  useEffect(() => {
    updatePath([PATHS.home])
  }, [updatePath]);


  useEffect(() => {
    const elka = () => history.push('/category');
    if (user) {
      updateFunc({ enabled: true, value: 'GO TO FORUM', actionFunc: elka });
    }
  }, [user, updateFunc, history]);

  


  return (
    <div>
      <div className='homeTopDiv'>if programmers are still reading books - we recommend it for you</div>
      <div className='imagesDiv'>
        <img className='homeImg' src={nodeBook} alt='nodeBook'></img>
        <img className='homeImg' src={jsBook} alt='jsBook'></img>
        <img className='homeImg' src={gitBook} alt='gitBook'></img>
        <img className='homeImg' src={hopeBook} alt='hopeBook'></img>
      </div>
      <div className='imagesDiv'>
        <img className='homeImg' src={writeCodeBook} alt='writeCodeBook'></img>
        <img className='homeImg' src={unitTestBook} alt='unitTestBook'></img>
        <img className='homeImg' src={delBook} alt='delBook'></img>
        <img className='homeImg' src={googleBook} alt='googleBook'></img>
        </div>
    </div>
  )
}

export default Home;