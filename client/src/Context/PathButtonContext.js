import { createContext } from "react";

const PathButtonContext = createContext({
  options: { enabled: false, value: 'none', actionFunc: () => null },
  updateFunc: () => null,
});

export default PathButtonContext;