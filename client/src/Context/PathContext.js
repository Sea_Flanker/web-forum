import { createContext } from 'react';

export const PathContext = createContext(
  {
    path: [],
    updatePath: () => null,
  }
);